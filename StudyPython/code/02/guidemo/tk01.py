#coding:utf8
import Tkinter as tk

class Application(tk.Frame): #继承自tk.Frame
    def __int__(self, master=None):
        tk.Frame.__init__(self, master) #调用父类构造方法
        self.grid() # Necessary to make the application actually appear on the screen.
        self.createWidgets()
    def createWidgets(self):
        self.quitButton = tk.Button(self, text="Quit",
            command=self.quit)
        self.quitButton.grid()
app = Application()
app.master.title('Sample application')
app.mainloop()
