#coding:utf-8
'''
	本例子程序摘抄自优酷空间'用Python玩转大数据 - 简介'
	'http://v.youku.com/v_show/id_XNTk3NDcwMTg4.html?from=y1.7-2'
'''
print "hello,world!"
import urllib2

def fetch(url):
	http_header = {'User-Agent': 'Chrome'}
	# http_request = urllib2.Request(url, None)
	http_request = urllib2.Request(url, None, http_header)
	

	http_response = urllib2.urlopen(http_request)

	print http_response.code

	print http_response.info()

	print http_response.read()

if __name__ == '__main__':
	fetch("http://www.baidu.com")

