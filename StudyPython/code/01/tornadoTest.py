#coding:utf-8
#filename: tornadoTest.py

'''
	例子程序来自"tornado"官网的helloworld
	'http://www.tornadoweb.org/en/stable/'
'''

import tornado.ioloop
import tornado.web

class MainHandler(tornado.web.RequestHandler):
	def get(self):
		self.write("Hello, world")

application = tornado.web.Application([
	(r"/", MainHandler),
])

if __name__ == "__main__":
	application.listen(8888)
	tornado.ioloop.IOLoop.current().start()
