#-*- coding:utf-8 -*-

def function(a,b):
    print a,b

apply(function, ("hello","world"))
apply(function, (1, 2+3))

print "===="

apply(function, ("hello,",), {"b":"world"})
apply(function , (), {"a":"hello", "b":"world"})


# if __name__ == '__main__':
#     print "hello,world"
