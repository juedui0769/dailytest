#-*- coding:utf-8 -*-

'''
    1, 提供网址
    2, 爬取该网址的内容
    3, 分析内容,统计词频.
'''

import tornado.httpclient as hc

#此程序只做到了第二步.
class websiteWordStatistics:
    def fetch(self,url):
        _header = {'User-Agent': 'Chrome'}
        _req = hc.HTTPRequest(url=url, method='GET', headers=_header, connect_timeout=20, request_timeout=600)

        _client = hc.HTTPClient()
        _resp = _client.fetch(_req)

        print _resp.code
        print _resp.body

if __name__ == '__main__':
    wws = websiteWordStatistics()
    url = "http://www.baidu.com"
    url = "http://www.apache.org/"
    wws.fetch(url)


