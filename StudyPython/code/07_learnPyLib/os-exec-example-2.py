#-*- coding:utf-8 -*-

# if __name__ == '__main__':
#     print "hello,world"

'''
    程序运行报错, 同书本例子不符.
'''


import os
import sys

def run(program, *args):
    pid = os.fork()
    if not pid:
        os.execvp(program, (program,) + args)
    return os.wait()[0]

run("python", "hello.py")

print("goodbye")





