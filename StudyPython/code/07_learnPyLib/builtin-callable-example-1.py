#-*- coding:utf-8 -*-

# if __name__ == '__main__':
#     print "hello,world"

'''
    Python 不会把类作为本质上的类型对待;相反地,
    所有的类都属于一个特殊的类类型(special class type),
    所有的类实例属于一个特殊的实例类型(special instance type).
    这意味着你不能使用 type 函数来测试一个实例是否属于一个给定的类;所有的实例都是同样的类型!
    为了解决这个问题,你可以使用 isinstance 函数,它会检查一个对象是不是给定类(或其子类)的实例.
'''


def dump(function):
    if callable(function):
        print function, "is callable"
    else:
        print function, "is *not* callable"

class A:
    def method(self,value):
        return value

class B(A):
    def __call__(self,value):
        return value

a = A()
b = B()

dump(0)
dump("string")
dump(callable)

dump(dump)
dump(A)
dump(B)
dump(B.method)

dump(a) # instances
dump(b)
dump(b.method)




