#-*- coding:utf-8 -*-

# if __name__ == '__main__':
#     print "hello,world"

import time

# make sure we have a strptime function!

try:
    strptime = time.strptime
except AttributeError:
    from time import strptime

print strptime("30 Nov 00", "%d %b %y")
print strptime("1 Jan 70 1:30pm", "%d %b %y %I:%M%p")


