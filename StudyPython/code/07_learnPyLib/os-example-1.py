#-*- coding:utf-8 -*-

# if __name__ == '__main__':
#     print "hello,world"

'''
    返回对象中有些属性在非 Unix 平台下是无意义的, 比如(st_inode, st_dev)
    stat 模块包含了很多可以处理该返回对象的常量及函数.
'''

import os
import time

file = "samples/sample.jpg"

def dump(st):
    # 很好奇,这是怎么赋值的呢?
    mode, ino, dev, nlink, uid, gid, size, atime, mtime, ctime = st
    print "- size: ", size, "bytes"
    print "- owner:", uid, gid
    print "- created:", time.ctime(ctime)
    print "- last accessed:", time.ctime(atime)
    print "- last modified:", time.ctime(mtime)
    print "- mode: ", oct(mode)
    print "- inode/dev:", ino, dev

    # get stats for a filename

st = os.stat(file)
print "stat", file
dump(st)
print #

# get stats for an open file
fp = open(file)
st = os.fstat(fp.fileno())
print "fstat", file
dump(st)








