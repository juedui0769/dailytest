#-*- coding:utf-8 -*-

# if __name__ == '__main__':
#     print "hello,world"

def dump(value):
    print type(value), value

class A:
    a = 1

class B(object):
    a = 1

dump(1)
dump(1.0)
dump("one")

dump(A)
dump(B)
