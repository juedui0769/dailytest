#-*- coding:utf-8 -*-

# if __name__ == '__main__':
#     print "hello,world"

import time

now = time.time()

print now , "seconds since", time.gmtime(0)[:6]
print
print "or in other words:"
print "- local time:", time.localtime(now)
print "- utc:", time.gmtime(now)


