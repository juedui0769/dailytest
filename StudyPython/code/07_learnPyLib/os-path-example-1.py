#-*- coding:utf-8 -*-

# if __name__ == '__main__':
#     print "hello,world"

'''
    os.path 模块包含了各种处理长文件名(路径名)的函数.
    os.path 模块包含了许多与平台无关的处理长文件名的函数.
    也就是说,你不需要处理前后斜杠,冒号等.
'''

import os

filename = "my/little/pony"

print "using", os.name, "..."
print "split", "=>", os.path.split(filename)

print "splitext", "=>", os.path.splitext(filename)
print "dirname", "=>", os.path.dirname(filename)
print "basename", "=>", os.path.basename(filename)

print "join", " =>", os.path.join(os.path.dirname(filename)), os.path.basename(filename)











