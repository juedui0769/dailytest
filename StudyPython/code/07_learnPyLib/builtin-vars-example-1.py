#-*- coding:utf-8 -*-

# if __name__ == '__main__':
#     print "hello,world"

'''
    在python自带的CHM文档中,"2. Built-in Functions"有介绍"vars()"
    想看下vars()到底是什么,可以 "print vars()" 看下结果.
'''

book = "library2"
pages = 250
scripts = 350

print "the %(book)s book contains more than %(scripts)s scripts" % vars()

# print vars()