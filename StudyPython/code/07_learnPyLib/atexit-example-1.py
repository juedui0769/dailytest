#-*- coding:utf-8 -*-

# if __name__ == '__main__':
#     print "hello,world"

'''
    该模块其实是一个对 sys.exitfunc 钩子(hook)的简单封装.
'''


import atexit

def exit(*args):
    print "exit", args

# register two exit handler
atexit.register(exit)
atexit.register(exit, 1)
atexit.register(exit, "hello", "world")


