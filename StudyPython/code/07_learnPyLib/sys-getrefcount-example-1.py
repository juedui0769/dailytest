#-*- coding:utf-8 -*-

# if __name__ == '__main__':
#     print "hello,world"

'''
    处理引用计数
    getrefcount 函数返回给定对象的引用计数(也就是这个对象的使用次数).
    Python会跟踪这个值, 当它减少为0的时候,就销毁这个对象.
'''


import sys

variable = 1234

print sys.getrefcount(0)
print sys.getrefcount(variable)
print sys.getrefcount(None)



