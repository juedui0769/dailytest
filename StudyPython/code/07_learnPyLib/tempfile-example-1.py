#-*- coding:utf-8 -*-

# if __name__ == '__main__':
#     print "hello,world"

import tempfile
import os

tempfile = tempfile.mktemp()

print "tempfile", "=>", tempfile

file = open(tempfile, "w+b")
file.write("*" * 10)
file.seek(0)
print len(file.read()), "bytes"
file.close()

try:
    os.remove(tempfile)
except OSError:
    pass



