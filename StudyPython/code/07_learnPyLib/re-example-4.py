#-*- coding:utf-8 -*-

# if __name__ == '__main__':
#     print "hello,world"

'''

'''


import re

text = "you're no fun anymore..."

# literal replace (string.replace is faster)
#
print re.sub("fun", "entertaining", text)

# collapse all non-leter sequences to a single dash
print re.sub("[^\w]+", "-", text)

# convert all words to beeps
print re.sub("\S+", "-BEEP-", text)






