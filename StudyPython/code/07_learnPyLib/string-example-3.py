#-*- coding:utf-8 -*-

# if __name__ == '__main__':
#     print "hello,world"

'''
    大多数情况下(特别是当你使用的是1.6及更高版本时),
    你可以使用 int 和 float 函数代替 string 模块中对应的函数.
'''


import string

print int("4711")
print string.atoi("4711")

print string.atoi("4711", 8)
print string.atoi("4711", 16)

print string.atoi("3mv", 36)

print string.atoi("4711", 0)
print string.atoi("04711", 0)
print string.atoi("0x4711", 0)

print float("4711")
print string.atof("1")
print string.atof("1.23e5")


