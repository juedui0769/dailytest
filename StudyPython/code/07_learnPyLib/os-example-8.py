#-*- coding:utf-8 -*-

# if __name__ == '__main__':
#     print "hello,world"

'''
    system 函数在当前进程下执行一个新命令, 并等待它完成.

'''


import os

if os.name == "nt":
    command = "dir"
else:
    command = "ls -l"

os.system(command)


