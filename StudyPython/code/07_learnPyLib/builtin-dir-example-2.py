#-*- coding:utf-8 -*-

# if __name__ == '__main__':
#     print "hello,world"

'''
    在python自带的文档(chm格式的)中,可以搜索到"__bases__",其代表父类.
'''

class A:
    def a(self):
        pass
    def b(self):
        pass
class B(A):
    def c(self):
        pass
    def d(self):
        pass
def getmembers(klass, members=None):
    # get a list of all class members, ordered by class
    if members is None:
        members = []
    for k in klass.__bases__:
        getmembers(k, members)
    for m in dir(klass):
        if m not in members:
            members.append(m)
    return members

print getmembers(A)
print getmembers(B)
print getmembers(IOError)



