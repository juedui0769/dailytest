#-*- coding:utf-8 -*-
#
# if __name__ == '__main__':
#     print "hello,world"

'''
    eval函数只针对简单的表达式.
    如果要处理大块的代码,你应该使用 compile 和 exec 函数.
'''

print eval("__import__('os').getcwd()", {})
print eval("__import__('os').remove('file')", {"__builtins__":{}})

