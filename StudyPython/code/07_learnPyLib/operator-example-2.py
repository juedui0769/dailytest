#-*- coding:utf-8 -*-

# if __name__ == '__main__':
#     print "hello,world"

import operator
import UserList

def dump(data):
    print type(data), "=>",
    if operator.isCallable(data):
        print "CALLABLE",
    if operator.isMappingType(data):
        print "MAPPING",
    if operator.isNumberType(data):
        print "NUMBER"
    if operator.isSequenceType(data):
        print "SEQUENCE",
    print

dump(0)
dump("string")
dump("string"[0])

dump([1,2,3])
dump((1,2,3))

dump({"a": 1})

dump(len)

dump(UserList)
dump(UserList.UserList)
dump(UserList.UserList())






