#-*- coding:utf-8 -*-

# if __name__ == '__main__':
#     print "hello,world"

'''
    在"Library Reference > 2.Built-in Functions > open()"中有
    介绍open(file, "rb"),可以前往查阅.
'''


def load(file):
    if isinstance(file, type("")):
        file = open(file, "rb")
        return file.read()

print len(load("./samples/sample.jpg")), "bytes"

# 下面这句会报错.不知道原因.
#print len(load(open("./samples/sample.jpg", "rb"))), "bytes"