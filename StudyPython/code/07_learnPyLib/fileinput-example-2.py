#-*- coding:utf-8 -*-

# if __name__ == '__main__':
#     print "hello,world"

'''
    为何我会觉得这个程序是异步执行的呢?!
    本来我认为应该在第一行打印的信息, 在最后才打印.
'''


import fileinput
import glob
import string, sys

for line in fileinput.input(glob.glob("samples/*.txt")):
    if fileinput.isfirstline(): # first in a file?
        sys.stderr.write("-- reading %s --\n" % fileinput.filename())
    sys.stdout.write(str(fileinput.lineno()) + " " + string.upper(line) )


