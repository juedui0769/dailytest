#-*- coding:utf-8 -*-
'''
    使用的"scrapy"的cmdline来调用我们的主程序.
    main.py作为程序入口程序(注:从极客学院的scrapy课程中学到的.)
    cnblogsSpider - 指向 cnblogsDemo.py 中定义的 name 值.
'''
from scrapy import cmdline
cmdline.execute("scrapy crawl cnblogsSpider".split())

