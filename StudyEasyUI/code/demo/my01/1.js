$(function () {
    //
    $.ajax({
        url: submitUrl,
        type: 'post',
        cache: false,
        dataType: 'json',
        contentType: "application/x-www-form-urlencoded;charset=UTF-8",
        data: parseFormInputData(),
        success: function (data) {
            if ("SUCESS" == data.msg) {
                var type = data.type;
                $("#" + type).click();
                $("#w").window("close");
            }
        },
        error: function () {
        }
    });
});