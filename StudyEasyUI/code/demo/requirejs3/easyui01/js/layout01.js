require.config({
    //baseUrl: 'js',
    shim: {
        //easyui-lang-zh_CN.js也依赖jquery
        'zhCN': ['jquery'],
        'easyui': ['jquery']
    },
    paths: {
        //引入的js文件不需要带入后缀'.js'
        jquery: '../../libs/easyui/jquery.min',
        easyui: '../../libs/easyui/jquery.easyui.min',
        zhCN: '../../libs/easyui/locale/easyui-lang-zh_CN'
    }
});
//require(['easyui']);
require(['jquery','zhCN','easyui'], function($,zhCN,easyui){
    $(function(){
        $('body').layout();
        $('body').layout('add',{
            region: 'east',
            width: 550,
            title: 'east',
            split: true,
            collapsible: false,
            tools: [
                {
                    iconCls: 'icon-add',
                    handler: function(){alert('add');}
                },
                {
                    iconCls: 'icon-remove',
                    handler: function(){alert('remove');}
                }
            ]
        });
        $('body').layout('add',{
            region: 'center',
            title: 'center',
            split: true,
            tools: [
                {
                    iconCls: 'icon-add',
                    handler: function(){alert('add');}
                },
                {
                    iconCls: 'icon-remove',
                    handler: function(){alert('remove');}
                }
            ]
        });
    });
});