require.config({
    //baseUrl: 'js',
    shim: {
        //easyui-lang-zh_CN.js也依赖jquery
        'zhCN': ['jquery'],
        'easyui': ['jquery']
    },
    paths: {
        //引入的js文件不需要带入后缀'.js'
        jquery: '../../libs/easyui/jquery.min',
        easyui: '../../libs/easyui/jquery.easyui.min',
        zhCN: '../../libs/easyui/locale/easyui-lang-zh_CN'
    }
});
//require(['easyui']);
require(['jquery','zhCN','easyui'], function($,zhCN,easyui){
    $(function(){
        //定义main-panel的ID
        var id_panel = 'id_main_panel';
        var BODY_W = $('body').width();
        var CLIENT_H = document.documentElement.clientHeight;
        $('body').append('<div id="{1}"></div>'.replace('{1}',id_panel));
        $('#'+id_panel).panel({
            width: (BODY_W - 30),
            height: (CLIENT_H - 45),
            border: true,
            closable: true,
            title: '门诊处方'
        });
        //定义main-layout的ID
        var id_layout = 'id_main_layout';
        var pbody = $('#'+id_panel).panel('body');
        pbody.append('<div id="{1}"></div>'.replace('{1}',id_layout));
        $('#'+id_layout).css({width:'100%',height:'100%'});
        $('#'+id_layout).layout();
        $('#'+id_layout).layout('add',{
            region: 'east',
            width: '30%',
            title: 'east',
            split: true,
            collapsible: false,
            tools: [
                {
                    iconCls: 'icon-add',
                    handler: function(){alert('add');}
                },
                {
                    iconCls: 'icon-remove',
                    handler: function(){alert('remove');}
                }
            ]
        });
        $('#'+id_layout).layout('add',{
            region: 'center',
            title: 'center',
            split: true,
            tools: [
                {
                    iconCls: 'icon-add',
                    handler: function(){alert('add');}
                },
                {
                    iconCls: 'icon-remove',
                    handler: function(){alert('remove');}
                }
            ]
        });
    });
});