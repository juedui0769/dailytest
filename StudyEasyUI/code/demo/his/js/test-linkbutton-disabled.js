$(function(){
    console.log("switchbutton");
    //不知什么原因，switchbutton不能使用。
    //$('#btn_switch').switchbutton({
    //    onText:'启用',
    //    offText:'禁用',
    //    checked:true,
    //    onChange:function(checked){
    //        console.log(checked);
    //    }
    //});

    $('#btn01').linkbutton({
        iconCls: 'icon-ok',
        text: '保存',
        onClick: function () {
            alert('"保存"按钮被点击了！');
        }
    });

    //给radio button绑定事件。
    $('input[name="chk"]').each(function(i,n){
        $(n).on('click',function(event){
            //console.log(arguments);
            //console.log(event.target);
            if($(event.target).val() == 'enable') {
                $('#btn01').linkbutton('enable');
            }else {
                $('#btn01').linkbutton('disable');
            }
        });
    });

    //触发“启用”按钮
    $($('input[name="chk"]')[0]).click();


    //$('#btn01').on('click', function(){
    //    alert('"保存"按钮被点击了！');
    //});
});