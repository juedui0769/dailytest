
$(function(){
    //获得需要响应"回车"的页面控件.
    var inputArr = $("#ff1").find("span > input[type!='hidden']");
    //
    var mappingArr = [];
    $.each(inputArr, function(i,n){
        var item = $(n);
        //"日期控件"会响应'回车',所以这里将所有的事件都事先清除掉.
        item.unbind();
        var siblingsObj = item.siblings("input[type='hidden']").eq(0);
        var obj={};
        obj.name = siblingsObj.attr("name");
        obj.obj = item;
        mappingArr.push(obj);
    });

    $(document).bind("keydown",function(){
        var args = arguments;
        var _event = args[0];
        if(_event.keyCode == 13){
            var target = $(_event.target);
            //alink_save
            if(!!target.attr("id") && "alink_save"==target.attr("id")){
                //
                alert("是否保存!s");
            }else{
                var targetSiblingsObj = target.siblings("input[type='hidden']").eq(0);
                var tarSiblingsVal = "";
                if(targetSiblingsObj && !!targetSiblingsObj.attr("name")){
                    tarSiblingsVal = targetSiblingsObj.attr("name");
                    $.each(mappingArr, function(i,n){
                        if( tarSiblingsVal == n.name ){
                            if(!!mappingArr[i+1]){
                                mappingArr[i+1].obj.focus();
                            }else{
                                $("#alink_save").focus();
                            }
                            return false; //break;
                        }
                    });
                }
            }
        }
    });
    //
    var applyTime = moment(); //制单日期
    $("#date1").datetimebox('setValue', applyTime.format("YYYY-MM-DD HH:mm:ss"));
    //
    var validityDate = moment();
    validityDate.year(validityDate.year()+2);
    $("#date2").datebox('setValue', validityDate.format("YYYY-MM-DD"));

    //给"供应商"赋值:
    $("#id_supplier").combobox({
        valueField: 'XH',
        textField: 'SUPPLIER_NAME',
        mode: 'local',
        data: [
            {
                'XH': 1,
                'SUPPLIER_NAME':'供应商A',
                'PYJM':'aa'
            },
            {
                'XH': 2,
                'SUPPLIER_NAME':'供应商B',
                'PYJM':'bb'
            },
            {
                'XH': 3,
                'SUPPLIER_NAME':'供应商C',
                'PYJM':'cc'
            },
            {
                'XH': 4,
                'SUPPLIER_NAME':'供应商d',
                'PYJM':'cca'
            },
            {
                'XH': 5,
                'SUPPLIER_NAME':'供应商e',
                'PYJM':'cac'
            },
            {
                'XH': 6,
                'SUPPLIER_NAME':'供应商f',
                'PYJM':'acc'
            }
        ],
        filter: function(q, row){
            q = q.toLowerCase();
            var pyjm = row['PYJM'].toLowerCase();
            //var opts = $(this).combobox('options');
            //console.log( row );
            //console.log( row['PYJM'].indexOf(q) );
            //console.log(opts);
            //console.log(opts.PYJM);
            //console.log( row[opts.PYJM].indexOf(q) );
            //row['PYJM'].indexOf(q)
            return row['PYJM'].indexOf(q) == 0;
        }
    });

    ///test
    $("#id_btn01").bind('click',function(){
        //toggle
        $(this).css("backgroundColor","red");
    });

});