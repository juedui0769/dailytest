/**
 * Created by WXG on 2015/6/18.
 */
$(function(){
    var options =  {
        content: "Some text", // text of the snackbar
        style: "toast", // add a custom class to your snackbar
        timeout: 2000 // time in milliseconds after the snackbar autohides, 0 is disabled
    };

    $.snackbar(options);
});