package com.wxg.demo;

import java.lang.management.ManagementFactory;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * 显示机器名.
 * @author sfit0442
 *
 */
public class ShowMachineName {

	private static final Logger LOG = LogManager.getLogger("HibernateStudy01");
	
	@Test
	public void showMachineName(){
		String serverName = ManagementFactory.getRuntimeMXBean().getName();// 机器名
		LOG.info( serverName );
	}
	
}
