package com.wxg.hibernate.d01;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.junit.Test;

import com.wxg.hibernate.d01.util.HibernateUtils4Test;
import com.yiibai.hibernate.d01.Employee;

/**
 * Hibernate学习案例.
 * @author sfit0442
 *
 */
public class HibernateStudy02 {

	private static final Logger LOG = LogManager.getLogger(HibernateStudy02.class);
	
	/**
	 * 默认方式
	 */
	@Test
	public void testInitConfigurationByDefault(){
		//LOG.info("===testInitConfigurationByDefault===");
		System.out.println("===testInitConfigurationByDefault===");
		Session session = HibernateUtils4Test.getSessionByDefaultConfiguration();
		try {
			@SuppressWarnings("unchecked")
			List<Employee> employees = session.createQuery("FROM Employee").list();
			LOG.info("size : " + employees.size());
		} finally{
			session.close();
		}
	}
	
	@Test
	public void testInitConfigurationByInputStringArgs(){
		System.out.println("===testInitConfigurationByInputStringArgs===");
		final String path = "/hibernate.cfg.xml";
		System.out.println(path);
		Session session = HibernateUtils4Test.getSessionByInputStrArgsConfiguration(path);
		try {
			@SuppressWarnings("unchecked")
			List<Employee> employees = session.createQuery("FROM Employee").list();
			LOG.info("size : " + employees.size());
		} finally{
			session.close();
		}
	}
	
	@Test
	public void testInitConfigurationByInputStringArgs2(){
		System.out.println("==={2}=testInitConfigurationByInputStringArgs2===");
		final String path = "/hibernate_2015-4-30.cfg.xml";
		System.out.println(path);
		Session session = HibernateUtils4Test.getSessionByInputStrArgsConfiguration(path);
		try {
			@SuppressWarnings("unchecked")
			List<Employee> employees = session.createQuery("FROM Employee").list();
			LOG.info("size : " + employees.size());
		} finally{
			session.close();
		}
	}
	
	@Test
	public void testInitConfigurationByInputStringArgs3(){
		System.out.println("==={3}=testInitConfigurationByInputStringArgs3===");
		final String path = "/com/wxg/hibernate/d01/hibernate.cfg.xml";
		System.out.println(path);
		Session session = HibernateUtils4Test.getSessionByInputStrArgsConfiguration(path);
		try {
			@SuppressWarnings("unchecked")
			List<Employee> employees = session.createQuery("FROM Employee").list();
			LOG.info("size : " + employees.size());
		} finally{
			session.close();
		}
	}
	
}
