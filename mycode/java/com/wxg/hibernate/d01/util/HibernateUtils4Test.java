package com.wxg.hibernate.d01.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtils4Test {


	/**
	 * 以默认方法获取Session
	 * @return
	 */
	public static Session getSessionByDefaultConfiguration(){
		Configuration cfg = new Configuration();
		SessionFactory factory = cfg.configure().buildSessionFactory();
		return factory.openSession();
	}
	
	/**
	 * 
	 * @return
	 */
	public static Session getSessionByInputStrArgsConfiguration(String cfgarg){
		Configuration cfg = new Configuration();
		SessionFactory factory = cfg.configure(cfgarg).buildSessionFactory();
		return factory.openSession();
	}
	
}
