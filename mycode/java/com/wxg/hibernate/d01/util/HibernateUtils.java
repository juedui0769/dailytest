package com.wxg.hibernate.d01.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * 操作hibernate的工具类
 * @author sfit0442
 *
 */
public class HibernateUtils {

	/**
	 * final static 
	 * 是否合适!
	 */
	private static final SessionFactory FACTORY = new Configuration().configure().buildSessionFactory();
	
	/**
	 * 获取session
	 * @return
	 */
	public static Session getHibernateSession(){
		return FACTORY.openSession();
	}
	
}
