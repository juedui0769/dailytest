package com.wxg.hibernate.d01;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.Ignore;
import org.junit.Test;

import com.wxg.hibernate.d01.util.HibernateUtils;
import com.yiibai.hibernate.d01.Employee;

public class HibernateStudy01 {

	private static final Logger LOG = LogManager.getLogger("HibernateStudy01");
	
	@Test
	public void showHibernateVersion(){
		//输出hibernate版本.
		LOG.info(org.hibernate.Version.getVersionString());
		//Assert.assertEquals("3.6.10.Final", org.hibernate.Version.getVersionString());
		//Collections.unmodifiableList(new ArrayList());
	}
	
	@Test
	public void complain(){
		LOG.info("我没法按照'敏捷开发'上的那样去写测试用例");
		LOG.info("红-绿-重构循环");
	}
	
	/**
	 * 全手写,获取factory,获取session
	 */
	@Test
	@Ignore
	public void testOptHibernateMysql(){
		SessionFactory factory = null;
		factory = new Configuration().configure().buildSessionFactory();
		Session session = factory.openSession();
		try {
			@SuppressWarnings("unchecked")
			List<Employee> employees = session.createQuery("FROM Employee").list();
			for (Iterator<Employee> iterator = employees.iterator(); iterator.hasNext();) {
				Employee employee = iterator.next();
				System.out.print("First Name: " + employee.getFirstName());
				System.out.print("  Last Name: " + employee.getLastName());
				System.out.println("  Salary: " + employee.getSalary());
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
	
	/**
	 * 测试自己手写的 HibernateUtils
	 */
	@Test
	@Ignore
	public void testOptHibernateByUtils(){
		Session session = HibernateUtils.getHibernateSession();
		try {
			@SuppressWarnings("unchecked")
			List<Employee> employees = session.createQuery("FROM Employee").list();
			LOG.info("size : " + employees.size());
		} finally{
			session.close();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
