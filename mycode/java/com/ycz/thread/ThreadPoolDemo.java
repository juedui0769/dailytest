package com.ycz.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 阳春知作品
 * 
 * @author sfit0442
 * 
 */
public class ThreadPoolDemo {

	public static void main(String[] args) {
		ExecutorService executorService = null;// Executors.newScheduledThreadPool(3);
		
		// 定时器线程池
		executorService = Executors.newScheduledThreadPool(3);
		
		/*
		// 每次只跑三个线程 固定线程池
		executorService = Executors.newFixedThreadPool(3);
		// 缓存线程池
		executorService = Executors.newCachedThreadPool();
		// 单一线程池
		executorService = Executors.newSingleThreadExecutor();
		*/
		
		
		for (int i = 0; i < 20; i++) {
			final int task = i;
			executorService.execute(new Runnable() {
				@Override
				public void run() {
					int j = 0;
					while (j < 10) {
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						System.out.println(Thread.currentThread().getName() + "  task  [" + task + "]  j   {" + (j++)
								+ "}");
					}
				}
			});
		}
		executorService.shutdown();
		//executorService.shutdownNow();
		
		
		
	}
}
