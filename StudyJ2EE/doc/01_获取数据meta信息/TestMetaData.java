package com.wxg;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TestMetaData {

    public static void main(String[] args) {
        //oracle.jdbc.driver.OracleDriver
        //com.microsoft.sqlserver.jdbc.SQLServerDriver
        String DbDriver = "com.mysql.jdbc.Driver";
        //DbDriver = "oracle.jdbc.driver.OracleDriver";
        DbDriver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        //jdbc:mysql://localhost:3306/test
        //jdbc:sqlserver://localhost:1433;DatabaseName=HIS_HD
        //jdbc:oracle:thin:@localhost:1521:orcl
        String url = "jdbc:mysql://localhost:3306/test";
        url = "jdbc:sqlserver://localhost:1433;DatabaseName=HIS_HD";
        String user = "sa";
        String password = "hisadmin";
        //
        Connection conn = null;
        ResultSet rs = null;
        try {
            Class.forName(DbDriver);
            
            //
            conn = DriverManager.getConnection(url, user, password);
            DatabaseMetaData dbmetadata = conn.getMetaData();
            //
            String[] types = {"TABLE"};
            rs = dbmetadata.getTables(null, null, null, types);
            String tablename = null;
            while(rs.next()){
                tablename = rs.getString("TABLE_NAME");
                System.out.println(tablename);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally{
            try {
                if(null != rs){
                    rs.close();
                }
                if (null != conn){
                    conn.close();
                }
                System.out.println("==================");
                System.out.println(conn.isClosed());
                System.out.println(rs.isClosed());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
