﻿
*
less的安装
--
npm install -g less
--
要先安装"node",然后使用"npm"安装即可.
--
npm 在0.6之后就内建到node.js中了,所以发现npm命令不管用,请关闭cmd再打开.

*
"Less"的文档比"Sass"要多,被翻译成中文的也不少,但是有优劣,要甄别.

*
http://www.bootcss.com/p/lesscss/
--
这篇还不错.

*
在"WebStorm"中如何制定"less"的watcher呢?!
--
请看这篇"http://www.cnblogs.com/fxair/p/3708074.html"
Watcher Settings
  File type: Less
  Scope: Project Files
  Program: C:\Program Files\nodejs\node.exe
  Arguments: C:\Users\WXG\AppData\Roaming\npm\node_modules\less\bin\lessc --no-color $FileName$
  Working directory: $FileDir$
  Environment variables: 
  Output paths to refresh: $FileNameWithoutExtension$.css
--


