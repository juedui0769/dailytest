
*
Could not find artifact com.oracle:ojdbc14:jar:10.2.0.5.0

*
https://sites.google.com/site/olivabianco/java-blog/installoraclejarfileinyourmavenrepository

*
'packaging' is missing.

*


*
原文：

 Install oracle jar file in your maven repository
publicado a la‎(s)‎ 30 may. 2012 7:06 por Daniel Oliva Bianco
The folowing steps are needed to solve the issue with maven dependencies related to oracle files (they are not available for download from http://repo1.maven.org/maven2/com/oracle/ojdbc14/ )

1- You can download your preferred driver from Oracle site:
http://www.oracle.com/technetwork/database/features/jdbc/index-091264.html
or get the Oracle Database 10g 10.1.0.5 JDBC Drivers attached below (it also works for 10.1.0.4, 10.1.0.3 and 10.1.0.2)

2- then you have to open a window console and run the following command

mvn install:install-file -Dfile=ojdbc14dms_g.jar -DgroupId=com.oracle -DartifactId=ojdbc14 -Dversion=10.2.0.5.0 -Dpackaging=jar -DgeneratePom=true

in red are the properties you have to change to match with your pom file, for the command above, the pom file should be:

        <dependency>
            <groupId>com.oracle</groupId>
            <artifactId>ojdbc14</artifactId>
            <version>10.2.0.5.0</version>
        </dependency>

3- Now you have the dependency installed in your local repository, so now you are able to run maven commands

Note: if you have some Continuous Integration tool you will have to install the oracle jar file as well

----------------------------------------------------

<dependency>
  <groupId>ojdbc</groupId>
  <artifactId>ojdbc</artifactId>
  <version>14</version>
  <type>pom</type>
</dependency>

mvn install:install-file -Dfile=ojdbc14.jar -DgroupId=ojdbc -DartifactId=ojdbc -Dversion=14 -Dpackaging=jar  -DgeneratePom=true

----------------------------------------------------

<dependency>
  <groupId>com.oracle</groupId>
  <artifactId>ojdbc14</artifactId>
  <version>10.2.0.5.0</version>
</dependency>

mvn install:install-file -Dfile=ojdbc14.jar -DgroupId=com.oracle -DartifactId=ojdbc14 -Dversion=10.2.0.5.0 -Dpackaging=jar -DgeneratePom=true

----------------------------------------------------

* 执行命令后，结果如下：

[INFO] --- maven-install-plugin:2.4:install-file (default-cli) @ standalone-pom ---
[INFO] Installing C:\wxg\pom\01_upload_ojdbc_jar\ojdbc14.jar to D:\tools\maven\repository\repos\ojdbc\ojdbc\14\ojdbc-14.jar
[INFO] Installing C:\Users\admin\AppData\Local\Temp\mvninstall5626954704021760097.pom to D:\tools\maven\repository\repos\ojdbc\ojdbc\14\ojdbc-14.pom



