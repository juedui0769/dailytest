package com.sf.tools.ui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;
import javax.swing.JTextField;

import com.sf.tools.common.CheckI18NProgressHandler;
import com.sf.tools.common.ConnectionFactory;
import com.sf.tools.generate.DataCreator;


public class DataDictionary implements ActionListener{

	private static final long serialVersionUID = 1L;
	
	private JPanel con = new JPanel();
	
	private JProgressBar task_progress = new JProgressBar();
	private JButton generate = new JButton("生成");
	
	private JLabel label2 = new JLabel("ConnectionURL：");
	private JLabel label3 = new JLabel("用户名：");
	private JLabel label4 = new JLabel("密码：");
	
	private JTextField text2 = new JTextField("jdbc:oracle:thin:@xxx.xxx.xxx.xxx:1521:dbName");
	private JTextField text3 = new JTextField("");
	private JPasswordField text4 = new JPasswordField("");
	private JButton testBtn = null; // 测试
	
	private JFileChooser jfc = new JFileChooser();
	private JTextField text1 = new JTextField();
	private JButton button1 = new JButton("浏览");
	
	private String filePath;
	
	DataDictionary(){
		JPanel source = new JPanel();
        source.setLayout(null);
        source.setPreferredSize(new java.awt.Dimension(780, 120));
        source.setBorder(BorderFactory.createTitledBorder("数据源"));
        
        JPanel genePath = new JPanel();
        genePath.setLayout(null);
        genePath.setPreferredSize(new java.awt.Dimension(780, 70));
        genePath.setBorder(BorderFactory.createTitledBorder("保存路径"));
        
        testBtn = new JButton("测试");
        
        label2.setBounds(30, 20, 100, 20);
        text2.setBounds(130, 20, 300, 20);
        
        label3.setBounds(30, 50, 100, 20);
        text3.setBounds(130, 50, 100, 20);
        
        label4.setBounds(30, 80, 100, 20);
        text4.setBounds(130, 80, 100, 20);
        text4.setEchoChar('*');
        testBtn.setBounds(360, 80, 70, 22);
        
        text1.setBounds(30, 30, 670, 20);text1.setEditable(false);
        button1.setBounds(710, 30, 60, 22);
        
        JPanel progress = new JPanel();
        progress.setLayout(null);
        progress.setPreferredSize(new java.awt.Dimension(780, 100));
        task_progress.setBounds(4, 20, 695, 22);
		generate.setBounds(705, 20, 60, 22);	generate.setEnabled(false);
		progress.add(task_progress);
		progress.add(generate);
        
        source.add(label2);
        source.add(text2);
        
        source.add(label3);
        source.add(text3);
        
        source.add(label4);
        source.add(text4);
        source.add(testBtn);
        
        genePath.add(text1);
        genePath.add(button1);
        
        testBtn.addActionListener(this);
        generate.addActionListener(this);
        button1.addActionListener(this);
        
		con.add(source,BorderLayout.NORTH);
		con.add(genePath,BorderLayout.CENTER);
		con.add(progress,BorderLayout.SOUTH);
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(testBtn)){
			String url = text2.getText();
        	String user = text3.getText();
        	String pass = new String(text4.getPassword());
        	String driver = "oracle.jdbc.driver.OracleDriver";
        	
        	boolean bool = ConnectionFactory.getIntance().testConnect(driver, url, user, pass);
        	if(bool){
        		generate.setEnabled(true);
        		JOptionPane.showMessageDialog(new JFrame("提示"), "连接成功!");
        	}else{
        		JOptionPane.showMessageDialog(new JFrame("提示"), "连接失败!");
        	}
		}else if(e.getSource().equals(button1)){
			jfc.setDialogTitle("选择文件夹") ;
			jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			int state = jfc.showOpenDialog(con);
			if(state == JFileChooser.APPROVE_OPTION){	// 选择的是确定按钮
				File f = jfc.getSelectedFile();
				text1.setText(f.getAbsolutePath());
			}
		}else if(e.getSource().equals(generate)){
			String path = text1.getText();
			if(path == null || "".equals(path.trim())){
				JOptionPane.showMessageDialog(new JFrame("提示"), "请选择要生成的目录!");
				return ;
			}
			task_progress.setValue(0);
			generate.setEnabled(false);
			
			String url = text2.getText();
        	String user = text3.getText();
        	String pass = new String(text4.getPassword());
        	String driver = "oracle.jdbc.driver.OracleDriver";
        	
        	filePath = text1.getText()+File.separator+UUID.randomUUID().toString()+".xls";;
			DataDictionary.this.start(url,user,pass,driver,filePath);
			
		}
		
	}

	public void start(final String url, final String user, final String pass, final String driver, final String path){
		final DataCreator cf = new DataCreator();
		cf.setProgressHandler(new CheckI18NProgressHandler() {
			@Override
			public void loadTaskCount(int count, File file) {
				task_progress.setMaximum(count);
				task_progress.setValue(0);
			}
			@Override
			public void doneTaskCount() {
				task_progress.setValue(task_progress.getValue() + 1);
			}
		});
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				cf.start(url,user,pass,driver,path);
				try {
					Thread.sleep(500);
					generate.setEnabled(true);
//					task_progress.setValue(0);
					
//					JOptionPane.showMessageDialog(new JFrame("提示"), "数据字典已成功生成!");
					int statu = JOptionPane.showConfirmDialog(null, 
							"数据字典已成功生成，是否打开?", "提示", JOptionPane.YES_NO_OPTION);
					if(statu == JOptionPane.OK_OPTION){
						try {
							Runtime.getRuntime().exec("rundll32 SHELL32.DLL,ShellExec_RunDLL Explorer.exe /select," + filePath);
							java.awt.Desktop.getDesktop().open(new File(filePath));
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				}
				
			}
		});
		
		thread.start();
	}
	public Container getCon() {
		return con;
	}

}
