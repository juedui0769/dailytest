package com.sf.tools.ui;

import java.awt.Container;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.ScrollPaneConstants;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;

import net.sf.json.JSONArray;
import net.sf.json.JSONFunction;
import net.sf.json.JSONNull;
import net.sf.json.JSONObject;



public class JsonView {

	private static final long serialVersionUID = 1L;
	
	private JTree tree;
	private DefaultTreeModel treeModel;
	
	private JScrollPane scrollPane1;
	private JScrollPane scrollPane2 = new JScrollPane();
	private JTextArea area = new JTextArea();
	
	private JPanel con = new JPanel();
	
	private DefaultMutableTreeNode root = new DefaultMutableTreeNode("JSON");
	
	JsonView(){

		treeModel = new DefaultTreeModel(root);
		tree = new JTree(treeModel);
		tree.setShowsRootHandles(true);
		tree.setRootVisible(false);
		
		scrollPane1 = new JScrollPane(tree);
		scrollPane1.setPreferredSize(new java.awt.Dimension(550, 800));
		scrollPane2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		scrollPane2.setPreferredSize(new java.awt.Dimension(550, 800));
		scrollPane2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		area.setWrapStyleWord(true);
		area.setLineWrap(true);
		scrollPane2.setViewportView(area);
		
		JSplitPane jSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,scrollPane1, scrollPane2);

		jSplitPane.setDividerSize(8); // 设置分割条的宽度

		// 设置continuousLayout属性的值，在用户干预要使子组件连续地重新显示和布局子组件

		// jSplitPane.setContinuousLayout(true);

		// 分隔面板的分隔条显示出箭头

		jSplitPane.setOneTouchExpandable(true);

		jSplitPane.setDividerLocation(560);
		
		jSplitPane.setBounds(0, 0, 1150, 800);
		
		area.addKeyListener(new KeyListener(){
			public void keyReleased(KeyEvent e) {
				try {
					actionPerformed();
				} catch (Exception e2) {
					root.removeAllChildren();
					tree.updateUI();
				}
			}
			public void keyTyped(KeyEvent e) {
				try {
					actionPerformed();
				} catch (Exception e2) {
					root.removeAllChildren();
					tree.updateUI();
				}
			}
			public void keyPressed(KeyEvent e) {
				
			}
		});
		
		con.setLayout(null);
		con.add(jSplitPane);
	}
	
	public Container getCon() {
		return con;
	}

	public void parse(JSONObject jsonObject, String spa, DefaultMutableTreeNode parent){
		JSONArray names = jsonObject.names();
		for(int i=0;i<names.size();i++){
			Object object = jsonObject.get(names.get(i));
//System.out.println(object + " --- " + object.getClass());
			String node = spa+names.get(i);
			if(object instanceof Integer || object instanceof Long || object instanceof Double || object instanceof JSONNull){
//System.out.println(spa+names.get(i)+" : "+object);
				node = node + " : "+object;
			}else if(object instanceof String){
//System.out.println(spa+names.get(i)+" : "+"\""+object+"\"");
				node = node + " : "+"\""+object+"\"";
			}else if(object instanceof JSONArray){
//System.out.println(spa+names.get(i)+"[] : ");
				node = node + "[]";
			}else if(object instanceof JSONObject){
//System.out.println(spa+names.get(i)+" : ");
				node = node + "";
			}else if(object instanceof JSONFunction){
				node = node + " : "+object;
System.out.println("------------------" + object.toString().replace("\n", ""));
			}
//System.out.println(node + "--------" + parent.getUserObject());			
			DefaultMutableTreeNode subroot = new DefaultMutableTreeNode(node);
			parent.add(subroot);
			if(object instanceof JSONArray){
				parse((JSONArray)object,spa+"\t",subroot);
			}else if(object instanceof JSONObject){
				parse((JSONObject)object,spa+"\t",subroot);
			}
		}
	}
	
	public void parse(JSONArray jsonArray, String spa, DefaultMutableTreeNode parent){
		for(int k = 0; k < jsonArray.size(); k++) {
			Object object = (Object) jsonArray.get(k);
//System.out.println(spa+"["+k+"]" + "=======1" + parent.getUserObject());
			DefaultMutableTreeNode subroot = new DefaultMutableTreeNode("["+k+"]");
			parent.add(subroot);
			if(object instanceof Integer || object instanceof Long || object instanceof Double || object instanceof JSONNull){
//System.out.println(spa+object + "=======2" + parent.getUserObject());
				DefaultMutableTreeNode subroot2 = new DefaultMutableTreeNode(object);
				subroot.add(subroot2);
			}else if(object instanceof String){
//System.out.println(spa+"\""+object+"\"" + "=======3" +"["+k+"]");
				DefaultMutableTreeNode subroot2 = new DefaultMutableTreeNode("\""+object+"\"");
				subroot.add(subroot2);
			}
			
			if(object instanceof JSONArray){
				parse((JSONArray)object,spa+"\t",subroot);
			}else if(object instanceof JSONObject){
				parse((JSONObject)object,spa+"\t",subroot);
			}
		}
	}
	
	public void actionPerformed() {
		root.removeAllChildren();
//			root.removeFromParent();
		String json = area.getText();
		if(json.startsWith("{")){
			JSONObject jsonObject = JSONObject.fromObject(json);
//				System.out.println("1:"+jsonObject);
			parse(jsonObject,"",root);
			
		}else if(json.startsWith("[")){
			JSONArray jsonArray = JSONArray.fromObject(json);
//				System.out.println("2:"+jsonArray);
			parse(jsonArray,"",root);
		}
		
//			tree.repaint();
//			tree.removeAll();
		tree.updateUI();
		tree.expandRow(0);
		
		DefaultTreeCellRenderer cellRenderer = (DefaultTreeCellRenderer) tree.getCellRenderer();
		cellRenderer.setClosedIcon(null);
		
	}

}
