package com.sf.tools.ui;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.sf.tools.generate.I18N;



public class Internationalization implements ActionListener{

	private static final long serialVersionUID = 1L;
	
	private JPanel con = new JPanel();

	private JLabel label1 = new JLabel("前缀字符串:");
	private JTextField text1 = new JTextField();
	private JLabel label4 = new JLabel();
	
	private JFileChooser jfc = new JFileChooser();
	private JLabel label2 = new JLabel("被国际化的文件:");
	private JTextField text2 = new JTextField();
	private JButton button2 = new JButton("...");
	private JLabel label5 = new JLabel();
	
	private JButton button1 = new JButton("开始国际化");
	
	private JFileChooser jfc2 = new JFileChooser();
	private JLabel label3 = new JLabel("去除国际化文件:");
	private JTextField text3 = new JTextField();
	private JButton button3 = new JButton("...");
	
	private JButton button4 = new JButton("去除国际化");
	
	Internationalization(){
		
		//第一行 begin
		label1.setBounds(30, 10, 90, 20);
		text1.setBounds(120, 10, 540, 20);
		label4.setForeground(Color.RED);
		label4.setText("<html>说明：${app:i18n_def('ReportItemRpt.5','提示')},则前缀字符串为“ReportItemRpt”</html>");
		label4.setBounds(120, 20, 540, 40);
		//第一行 end
		
		//第二行 begin
		label2.setBounds(10, 60, 110, 20);
		text2.setBounds(120, 60, 540, 20); text2.setEditable(false);
		label5.setForeground(Color.RED);
		label5.setText("<html>说明：1：国际化的文件只能是js文件；<br>&nbsp;&nbsp;&nbsp;2：国际化后的文件为I18N_+国际化的文件名，例如：user.js,国际化后的文件为：I18N_user.js.</html>");
		label5.setBounds(120, 80, 560, 40);
		button2.setBounds(670, 60, 50, 20);
		//第二行 end
		
		button1.setBounds(560, 130, 100, 23);
		
		//第二行 begin
		label3.setBounds(10, 310, 110, 20);
		text3.setBounds(120, 310, 540, 20); text3.setEditable(false);
		button3.setBounds(670, 310, 50, 20);
		//第二行 end
		
		button4.setBounds(560, 350, 100, 23);
		
		button1.addActionListener(this);
		button2.addActionListener(this);
		button3.addActionListener(this);
		button4.addActionListener(this);
		
		con.setLayout(null);
		con.add(label1);
		con.add(text1);
		con.add(label4);
		con.add(label2);
		con.add(text2);
		con.add(button2);
		con.add(label5);
		con.add(button1);
//		con.add(label3);
//		con.add(text3);
//		con.add(button3);
//		con.add(button4);
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(button2)){
			jfc.setDialogTitle("选择文件或目录") ;
			jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			int state = jfc.showOpenDialog(con);
			if(state == JFileChooser.APPROVE_OPTION){	// 选择的是确定按钮
				File f = jfc.getSelectedFile();
				text2.setText(f.getAbsolutePath());
			}
		}else if(e.getSource().equals(button1)){
			String prefix = text1.getText();
			String filePath = text2.getText();
			if(prefix == null || "".equals(prefix.trim())){
				JOptionPane.showMessageDialog(new JFrame("提示"), "前缀字符串不能为空!");
				return ;
			}
			if(filePath == null || "".equals(filePath.trim())){
				JOptionPane.showMessageDialog(new JFrame("提示"), "请选择要国际化的文件或目录!");
				return ;
			}
			I18N.MARK = prefix;
			I18N.PATH = filePath;
			I18N.start();
//			JOptionPane.showMessageDialog(new JFrame("提示"), result);
			int statu = JOptionPane.showConfirmDialog(null, 
					"恭喜你,国际化成功!是否打开?", "提示", JOptionPane.YES_NO_OPTION);
			if(statu == JOptionPane.OK_OPTION){
				try {
//					Runtime.getRuntime().exec("rundll32 SHELL32.DLL,ShellExec_RunDLL Explorer.exe /select," + filePath);
					File file = new File(filePath);
					if(file.isFile()){
						file = file.getParentFile();
					}
					java.awt.Desktop.getDesktop().open(file);
				} catch (IOException e3) {
					e3.printStackTrace();
				}
			}
		}else if(e.getSource().equals(button3)){
			jfc2.setDialogTitle("选择文件或目录") ;
			jfc2.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			int state = jfc2.showOpenDialog(null);
			if(state == JFileChooser.APPROVE_OPTION){	// 选择的是确定按钮
				File f = jfc2.getSelectedFile();
				text3.setText(f.getAbsolutePath());
			}
		}else if(e.getSource().equals(button4)){
			String filePath = text3.getText();
			if(filePath == null || "".equals(filePath.trim())){
				JOptionPane.showMessageDialog(new JFrame("提示"), "请选择要国际化的文件或目录!");
				return ;
			}
			I18N.REVER_PATH = filePath;
			String result = I18N.reverI18N();
			JOptionPane.showMessageDialog(new JFrame("提示"), result);
		}
	}

	public Container getCon() {
		return con;
	}

}
