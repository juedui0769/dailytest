package com.sf.tools.ui;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import com.sf.tools.generate.Conver;


public class Conversion  implements ActionListener{

	private static final long serialVersionUID = 1L;
	
	private JScrollPane scrollPane1 = new JScrollPane();
	private JScrollPane scrollPane2 = new JScrollPane();
	private JTextArea area1 = new JTextArea();
	private JTextArea area2 = new JTextArea();
	
	private JPanel con = new JPanel();
	
	private JComboBox combo = new JComboBox();
	private JButton conver = new JButton("转换");
	
	
	Conversion(){
		combo.setModel(new DefaultComboBoxModel(new String[] {"简体->繁体", "繁体->简体", "汉字->Unicode", "Unicode->汉字"}));
//		combo.setEditable(true);
		
		scrollPane1.setBounds(100, 10, 540, 250);
		scrollPane1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		area1.setWrapStyleWord(true);
		area1.setLineWrap(true);
		scrollPane1.setViewportView(area1);

		combo.setBounds(440, 270, 110, 25);
		conver.setBounds(560, 270, 80, 25);
		
		scrollPane2.setBounds(100, 305, 540, 250);
		scrollPane2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		area2.setWrapStyleWord(true);
		area2.setLineWrap(true);
		area2.setEditable(false);
		scrollPane2.setViewportView(area2);
		
		
		conver.addActionListener(this);
		
		con.setLayout(null);
		con.add(scrollPane1);
		con.add(combo);
		con.add(conver);
		con.add(scrollPane2);
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(conver)){
//			System.out.println(area1.getText());
//			System.out.println(area2.getText());
			String type = (String) combo.getSelectedItem();
			if("简体->繁体".equals(type)){
				area2.setText(Conver.toFanTiString(area1.getText()));
			}else if("繁体->简体".equals(type)){
				area2.setText(Conver.toJianTiString(area1.getText()));
			}else if("汉字->Unicode".equals(type)){
				area2.setText(Conver.toUnicode(area1.getText()));
			}else if("Unicode->汉字".equals(type)){
				area2.setText(Conver.toGBK(area1.getText()));
			}
//			System.out.println("阿萨德阿萨德");
		}
	}

	public Container getCon() {
		return con;
	}

}
