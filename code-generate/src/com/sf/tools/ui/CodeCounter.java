package com.sf.tools.ui;

import java.awt.Color;
import java.awt.Container;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;

import com.sf.tools.common.CheckI18NProgressHandler;
import com.sf.tools.generate.CodeCount;


public class CodeCounter implements ActionListener{

	private static final long serialVersionUID = 1L;
	
	private JFileChooser jfc = new JFileChooser();
	private JLabel label1 = new JLabel("统计文件或目录:");
	private JTextField text1 = new JTextField();
	private JButton button1 = new JButton("...");
	
	private JLabel label2 = new JLabel("文件后缀名:");
	private String[] suffixs = new String[]{".java",".js","html",".jsp",".xml",".properties"};
	private JCheckBox[] boxs = new JCheckBox[suffixs.length];
	
	private JLabel label3 = new JLabel("自定义后缀名:");
	private JTextField text2 = new JTextField();
	private JLabel label4 = new JLabel();
	
	private JPanel pnl_showPicture = new JPanel();				// 创建显示图片的面板
//	private JTextArea log_area = new JTextArea();
	
	private JPanel con = new JPanel();
	
	private JProgressBar task_progress = new JProgressBar();
	private JButton check = new JButton("统计");
	
//	private JButton openResultFile = new JButton("打开文件");
//	private File saveLogFile = null;
	
	CodeCounter(){
		//第一行 begin
		label1.setBounds(10, 10, 110, 20);
		text1.setBounds(120, 10, 580, 20); text1.setEditable(false);
		button1.setBounds(710, 10, 60, 20);
		//第一行 end
		
		label2.setBounds(10, 50, 110, 20);
		for (int i=0,columnWidth=0;i<suffixs.length;i++,columnWidth+=70) {
			boxs[i] = new JCheckBox(suffixs[i],true);
			if(i == suffixs.length - 1){
				boxs[i].setBounds(120+columnWidth, 50, 100, 20);
			}else{
				boxs[i].setBounds(120+columnWidth, 50, 70, 20);
			}
		}
		
		
		label3.setBounds(10, 90, 110, 20);
		text2.setBounds(120, 90, 580, 20);
		
		label4.setBounds(120, 100, 580, 30);
		label4.setForeground(Color.RED);
		label4.setText("<html>说明：多个之间用逗号分开.例如：java,js,css</html>");
        
		
		task_progress.setBounds(10, 140, 690, 22);
		check.setBounds(710, 140, 60, 22);
		
//		pnl_showPicture.setBounds(10, 130, 760, 450);
		pnl_showPicture.setBounds(125, 185, 550, 400);
		
//		scrollPane1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
//		scrollPane1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
//		log_area.setWrapStyleWord(true);
//		log_area.setLineWrap(true);
//		log_area.setEditable(false);
//		scrollPane1.setViewportView(log_area);

//		openResultFile.setBounds(690, 550, 80, 22);
//		openResultFile.setVisible(false);
		
		button1.addActionListener(this);
		check.addActionListener(this);
		
//		openResultFile.addActionListener(this);
		
		con.add(label1);
		con.add(text1);
		con.add(label2);
		for (JCheckBox box : boxs) {
			box.addActionListener(this);
			con.add(box);
		}
		con.setLayout(null);
		con.add(label3);
		con.add(text2);
		con.add(label4);
		con.add(button1);
//		con.add(scrollPane1);
		con.add(pnl_showPicture);
		con.add(task_progress);
		con.add(check);
//		con.add(openResultFile);
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(button1)){
			jfc.setDialogTitle("选择文件或目录") ;
			jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			int state = jfc.showOpenDialog(con);
			if(state == JFileChooser.APPROVE_OPTION){	// 选择的是确定按钮
				File f = jfc.getSelectedFile();
				text1.setText(f.getAbsolutePath());
			}
		}else if(e.getSource().equals(check)){
			String path = text1.getText();
			if(path == null || "".equals(path.trim())){
				JOptionPane.showMessageDialog(new JFrame("提示"), "请选择要统计的文件或目录!");
				return ;
			}
			
			String temp = "";
			StringBuffer sb = new StringBuffer();
			
			String suffix = text2.getText().replace("，", ",").trim();
			for (int i=0;i<boxs.length;i++) {
				if(boxs[i].isSelected()){
					sb.append(suffixs[i]).append(",");
				}
			}
			
			sb.append(suffix);
			if(sb.length() > 0){
				temp = sb.substring(0, sb.length());
			}
			
			task_progress.setValue(0);
//			log_area.setText("");
			check.setEnabled(false);
//			openResultFile.setVisible(false);
//			PrintUtil.redirectSystemStreams(log_area,CodeCount.LOG_MARK);
//			CheckFile.checkI18N(path);
			pnl_showPicture.removeAll();
			pnl_showPicture.repaint();
			
			CodeCounter.this.start(path,temp);
			
		}
//		else if(e.getSource().equals(openResultFile)){
//			try {
////				java.awt.Desktop.getDesktop().open(saveLogFile);
//				Runtime.getRuntime().exec("rundll32 SHELL32.DLL,ShellExec_RunDLL Explorer.exe /select," + saveLogFile.getAbsolutePath());
//			} catch (IOException e1) {
//				e1.printStackTrace();
//			}
//		}
		
	}

	public void start(final String path, final String suffix){
		final CodeCount cf = new CodeCount();
		cf.setProgressHandler(new CheckI18NProgressHandler() {
			@Override
			public void loadTaskCount(int count, File file) {
				task_progress.setMaximum(count);
				task_progress.setValue(0);
//				saveLogFile = file;
			}
			@Override
			public void doneTaskCount() {
				task_progress.setValue(task_progress.getValue() + 1);
//				System.out.println("CODE_COUNT progress: " + task_progress.getValue());
			}
		});
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				String imagePath = "";
				try {
					imagePath = cf.start(path,suffix);
					
					Image image = Toolkit.getDefaultToolkit().createImage(imagePath);
					ImageIcon icon = new ImageIcon(image);		// 实例化图像对象
					pnl_showPicture.add(new JLabel(icon));		// 把显示图片的标签放到显示图片面板上
					pnl_showPicture.revalidate();
					
					Thread.sleep(500);
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException(e);
				} finally {
					check.setEnabled(true);
//					task_progress.setValue(0);
					File f = new File(imagePath);
					f.delete();
				}
				
			}
		});
		
		thread.start();
	}
	public Container getCon() {
		return con;
	}

}
