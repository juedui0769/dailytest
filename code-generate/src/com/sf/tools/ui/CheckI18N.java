package com.sf.tools.ui;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import com.sf.tools.common.CheckI18NProgressHandler;
import com.sf.tools.common.PrintUtil;
import com.sf.tools.generate.CheckFile;


public class CheckI18N implements ActionListener{

	private static final long serialVersionUID = 1L;
	
	private JFileChooser jfc = new JFileChooser();
	private JLabel label1 = new JLabel("检查文件或目录:");
	private JTextField text1 = new JTextField();
	private JButton button1 = new JButton("...");
	
	private JScrollPane scrollPane1 = new JScrollPane();
	private JTextArea log_area = new JTextArea();
	
	private JPanel con = new JPanel();
	
	private JProgressBar task_progress = new JProgressBar();
	private JButton check = new JButton("检测");
	
	private JButton openResultFile = new JButton("打开文件");
	private File saveLogFile = null;
	
	CheckI18N(){
		//第一行 begin
		label1.setBounds(10, 10, 110, 20);
		text1.setBounds(120, 10, 580, 20); text1.setEditable(false);
		button1.setBounds(710, 10, 60, 20);
		//第一行 end
		
		GridBagConstraints gbc_task_progress = new GridBagConstraints();
		gbc_task_progress.ipady = 8;
		gbc_task_progress.fill = GridBagConstraints.BOTH;
		gbc_task_progress.gridwidth = 8;
		gbc_task_progress.insets = new Insets(0, 5, 5, 5);
		gbc_task_progress.gridx = 0;
		gbc_task_progress.gridy = 2;
		
		task_progress.setBounds(10, 50, 690, 22);
		check.setBounds(710, 50, 60, 22);
		
		scrollPane1.setBounds(10, 90, 760, 450);
		scrollPane1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		log_area.setWrapStyleWord(true);
		log_area.setLineWrap(true);
		log_area.setEditable(false);
		scrollPane1.setViewportView(log_area);

		openResultFile.setBounds(690, 550, 80, 22);
		openResultFile.setVisible(false);
		
		button1.addActionListener(this);
		check.addActionListener(this);
		openResultFile.addActionListener(this);
		
		con.setLayout(null);
		con.add(label1);
		con.add(text1);
		con.add(button1);
		con.add(scrollPane1);
		con.add(task_progress);
		con.add(check);
		con.add(openResultFile);
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(button1)){
			jfc.setDialogTitle("选择文件或目录") ;
			jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			int state = jfc.showOpenDialog(con);
			if(state == JFileChooser.APPROVE_OPTION){	// 选择的是确定按钮
				File f = jfc.getSelectedFile();
				text1.setText(f.getAbsolutePath());
			}
		}else if(e.getSource().equals(check)){
			String path = text1.getText();
			if(path == null || "".equals(path.trim())){
				JOptionPane.showMessageDialog(new JFrame("提示"), "请选择要检测的文件或目录!");
				return ;
			}
			
			task_progress.setValue(0);
			log_area.setText("");
			check.setEnabled(false);
			openResultFile.setVisible(false);
			PrintUtil.redirectSystemStreams(log_area,CheckFile.LOG_MARK);
//			CheckFile.checkI18N(path);
			CheckI18N.this.start(path);
			
		}else if(e.getSource().equals(openResultFile)){
			try {
//				java.awt.Desktop.getDesktop().open(saveLogFile);
				Runtime.getRuntime().exec("rundll32 SHELL32.DLL,ShellExec_RunDLL Explorer.exe /select," + saveLogFile.getAbsolutePath());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		
	}

	public void start(final String path){
		final CheckFile cf = new CheckFile();
		cf.setProgressHandler(new CheckI18NProgressHandler() {
			@Override
			public void loadTaskCount(int count, File file) {
				task_progress.setMaximum(count);
				task_progress.setValue(0);
				saveLogFile = file;
			}
			@Override
			public void doneTaskCount() {
				task_progress.setValue(task_progress.getValue() + 1);
//				System.out.println("progress: " + task_progress.getValue());
			}
		});
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				cf.start(path);
				try {
					Thread.sleep(500);
					check.setEnabled(true);
					task_progress.setValue(0);
					if(saveLogFile!=null){
						openResultFile.setVisible(true);
					}
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				}
				
			}
		});
		
		thread.start();
	}
	public Container getCon() {
		return con;
	}

}
