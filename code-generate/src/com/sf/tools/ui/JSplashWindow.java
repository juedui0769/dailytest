package com.sf.tools.ui;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JWindow;
import javax.swing.UIManager;

import com.sf.tools.common.TimerTaskUpdate;

public class JSplashWindow extends JWindow {
//	Thread splashThread = null;

	private static final long serialVersionUID = -5899810222109775464L;

	public JSplashWindow() {
		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));// 设置启动界面的光标样式
		JPanel splash = new JPanel(new BorderLayout());
		URL url = getClass().getResource("/home1.jpg");// 获得指定资源文件的相对路径。
		if (url != null) {
			splash.add(new JLabel(new ImageIcon(Toolkit.getDefaultToolkit().createImage(url))), BorderLayout.CENTER);
		}
//		splash.setPreferredSize(new java.awt.Dimension(500, 300));
		setContentPane(splash);
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();// 获得屏幕的大小
		pack();
		setLocation((screen.width - getSize().width) / 2,
				(screen.height - getSize().height) / 2);// 使启动窗口居中显示
		start();
	}

	public void start() {
//		toFront();// window类的toFront()方法可以让启动界面显示的时候暂时在最前面，用window类的setAlwayOnTop(boolean)方法可以让窗口总保持在最前面。
		this.setAlwaysOnTop(true);
//		splashThread = new Thread(this);
//		splashThread.start();
		run();
	}

	public void run() {
		try {
			setVisible(true);
			Thread.sleep(3000);
//			setVisible(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		dispose();
//		showFrame("Demo splash window");
		
		EventQueue.invokeLater(new Runnable(){
			@Override
			public void run() {
				Main frame = new Main();
				frame.setVisible(true);
				new TimerTaskUpdate(frame);
			}
		});
	}

	static void showFrame(String title) {
		JFrame frame = new JFrame(title);
		frame.setSize(400, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension screenSize = frame.getToolkit().getScreenSize();// 获得屏幕的大小
		Dimension frameSize = frame.getSize();
		if (frameSize.height > screenSize.height) {
			frameSize.height = screenSize.height;
		}
		if (frameSize.width > screenSize.width) {
			frameSize.width = screenSize.width;
		}
		frame.setLocation((screenSize.width - frameSize.width) / 2,
				(screenSize.height - frameSize.height) / 2);
		frame.setVisible(true);
	}

	
	
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		new JSplashWindow();
//		showFrame("Demo splash window");
//		splash.start();
	}
}