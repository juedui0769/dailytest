package com.sf.tools.common;

import java.util.Timer;
import java.util.TimerTask;

import com.sf.tools.ui.Main;

public class TimerTaskUpdate extends Timer{

	private Main main;
	
	public TimerTaskUpdate(){
	}
	
	public TimerTaskUpdate(Main main){
		this.main = main;
		
		schedule();
	}
	
	//程序启动时检测更新,以后每小时检测一次
	public void schedule(){
		new TimerTaskUpdate().schedule(new TimerTask(){
			@Override
			public void run() {
				main.update();
			}
		}, 0, 1000*60*60);
	}
	
}