package com.sf.tools.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MysqlDao implements DBUtils{

	private String dbName;
	
	public MysqlDao(String driver, String url, String user, String pass) {
		this.dbName = url.substring(url.lastIndexOf("/")+1);
		ConnectionFactory.getIntance().init(driver, url, user, pass);
	}

	/**
	 * 获取所有表名
	 * @return
	 *///; InnoDB free: 9216 kB
	public List<String[]> findAllTables(){
		List<String[]> list = new ArrayList<String[]>();
		//select * from all_tables where owner='HFLS_USER';
		String sql = "select table_name,table_comment from information_schema.tables where table_schema='"+dbName+"'";
		Connection conn = ConnectionFactory.getIntance().getConnection();
		PreparedStatement pstm=null;
		ResultSet rs = null;
		try {
			pstm = conn.prepareStatement(sql);
			rs = pstm.executeQuery();
			while(rs.next()){
				String[] row = new String[2];
//				System.out.println(rs.getString(1));
				for(int i=1;i<=2;i++){
//					System.out.print(rs.getString(i) + "  ");
//					row.add(rs.getString(i));
					row[i-1] = rs.getString(i);
				}
//				if(row[1].startsWith("InnoDB")){
//					row[1] = "";
//				}else{
//					row[1] = row[1].substring(0, row[1].indexOf(";"));
//				}
//				System.out.println();
				list.add(row);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionFactory.getIntance().close(rs, pstm, conn);
		}
		return list;
	}

	public List<String[]> findTableByName(String tableName){
		String sql = "select column_name, column_type, column_comment , " +
				" if(column_key='PRI','Y','N') is_primary " +
			//	" case when column_key='PRI' then 'Y' else 'N' end is_primary " +
				" from information_schema.columns " +
				" where table_name = ?";
		
		Connection conn = ConnectionFactory.getIntance().getConnection();
		PreparedStatement pstm=null;
		ResultSet rs = null;
		List<String[]> list = new ArrayList<String[]>();
		try {
			pstm = conn.prepareStatement(sql.toString());
			pstm.setString(1, tableName);
			rs = pstm.executeQuery();
			while(rs.next()){
				String[] row = new String[4];
				for(int i=1;i<=4;i++){
					row[i-1] = rs.getString(i);
				}
				list.add(row);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionFactory.getIntance().close(rs, pstm, conn);
		}
		return list;
	}
	
	public void t(){
		String sql = "select table_name,table_comment from information_schema.tables where table_schema='gamecard'";
		Connection conn = ConnectionFactory.getIntance().getConnection();
		PreparedStatement pstm=null;
		ResultSet rs = null;
		try {
			pstm = conn.prepareStatement(sql);
			rs = pstm.executeQuery();
			while(rs.next()){
				System.out.println(rs.getString(1) + " - " + rs.getString(2));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionFactory.getIntance().close(rs, pstm, conn);
		}
	}
	
	public static void main(String[] args) {
//		new MysqlDao().t();
//		new MysqlDao().findAllTables();
//		new MysqlDao().findTableByName("information_schema.tables");
//		List<String[]> list = DBUtils.test("hfls_tm_car");
//		Object [][] userInfo = {{"11","12","13","14"},{"21","22","23","24"}};
//		userInfo = new Object[6][4];
//		
//		for(int i=0;i<6;i++){
//			for(int j=0;j<4;j++){
//				userInfo[i][j] = list.get(i)[j];
//			}
//		}
//		System.out.println("*****************************");
//		for (Object[] objects : userInfo) {
//			for (Object obj : objects) {
//				System.out.print(obj + " ");
//			}
//			System.out.println();
//		}
		String s = "jdbc:mysql://10.0.4.52:3306/gamecard";
		s = s .substring(s.lastIndexOf("/")+1);
		System.out.println(s);
	}

	@Override
	public List<String> getSequences() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getPrimaryKey(String tableName) {
		// TODO Auto-generated method stub
		return null;
	}
}
