package com.sf.tools.common;

import org.apache.log4j.Logger;


public class UpdateThread extends Thread{
	
	private static final Logger log = Logger.getLogger(UpdateThread.class);
	
	private String updateFilePath;
	private String currFilePath;
	
	public UpdateThread(String updateFilePath, String currFilePath) {
		this.updateFilePath = updateFilePath;
		this.currFilePath = currFilePath;
	}

	public void run(){
		try {
			log.info("开始更新...");
//			System.out.println("开始更新...");
			
//			File file = new File(path);
//			file.delete();
//			String path = System.getProperty("user.dir") + File.separator + "update.jar";
			Runtime.getRuntime().exec("java -jar "+updateFilePath+" "+currFilePath);
			
//			log.info("java -jar "+updateFilePath+".jar "+currFilePath);
			log.info("更新结束...");
		} catch (Exception e) {
			log.info("更新错误1!",e);
//			e.printStackTrace();
		} 
	}
}
