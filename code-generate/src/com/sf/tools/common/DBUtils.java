package com.sf.tools.common;

import java.util.List;

public interface DBUtils {

	
	public List<String[]> findAllTables();

	public List<String[]> findTableByName(String tableName);

	public List<String> getSequences();
	
	public List<String> getPrimaryKey(String tableName);
}
