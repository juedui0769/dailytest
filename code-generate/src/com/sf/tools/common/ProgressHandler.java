package com.sf.tools.common;

public interface ProgressHandler {

	public void loadTaskCount(int count);
	
	public void doneTaskCount();
}
