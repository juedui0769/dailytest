package com.sf.tools.common;

import java.sql.Blob;

public class Version {

	private String id;
	private String version;
	private Blob blob;
	
	public Version() {
		super();
	}
	
	public Version(String id, String version, Blob blob) {
		this.id = id;
		this.version = version;
		this.blob = blob;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public Blob getBlob() {
		return blob;
	}
	public void setBlob(Blob blob) {
		this.blob = blob;
	}

}
