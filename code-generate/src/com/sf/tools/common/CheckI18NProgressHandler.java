package com.sf.tools.common;

import java.io.File;


public interface CheckI18NProgressHandler {
	
	public void loadTaskCount(int count, File file);
	
	public void doneTaskCount();

}
