package com.sf.tools.common;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Common_old {

	public static Double version = 1.0;
	
	public static String filePath = "";

	public static String generatePath = "";
	
	public static String MODULE_NAME = "";
	
	public static String TABLE_NAME = "";
	
	public static String SEQUENCE_NAME = "";
	
	public static String DOMAIN_DESC = "";
	
	public static String DOMAIN_NAME = "";
	
	public static String IDAO_NAME = "";
	public static String DAO_NAME = "";
	public static String IBIZ_NAME = "";
	public static String BIZ_NAME = "";
	public static String ACTION_NAME = "";
	
	public static String PROP_NAME = "";
	public static String PROP_NAME_DAO = "";
	public static String PROP_NAME_BIZ = "";
	public static String PROP_NAME_ACTION = "";
	
	public static boolean isShowTree = true;
	public static String DEPT_QUERY = "";
	public static String DEPT_TYPE = "";
	
	public static boolean ADD = true;
	public static boolean UPDATE = true;
	public static boolean DELETE = true;
	public static boolean REPORT = true;
	
	public static String GENERATE_SPRING_TYPE = "";
	
	public static String FIELD_WIDTH = "";
	public static String ANCHOR = "";
	
	public static List<File> files = new ArrayList<File>();
	
	public static void init(){
		IDAO_NAME = "I"+DOMAIN_NAME+"Dao";
		DAO_NAME = DOMAIN_NAME+"Dao";
		IBIZ_NAME = "I"+DOMAIN_NAME+"Biz";
		BIZ_NAME = DOMAIN_NAME+"Biz";
		ACTION_NAME = DOMAIN_NAME+"Action";
		
		PROP_NAME = (DOMAIN_NAME.charAt(0)+"").toLowerCase()+DOMAIN_NAME.substring(1);
		PROP_NAME_DAO = (DOMAIN_NAME.charAt(0)+"").toLowerCase()+DOMAIN_NAME.substring(1)+"Dao";
		PROP_NAME_BIZ = (DOMAIN_NAME.charAt(0)+"").toLowerCase()+DOMAIN_NAME.substring(1)+"Biz";
		PROP_NAME_ACTION = (DOMAIN_NAME.charAt(0)+"").toLowerCase()+DOMAIN_NAME.substring(1)+"Action";
		
	}
	
	public static String getClassDesc(String desc){
		StringBuffer sb = new StringBuffer();
		sb.append("/**").append("\n")
		  .append("* <pre>").append("\n")
		  .append("* *********************************************").append("\n")
		  .append("* Copyright sf-express.").append("\n")
		  .append("* All rights reserved.") .append("\n")
		  .append("* Description: "+desc+"").append("\n")
		  .append("* HISTORY").append("\n")
		  .append("* *********************************************").append("\n")
		  .append("*  ID     DATE          PERSON          REASON").append("\n")
		  .append("*  1      "+getCurrDate()+"    "+getCurrUser()+"         创建") .append("\n")
		  .append("* *********************************************").append("\n")
		  .append("* </pre>").append("\n")
		  .append("*/").append("\n");
		return sb.toString();
	}
	
	public static String getCurrUser(){
		String name = "admin";
		try{
			name = System.getProperty("user.name");
		}catch(Exception e){
			e.printStackTrace();
			return name;
		}
		return name;
	}
	
	public static String getCurrDate(){
		String date = "2013-01-01";
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			date = sdf.format(new Date());
		}catch(Exception e){
			e.printStackTrace();
			return date;
		}
		return date;
	}
	
	public static String getFieldName(String columnName){
		String name = columnName.toLowerCase();
		char[] ch = name.toCharArray();
		for(int i=0;i<ch.length;i++){
			if("_".equals(ch[i]+"")){
				ch[i+1] = ((ch[i+1]+"").toUpperCase()).charAt(0);
			}
		}
		String s = new String(ch).replace("_", "");
		return s;
	}
	
	public static String getFieldType(String columnType){
		String type = columnType.toLowerCase();
		if(type.matches("number[(]\\d{2}[)]")){
			return "Long";
		}else if(type.matches("number[(]\\d{1}[)]")){
			return "Integer";
		}else if(type.matches("number[(]\\d+\\,\\d+[)]")){
			return "Double";
		}else if(type.matches("date")){
			return "Date";
		}else if(type.matches("varchar2[(]\\d+[)]")){
			return "String";
		}else if(type.matches("char[(]\\d+[)]")){
			return "String";
		}else if(type.matches("number")){
			return "Long";
		}
		return "String";
	}
	
	public static String getLengthDesc(String columnType){
		Pattern p = Pattern.compile("number[(](\\d{1,2})[)]",Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(columnType);
		if(m.find()){
			return "precision=\""+m.group(1)+"\"";
		}
		
		p = Pattern.compile("number[(](\\d+)\\,(\\d+)[)]",Pattern.CASE_INSENSITIVE);
		m = p.matcher(columnType);
		if(m.find()){
			return "precision=\""+m.group(1)+"\" scale=\""+m.group(2)+"\"";
		}
		
		p = Pattern.compile("varchar2[(](\\d+)[)]",Pattern.CASE_INSENSITIVE);
		m = p.matcher(columnType);
		if(m.find()){
			return "length=\""+m.group(1)+"\"";
		}
		
		p = Pattern.compile("char[(](\\d+)[)]",Pattern.CASE_INSENSITIVE);
		m = p.matcher(columnType);
		if(m.find()){
			return "length=\""+m.group(1)+"\"";
		}
		
		p = Pattern.compile("date",Pattern.CASE_INSENSITIVE);
		m = p.matcher(columnType);
		if(m.find()){
			return "length=\"7\"";
		}
		
		return "";
	}
	
	public static String getSubDesc(String desc, String column){
		if(desc == null || "".equals(desc.trim())) return column;
		desc = desc.trim().replace("（", "(").replace("：", ":").replace("；", ";").replace("【", "[");
		String splits[] = {" ", "(", ":",";","["};
		for(int i=0;i<splits.length;i++){
			if(desc.indexOf(splits[i]) != -1){
				return desc.substring(0, desc.indexOf(splits[i]));
			}
		}
		Pattern p = Pattern.compile("\\d\\s*{1}=");
		Matcher m = p.matcher(desc);
		if(m.find()){
			desc = desc.substring(0, desc.indexOf(m.group()));
		}
		return desc;
	}
	
	public static String getFileEncoding(String path){
		//ANSI 0 0 0   utf8 -17 -69 -65  UNICODE -1 -2 0
		String code="gb2312";
		try {
			InputStream inputStream=new FileInputStream(new File(path));
			byte []head=new byte[3];
			inputStream.read(head);
//System.out.println(head[0] + "   " + head[1] + "   " + head[2]);
			if(head[0]==-1&&head[1]==-2){
				code="UTF-16";
			}
			if(head[0]==-2&&head[1]==-1){
				code="Unicode";
			}
			if((head[0]==-17&&head[1]==-69) || (head[0]==-26&&head[1]==-95)
					|| (head[0]==60 && head[1]==63)){
				code="UTF-8";
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
//		System.out.println("***********"+code);
		return code;
	}
	
	public static boolean endWithSuffixs(String fileName,List<String> suffixs){
		for (String suffix : suffixs) {
//			System.out.println(fileName + "---" + suffix);
			if(fileName.endsWith(suffix)){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 获取指定目录下以指定的后缀名结束的所有文件
	 * @author 徐益森(sfhq217)
	 * @date 2012-10-16 
	 * @param path 文件夹路径
	 * @param name 后缀名,name如果为 null 或 "" 则获取所有文件
	 * @param isdepth 是否包含子目录
	 * @return
	 */
	public static List<File> findFilesByFileName(String path, final List<String> suffixs, final boolean isdepth){
		File file = new File(path);
		if(file.exists()){
			if(file.isDirectory()){
				File[] listFiles = file.listFiles(new FileFilter(){
					@Override
					public boolean accept(File f) {
						if(f.isDirectory() && isdepth==true){
							return true;
						}
//						if(f.isFile() && (name==null || "".equals(name.trim()))){
//							return true;
//						}
						if(f.isFile()){
							return true;
						}
						return false;
					}
				});
				for (File file2 : listFiles) {
					//过滤 .svn 文件
					if(file.getName().toLowerCase().endsWith(".svn")) continue;
					
					if(file2.isFile() && endWithSuffixs(file2.getName(),suffixs)){
//						System.out.println(file2.getPath());
						files.add(file2);
					}else{
						findFilesByFileName(file2.getPath(),suffixs,isdepth);
					}
				}
			}
		}
		return files;
	}
	
	public static void write(File file,String content){
//		PrintWriter pw = null;
//		try {
//			pw = new PrintWriter(file);
//			pw.write(content);
//			pw.close();
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		}
		
		try {
			Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),"UTF-8"));
			out.write(content);
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
//		System.out.println(getLengthDesc("number(1)"));
//		System.out.println(getLengthDesc("number(9)"));
//		System.out.println(getLengthDesc("number(10)"));
//		System.out.println(getLengthDesc("number(19)"));
//		System.out.println(getLengthDesc("number(19,5)"));
//		System.out.println(getLengthDesc("number(19,15)"));
//		System.out.println(getLengthDesc("varchar2(32)"));
//		System.out.println(getLengthDesc("char(16)"));
//		System.out.println(getLengthDesc("date"));
//		
//		System.out.println(getSubDesc("工作事项ID"));
//		getFileEncoding("D:/a/sql.properties");
		System.out.println("aaa".matches("aaa"));
	}
}
