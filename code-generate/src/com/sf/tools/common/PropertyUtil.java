package com.sf.tools.common;

import java.io.IOException;
import java.util.Properties;

public class PropertyUtil {

	private static Properties prop = new Properties();
	
	static{
		try {
			prop.load(ConnectionFactory.class.getResourceAsStream("/db.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static String getKey(String key){
		return prop.getProperty(key);
	}
}
