package com.sf.tools.common;

import java.awt.Color;
import java.awt.Font;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

public class PieChartUtil {

	//用于存储数据
	private Map<String,Double> data = new HashMap<String,Double>();
	
	// 饼状图  
	private PieDataset getDataSet() {   
		// 设置数据 
		DefaultPieDataset dataSet = new DefaultPieDataset();
		Set<String> keys = data.keySet();
		for (String key : keys) {
			if("count".equals(key)){
				continue;
			}
			dataSet.setValue(key, data.get(key));
		}
		return dataSet;
	}
	
	// 这是个饼状图  
	public String drawPic() {   
		String tempImagePath = "D:/"+new Date().getTime()+".jpg";
		PieDataset dataset = getDataSet(); 
		// 拿到数据   
		JFreeChart chart = ChartFactory.createPieChart("", dataset, true, true, false);    
		PiePlot plot = (PiePlot) chart.getPlot();
		// 图片中显示百分比默认方式  
		plot.setLabelGenerator(new StandardPieSectionLabelGenerator("{0}={1}({2})", NumberFormat.getNumberInstance(), new DecimalFormat("0.00%")));   
		// 图例显示百分比:自定义方式 {0} 表示选项 {1} 表示数值 {2} 表示所占比例   
		plot.setLegendLabelGenerator(new StandardPieSectionLabelGenerator("{0}={1}({2})", NumberFormat.getNumberInstance(), new DecimalFormat("0.00%")));   
		// 设置背景颜色为白色   
		chart.setBorderPaint(Color.green);   
		// 指定图片的透明度(0.0-1.0)   
		plot.setForegroundAlpha(1.0f);   
		// 指定显示的饼图上圆形flase还是椭圆形true   
		plot.setCircular(true);   
		Font font = new Font("黑体", Font.CENTER_BASELINE, 20);   
		PiePlot pplot = (PiePlot) chart.getPlot();    
		pplot.setLabelFont(new Font("SimSun", Font.PLAIN, 12));    
		// 这句代码解决了底部汉字的乱码问题  
		chart.getLegend().setItemFont(new Font("宋体", Font.PLAIN, 12));    
//		Date date = new Date();   
//		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy年MM月dd日");    
//		TextTitle title = new TextTitle("截止" + sdf.format(date) + "空气质量状况分布图");
		TextTitle title = new TextTitle("代码统计(总共："+Math.round(data.get("count"))+" 行)");
		
		chart.setTitle(title);
		title.setFont(font);
		FileOutputStream fos_jpg = null;
		try {
			fos_jpg = new FileOutputStream(tempImagePath);
			ChartUtilities.writeChartAsJPEG(fos_jpg, 1.0f, chart, 540, 390,null);
			fos_jpg.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return tempImagePath;
	}
	
	public static void main(String[] args) throws ClassNotFoundException,SQLException {
//		System.out.println("这是个饼状图呵呵");
//		PieChartUtil.getPic();
		Double d = 1024.0;
		System.out.println(Math.round(d));
	}

	public Map<String, Double> getData() {
		return data;
	}

	public void setData(Map<String, Double> data) {
		this.data = data;
	}
}
