package com.sf.tools.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * 用于产生数据库连接对象
 * @author 徐益森
 * @date 2012-11-30
 * */
public class ConnectionFactory {
	
	//插入数据库配置信息
	private String username;
	private String password;
	private String url;
	private String driver;
	
	private static ConnectionFactory instance;
	
	private ConnectionFactory(){
		username = PropertyUtil.getKey("username");
		password = PropertyUtil.getKey("password");
		url = PropertyUtil.getKey("url");
		driver = PropertyUtil.getKey("driver");
	}
	
	public void resevet(){
		username = PropertyUtil.getKey("username");
		password = PropertyUtil.getKey("password");
		url = PropertyUtil.getKey("url");
		driver = PropertyUtil.getKey("driver");
	}
	
	/**
	 * 获取本身的一个实例且只一个
	 * */
	public synchronized static ConnectionFactory getIntance(){
		if(instance==null)
			instance=new ConnectionFactory();
		return instance;
	}
	
	public boolean testConnect(String driver, String url,String user, String pass){
		this.driver = driver;
		this.url = url;
		this.username = user;
		this.password = pass;
		Connection conn = null;
		boolean flag;
		try {
			conn = getConnection();
			flag = conn!=null ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try{
				if(conn != null && !conn.isClosed()){
					conn.close();
				}
			}catch(SQLException e){
				e.printStackTrace();
			}finally {
				conn = null;
			}
		}
		return flag;
	}
	
	public void init(String driver, String url,String user, String pass){
		this.driver = driver;
		this.url = url;
		this.username = user;
		this.password = pass;
	}
	
	/**
	 * 获取数据库连接
	 * */
	public Connection getConnection(){
		Connection conn=null;
		try {
			Class.forName(driver);
			conn = DriverManager.getConnection(url, username, password);
			if(conn != null){
				System.out.println("数据库 连接成功......");
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	/**
	 * 关闭连接
	 * @param conn
	 */
	public void close(Connection conn){
		try{
			if(conn != null && !conn.isClosed()){
				conn.close();
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally {
			conn = null;
		}
		resevet();
	}
	
	/**
	 * 关闭连接
	 * @param stmt
	 * @param conn
	 */
	public void close(Statement stmt,Connection conn){
		try{
			if(stmt!=null){
				stmt.close();
			}
			if(conn!=null && !conn.isClosed()){
				conn.close();
			}
		}catch(SQLException sqle){
			sqle.printStackTrace();
		}finally{
			stmt=null;
			conn=null;
		}
		resevet();
	}
	/**
	 * 关闭连接
	 * @param rs
	 * @param stmt
	 * @param conn
	 */
	public void close(ResultSet rs,Statement stmt,Connection conn){
		try{
			if(rs!=null){
				rs.close();
			}
			if(stmt!=null){
				stmt.close();
			}
			if(conn!=null && !conn.isClosed()){
				conn.close();
			}
		}catch(SQLException sqle){
			sqle.printStackTrace();
		}finally{
			rs=null;
			stmt=null;
			conn=null;
		}
		resevet();
	}
	
	public static void main(String[] args) {
		ConnectionFactory.getIntance().getConnection();
	}
}
