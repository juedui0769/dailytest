package com.sf.tools.generate;

import java.util.List;
import java.util.Map;

import com.sf.tools.common.Common;
import com.sf.tools.common.DBUtils;
import com.sf.tools.common.ProgressHandler;

public class GenerateCode {

	private ProgressHandler progressHandler;
	
//	private DBUtils dbUtils;
	
	public boolean generate(List<String> isGenerate, DBUtils dbUtils, List<String> isSearch,List<String> isAdd,List<String> isUpdate,
			List<String> isDelete,List<String> isImport,List<String> isExport,Map<String, String> mapping, List<String> isQuery, 
			List<String> isShow, List<String> isCombo, String projectName, String generatePath,String[] prefixs) {
		try{
//			BuildEntity.createDoMain();
//			BuildDao.createDao();
//			BuildBiz.createBiz();
//			BuildAction.createAction();
//			BuildConf.createHibernate();
//			BuildConf.createSpring();
//			BuildConf.createStruts();
//			BuildJspAndJs.createJSP();
//			BuildJspAndJs.createJS();
//			this.dbUtils = dbUtils;
			
			List<String[]> tableNames = dbUtils.findAllTables();
			
			this.progressHandler.loadTaskCount(isGenerate.size());
System.out.println("mapping: "+mapping.size()+"    *****************************************");
System.out.println(mapping);
System.out.println("isQuery: "+isQuery.size()+"    *****************************************");
System.out.println(isQuery);
System.out.println("isShow:  "+isShow.size()+"    *****************************************");
System.out.println(isShow);
System.out.println("isCombo: "+isCombo.size()+"    *****************************************");
System.out.println(isCombo);
			Common.init(isSearch,isAdd,isUpdate,isDelete,isImport,isExport,mapping,isQuery,isShow,isCombo,projectName,generatePath,prefixs);
			int index = 0;
			int exportCount = 0;
			for (String[] tableNameInfo : tableNames) {
				String tableName = tableNameInfo[0];
//				if(!"HFLS_TT_TRANSIT".equalsIgnoreCase(tableName)){
//					continue;
//				}
				if(!isGenerate.contains(tableName)){
					continue;
				}
				String comments = tableNameInfo[1];
//				System.out.println(tableName);
				List<String[]> columnInfos = dbUtils.findTableByName(tableName);
				Common.generate(tableName,comments,columnInfos);
				
				BuildEntity.createDoMain();
				BuildDao.createDao(dbUtils);
				List<String> joinBizs = BuildBiz.createBiz();
				BuildAction.createAction();
				BuildJspAndJs.createJSP();
				BuildJspAndJs.createJS(dbUtils);
				
				BuildCfgHibernate.createHibernate(index,index == isGenerate.size()-1 ? true : false);
				BuildCfgSpring.createSpring(index,index == isGenerate.size()-1 ? true : false,joinBizs);
				
				if(Common.EXPORT){
					BuildExcelExport.create(exportCount,exportCount == isExport.size()-1 ? true : false);
					exportCount++;
				}
				
				this.progressHandler.doneTaskCount();
				index++;
//				break;
			}
			BuildBaseEntityDao.createBaseDao();
			BuildCfgStruts.createStruts();
			BuildIconCss.createIconCss();
			BuildCommonJs.createCommonJs();

			if(isExport.size()>0 || isImport.size()>0){
				BuildDownloadAction.createDownloadAction();
			}
			
			if(isUpdate.size()>0){
				BuildClassUtil.createBuildClassUtil();
			}
			
//			for(int i=0;i<100;i++){
//				Thread.sleep(new Random(100).nextInt(10)*50);
//				this.progressHandler.doneTaskCount();
//			}
			
			return true;
		} catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}

	public ProgressHandler getProgressHandler() {
		return progressHandler;
	}

	public void setProgressHandler(ProgressHandler progressHandler) {
		this.progressHandler = progressHandler;
	}

	
}
