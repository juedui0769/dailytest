package com.sf.tools.generate;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import jxl.Workbook;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import com.sf.tools.common.Common;



public class BuildBiz {

	private static StringBuffer buff = new StringBuffer();
	private static boolean existDate = false;
	private static List<String> joinBiz = new ArrayList<String>();
	private static boolean flag = false;
	private static String joins = "";
	private static String defineBiz = "";
	private static boolean impUser;
	private static boolean impDate;
	public static List<String> createBiz(){
		existDate = false;
		joinBiz.clear();
		flag = false;
		joins = "";
		defineBiz = "";
		impUser = false;
		impDate = false;
		
		createInterface();
		createClass();
//		System.out.println(buff.toString());
		return joinBiz;
	}

	private static void importPackageIBiz(){
		buff.append("package com.sf.module.").append(Common.MODULE_NAME).append(".biz;").append("\n\n");
		if(Common.IMPORT){
			buff.append("import java.io.File;").append("\n")
				.append("import java.io.FileNotFoundException;").append("\n");
		}
		buff.append("IMPORT_LIST").append("\n");
		buff.append("import com.sf.framework.base.IPage;").append("\n")
			.append("import com.sf.framework.server.base.biz.IBiz;").append("\n")
			.append("import com.sf.module.").append(Common.MODULE_NAME).append(".domain.").append(Common.DOMAIN_NAME).append(";\n");
		if(Common.IMPORT){
			buff.append("import com.sf.module.frameworkimpl.util.io.AbortException;").append("\n");
		}
		buff.append("import com.sf.module.frameworkimpl.domain.QueryObj;").append("\n\n");
	}
	
	private static void importPackageBiz(){
		buff.append("package com.sf.module.").append(Common.MODULE_NAME).append(".biz;").append("\n\n");
		
		if(Common.IMPORT){
			buff.append("import java.io.File;").append("\n");
			buff.append("import java.io.FileInputStream;").append("\n");
			buff.append("import java.io.FileNotFoundException;").append("\n");
		}
		buff.append("IMPORT_DATEUTIL");
		if(Common.EXPORT){
			flag = true;
		}
		buff.append("IMPORT_LIST");
		if(Common.DELETE){
			buff.append("import org.hibernate.criterion.DetachedCriteria;").append("\n")
				.append("import org.hibernate.criterion.Restrictions;").append("\n\n");
		}
		if("注解".equals(Common.GENERATE_SPRING_TYPE)){
			buff.append("import javax.annotation.Resource;").append("\n\n")
				.append("import org.springframework.stereotype.Service;").append("\n\n");
		}
		buff.append("import com.sf.framework.base.IPage;").append("\n");
		if(Common.EXPORT){
			buff.append("import com.sf.framework.core.exception.BizException;").append("\n");
		}
		buff.append("import com.sf.framework.server.base.biz.BaseBiz;\n")
			.append("IMPORT_USER")
			.append("import com.sf.module.frameworkimpl.domain.QueryObj;").append("\n");
		if(Common.IMPORT){
			buff.append("import com.sf.module.frameworkimpl.util.io.AbortException;").append("\n");
			buff.append("import com.sf.module.frameworkimpl.util.io.read.DefaultExcelReadDealer;").append("\n");
			buff.append("import com.sf.module.frameworkimpl.util.io.read.ReadHelper;").append("\n");
		}
		buff.append("import com.sf.module."+Common.MODULE_NAME+".dao."+Common.IDAO_NAME+";\n")
			.append("import com.sf.module.").append(Common.MODULE_NAME).append(".domain.").append(Common.DOMAIN_NAME).append(";\n");
		if(Common.UPDATE){
			buff.append("import com.sf.module."+Common.MODULE_NAME+".util.ClassUtil;").append("\n");
		}
		if(Common.EXPORT){
			buff.append("import com.sf.module."+Common.MODULE_NAME+".excel.ExcelNameConfig;").append("\n");
			buff.append("import com.sf.module."+Common.MODULE_NAME+".util.PoiUtil;").append("\n");
		}
		buff.append("\n");
	}
	
	private static void createInterface() {
		buff = new StringBuffer();
		importPackageIBiz();
		buff.append(Common.getClassDesc(Common.DOMAIN_DESC+" Biz接口定义"))
			.append("public interface "+Common.IBIZ_NAME+" extends IBiz {\n\n")
			.append(Common.getMethodDesc("分页查询","queryObj"))
			.append("\t").append("public IPage<"+Common.DOMAIN_NAME+"> findPageBy(QueryObj queryObj);\n\n");
		
		if(Common.ADD){
			buff.append(Common.getMethodDesc("新增",Common.PROP_NAME));
			buff.append("\t").append("public void save("+Common.DOMAIN_NAME+" "+Common.PROP_NAME+");\n\n");
		}
		if(Common.UPDATE){
			buff.append(Common.getMethodDesc("修改",Common.PROP_NAME));
			buff.append("\t").append("public void update("+Common.DOMAIN_NAME+" "+Common.PROP_NAME+");\n\n");
		}
		if(Common.DELETE){
			buff.append(Common.getMethodDesc("删除","ids"));
			buff.append("\t").append("public void delete(Long[] ids);\n\n");
		}
		if(Common.IMPORT){
			buff.append(Common.getMethodDesc("导入","uploadFile"));
			buff.append("\t").append("public void addImports(File uploadFile) throws FileNotFoundException, AbortException;").append("\n\n");
		}
		if(Common.EXPORT){
			buff.append(Common.getMethodDesc("导出","queryObj"));
			buff.append("\t").append("public String export(QueryObj queryObj);").append("\n\n");
		}
		
		List<String> queryNames = new ArrayList<String>();
		Collection<String> values = Common.MAPPING.values();
		for (String value : values) {
			System.out.println(value);
			String[] arr = value.split(":");
			String tableName = arr[0];
			String showName = arr[2];
			if(tableName.equals(Common.TABLE_NAME) && !queryNames.contains(showName)){
				queryNames.add(showName);
			}
		}
		for (String queryName : queryNames) {
			String fieldName = Common.getFieldName(queryName);
			String queryDesc = Common.getQueryDesc(fieldName);
			String queryType = Common.getQueryType(fieldName);
			
			buff.append(Common.getMethodDesc("根据"+queryDesc+"获取"+Common.DOMAIN_DESC,fieldName));
			buff.append("\t").append("public "+Common.DOMAIN_NAME+" findBy("+queryType+" "+fieldName+");").append("\n\n");
		}
		
		boolean flag = false;
		if(!queryNames.isEmpty()){
			flag = true;
			buff.append(Common.getMethodDesc("获取所有"+Common.DOMAIN_DESC));
			buff.append("\t").append("public List<"+Common.DOMAIN_NAME+"> findAll();").append("\n\n");
		}
		
		buff.append("}");
		
		String content = buff.toString();
		content = content.replace("IMPORT_LIST", flag?"import java.util.List;\n":"");
		
		File file = new File(Common.SAVE_PATH+File.separator+"biz");
		if(!file.exists()){
			file.mkdirs();
		}
		Common.write(new File(file,Common.IBIZ_NAME+".java"), content);
		
		System.out.println(Common.IBIZ_NAME+".java 已生成.....");
	}
	
	private static void createClass() {
		File file = new File(Common.SAVE_PATH+File.separator+"biz");
		if(!file.exists()){
			file.mkdirs();
		}
		
		if(Common.IMPORT){
			Common.write(new File(file,Common.DOMAIN_NAME+"UploadReadRenderer.java"), getImportClass());
		}
		
		buff = new StringBuffer();
		importPackageBiz();
		buff.append(Common.getClassDesc(Common.DOMAIN_DESC+" Biz接口实现"));
		if("注解".equals(Common.GENERATE_SPRING_TYPE)){
			buff.append("@Service").append("\n");
		}
		buff.append("public class ").append(Common.BIZ_NAME).append(" extends BaseBiz implements ").append(Common.IBIZ_NAME).append(" {\n\n");
		if("注解".equals(Common.GENERATE_SPRING_TYPE)){
			buff.append("\t").append("@Resource").append("\n");
		}
		buff.append("\t").append("private "+Common.IDAO_NAME+" "+Common.PROP_NAME_DAO+";\n");
		
		buff.append("DEFINE_IBIZ_NAME").append("\n");
		
		buff.append(Common.getMethodDesc("分页查询","queryObj"));
		buff.append(create_Method_findPageBy());

		if(Common.ADD){
			buff.append(Common.getMethodDesc("新增",Common.PROP_NAME));
			buff.append(create_Method_save());
		}
		if(Common.UPDATE){
			buff.append(Common.getMethodDesc("修改",Common.PROP_NAME));
			buff.append(create_Method_update());
		}
		if(Common.DELETE){
			buff.append(Common.getMethodDesc("删除","ids"));
			buff.append(create_Method_delete());
		}
		if(Common.IMPORT){
			buff.append(Common.getMethodDesc("导入","uploadFile"));
			buff.append(create_Method_import());
		}
		if(Common.EXPORT){
			buff.append(Common.getMethodDesc("导出","queryObj"));
			buff.append(create_Method_export());
		}
		
		List<String> queryNames = new ArrayList<String>();
		Collection<String> values = Common.MAPPING.values();
		for (String value : values) {
			System.out.println(value);
			String[] arr = value.split(":");
			String tableName = arr[0];
			String showName = arr[2];
			if(tableName.equals(Common.TABLE_NAME) && !queryNames.contains(showName)){
				queryNames.add(showName);
			}
		}
		
		for (String queryName : queryNames) {
			flag = true;
			String fieldName = Common.getFieldName(queryName);
			String queryDesc = Common.getQueryDesc(fieldName);
			String queryType = Common.getQueryType(fieldName);
			
			buff.append(Common.getMethodDesc("根据"+queryDesc+"获取"+Common.DOMAIN_DESC,fieldName));
			buff.append("\t").append("public "+Common.DOMAIN_NAME+" findBy("+queryType+" "+fieldName+"){").append("\n")
				.append("\t\t").append("DetachedCriteria dc = DetachedCriteria.forClass("+Common.DOMAIN_NAME+".class);").append("\n")
				.append("\t\t").append("dc.add(Restrictions.eq(\""+fieldName+"\", "+fieldName+"));").append("\n")
				.append("\t\t").append("List<ClassInfo> list = classInfoDao.findBy(dc);").append("\n")
				.append("\t\t").append("if(!list.isEmpty()){").append("\n")
				.append("\t\t\t").append("return list.get(0);").append("\n")
				.append("\t\t").append("}").append("\n")
				.append("\t\t").append("return null;").append("\n")
				.append("\t").append("}").append("\n\n");
		}
		
		if(!queryNames.isEmpty()){
			flag = true;
			buff.append(Common.getMethodDesc("获取所有"+Common.DOMAIN_DESC));
			buff.append("\t").append("public List<"+Common.DOMAIN_NAME+"> findAll(){").append("\n")
				.append("\t\t").append("return "+Common.PROP_NAME_DAO+".findAll();").append("\n")
				.append("\t").append("}").append("\n\n");
		}
		
		buff.append("\t").append("public void set"+Common.DAO_NAME+"("+Common.IDAO_NAME+" "+Common.PROP_NAME_DAO+") {").append("\n");
		buff.append("\t\t").append("this."+Common.PROP_NAME_DAO+" = "+Common.PROP_NAME_DAO+";").append("\n");
		buff.append("\t").append("}").append("\n");
		
		for(String biz : joinBiz){
			String str = (biz.charAt(0)+"").toUpperCase()+biz.substring(1);
			buff.append("\t").append("public void set"+str+"(I"+str+" "+biz+") {").append("\n")
				.append("\t\t").append("this."+biz+" = "+biz+";").append("\n")
				.append("\t").append("}").append("\n")
				.append("").append("\n");
		}
		
		buff.append("}");
		
		
		String content = buff.toString();
		content = content.replace("IMPORT_SIMPLEDATEFORMAT", existDate?"import java.text.SimpleDateFormat;\n":"");
		content = content.replace("DEFINE_IBIZ_NAME", defineBiz);
		content = content.replace("IMPORT_LIST", flag?"import java.util.List;\n\n":"");
		content = content.replace("IMPORT_USER", impUser?"import com.sf.module.authorization.domain.User;\n":"");
		content = content.replace("IMPORT_DATEUTIL", impDate?"import java.util.Date;\n":"");
		
		Common.write(new File(file,Common.BIZ_NAME+".java"), content);
		
		System.out.println(Common.BIZ_NAME+".java 已生成.....");
		
		
		if(Common.EXPORT){
			WritableWorkbook book = null;
			WritableSheet sheet = null;
			FileOutputStream fos = null;
			try{
				String path = Common.SAVE_PATH+File.separator+"META-INF"+File.separator+"tpl";
				File f = new File(path);
				if(!f.exists()){
					f.mkdirs();
				}
				
				fos = new FileOutputStream(new File(path,Common.DOMAIN_DESC+".xls"));
				book = Workbook.createWorkbook(fos);
				sheet = book.createSheet(Common.DOMAIN_DESC, 0);
				WritableCellFormat wc = new WritableCellFormat();
				// 设置居中   
//				wc.setAlignment(Alignment.CENTRE);   
				WritableFont wf = new WritableFont(WritableFont.createFont("宋体"), 11);
				wc.setFont(wf);
				// 设置边框线   
				wc.setBorder(Border.ALL, BorderLineStyle.THIN);   
				// 设置单元格的背景颜色   
				wc.setBackground(jxl.format.Colour.GREY_25_PERCENT);
				
//				sheet.addCell(new Label(0, 0, "名称",wc));
//				sheet.addCell(new Label(1, 0, "类型",wc));
				int columnIndex=0;
				for(int i=0;i<Common.SHOW.size();i++){
					String header = Common.getQueryDesc(Common.SHOW.get(i));
					if("创建人".equals(header) || "创建时间".equals(header) || "修改人".equals(header) || "修改时间".equals(header)){
						continue;
					}
					if(header.indexOf("(") != -1){
						header = header.substring(0,header.indexOf("("));
					}
					sheet.addCell(new Label(columnIndex++, 0, header,wc));
				}
			}catch (Exception e) {
				e.printStackTrace();
			} finally{
				try {
					book.write();
				} catch (IOException e) {
					e.printStackTrace();
				}
				try {
					book.close();
					fos.close();
				} catch (Exception e) {
					e.printStackTrace();
				} finally{
					fos = null;
					book = null;
				}
			}
		}
	}
	
	private static StringBuffer create_Method_findPageBy(){
		StringBuffer sb = new StringBuffer();
		sb.append("\t").append("public IPage<"+Common.DOMAIN_NAME+"> findPageBy(QueryObj queryObj) {\n")
		  .append("\t\t").append("return "+Common.PROP_NAME_DAO+".findPageBy(queryObj);\n")
		  .append("\t}\n\n");
		return sb;
	}
	
	private static StringBuffer create_Method_save() {
		StringBuffer sb = new StringBuffer();
		sb.append("\t").append("public void save("+Common.DOMAIN_NAME+" "+Common.PROP_NAME+") {\n");
		
		boolean flag = false;
		sb.append("CREATE_EMP_TIME");
		for(int i=0;i<Common._FIELD.size();i++){
			String desc = Common.getQueryDesc(Common._FIELD.get(i));
			String fieldName = (Common._FIELD.get(i).charAt(0)+"").toUpperCase()+Common._FIELD.get(i).substring(1);
//			sb.append("\t\t").append("User user = (User) getCurrentUser();").append("\n");
			if("创建人".equals(desc)){
				flag = true;
				impUser = true;
				sb.append("\t\t").append("//设置创建人").append("\n");
				sb.append("\t\t").append(Common.PROP_NAME+".set"+fieldName+"(user.getEmployee().getCode());").append("\n");
			}else if("创建时间".equals(desc)){
				flag = true;
				impDate = true;
				sb.append("\t\t").append("//设置创建时间").append("\n");
				sb.append("\t\t").append(Common.PROP_NAME+".set"+fieldName+"(new Date());").append("\n");
			}
		}
		
		sb = new StringBuffer(sb.toString().replace("CREATE_EMP_TIME", flag?"\t\tUser user = (User) getCurrentUser();\n":""));
		
		sb.append("\t\t").append(Common.PROP_NAME_DAO+".save("+Common.PROP_NAME+");\n")
		  .append("\t}\n\n");
		return sb;
	}
	
	private static StringBuffer create_Method_update() {
		StringBuffer sb = new StringBuffer();
		sb.append("\t").append("public void update("+Common.DOMAIN_NAME+" "+Common.PROP_NAME+") {\n");
		sb.append("\t\t").append(Common.DOMAIN_NAME+" old = "+Common.PROP_NAME_DAO+".load("+Common.PROP_NAME+".getId());").append("\n");
		boolean flag = false;
		sb.append("UPDATE_EMP_TIME");
		for(int i=0;i<Common._FIELD.size();i++){
			String desc = Common.getQueryDesc(Common._FIELD.get(i));
			String fieldName = (Common._FIELD.get(i).charAt(0)+"").toUpperCase()+Common._FIELD.get(i).substring(1);
//			sb.append("\t\t").append("User user = (User) getCurrentUser();").append("\n");
			if("修改人".equals(desc)){
				flag = true;
				impUser = true;
				sb.append("\t\t").append("//设置修改人").append("\n");
				sb.append("\t\t").append(Common.PROP_NAME+".set"+fieldName+"(user.getEmployee().getCode());").append("\n");
			}else if("修改时间".equals(desc)){
				flag = true;
				impDate = true;
				sb.append("\t\t").append("//设置修改时间").append("\n");
				sb.append("\t\t").append(Common.PROP_NAME+".set"+fieldName+"(new Date());").append("\n");
			}
		}
		
		sb = new StringBuffer(sb.toString().replace("UPDATE_EMP_TIME", flag?"\t\tUser user = (User) getCurrentUser();\n":""));
		
		sb.append("\t\t").append("//复制对象属性").append("\n")
		  .append("\t\t").append("ClassUtil.copyProperties(old, "+Common.PROP_NAME+");").append("\n");
		sb.append("\t\t").append(Common.PROP_NAME_DAO+".update(old);\n")
		  .append("\t}\n\n");
		return sb;
	}
	
	private static StringBuffer create_Method_delete() {
		StringBuffer sb = new StringBuffer();
		sb.append("\t").append("public void delete(Long[] ids) {\n")
		  .append("\t\t").append("DetachedCriteria dc = DetachedCriteria.forClass("+Common.DOMAIN_NAME+".class);\n")
		  .append("\t\t").append("dc.add(Restrictions.in(\"id\", ids));\n")
		  .append("\t\t").append(Common.PROP_NAME_DAO+".removeBatch("+Common.PROP_NAME_DAO+".findBy(dc));\n")
		  .append("\t}\n\n");
		return sb;
	}
	
	private static StringBuffer create_Method_import(){
		StringBuffer sb = new StringBuffer();
		sb.append("\t").append("public void addImports(File uploadFile) throws FileNotFoundException, AbortException {\n")
		  .append("\t\t").append(Common.DOMAIN_NAME+"UploadReadRenderer render = new "+Common.DOMAIN_NAME+"UploadReadRenderer();\n")
		  .append("\t\t").append("render.set"+Common.DOMAIN_NAME+"Biz(this);\n");
		for(String biz : joinBiz){
			String str = (biz.charAt(0)+"").toUpperCase()+biz.substring(1);
			sb.append("\t\t").append("render.set"+str+"("+biz+");").append("\n");
		}
		sb.append("\t\t").append("ReadHelper helper = new ReadHelper(render);\n")
		  .append("\t\t").append("helper.processRead(new DefaultExcelReadDealer(new FileInputStream(uploadFile), 0, 1));\n")
		  .append("\t}\n\n");
		return sb;
	}
	
	private static StringBuffer create_Method_export(){
		StringBuffer sb = new StringBuffer();
		sb.append("\t").append("public String export(QueryObj queryObj) {\n")
		  .append("\t\t").append("//导出的Excel名称").append("\n")
		  .append("\t\t").append("String excelCode = ExcelNameConfig.ExcelName."+Common.DOMAIN_NAME+";").append("\n")
		  .append("\t\t").append("List<"+Common.DOMAIN_NAME+"> list = "+Common.PROP_NAME_DAO+".listExport(queryObj);").append("\n")
		  .append("\t\t").append("try {").append("\n")
		  .append("\t\t\t").append("return PoiUtil.exportExcel(excelCode, list);").append("\n")
		  .append("\t\t").append("} catch (Exception e) {").append("\n")
		  .append("\t\t\t").append("super.log.error(this.getClass().getName() + \" exportExcel failure!\", e);").append("\n")
		  .append("\t\t\t").append("throw new BizException(\"导出失败!\");").append("\n")
		  .append("\t\t").append("}").append("\n")
		  .append("\t").append("}").append("\n\n");
		  
		
		String content = sb.toString();
		content = content.replace("DEFINED_SIMPLEDATEFORMAT", existDate?"SimpleDateFormat sdf = new SimpleDateFormat(\"yyyy-MM-dd\");\n\n":"");
		return new StringBuffer(content);
	}
	
	private static String getImportClass(){
		StringBuffer sb = new StringBuffer();
		sb.append("package com.sf.module."+Common.MODULE_NAME+".biz;").append("\n")
		    .append("").append("\n")
		    .append("IMPORT_DATEUTIL")
		    .append("IMPORT_DECIMALFORMAT")
			.append("import java.util.List;").append("\n")
			.append("").append("\n")
			.append("import com.sf.framework.core.exception.BizException;").append("\n")
			.append("import com.sf.module.frameworkimpl.util.io.read.IReadRender;").append("\n")
			.append("JOIN_TABLE")
			.append("import com.sf.module."+Common.MODULE_NAME+".domain."+Common.DOMAIN_NAME+";").append("\n")
			.append("").append("\n")
			.append(Common.getClassDesc(Common.DOMAIN_DESC+" 导入Excel解析"))
			.append("public class "+Common.DOMAIN_NAME+"UploadReadRenderer implements IReadRender{").append("\n")
			.append("").append("\n")
			.append("\t").append("private "+Common.IBIZ_NAME+" "+Common.PROP_NAME_BIZ+";").append("\n")
			.append("DEFINE_IBIZ_NAME")
			.append("\t").append("").append("\n");
		StringBuffer headers = new StringBuffer();
		for(int i=0;i<Common.SHOW.size();i++){
			String header = Common.getQueryDesc(Common.SHOW.get(i));
			if("创建人".equals(header) || "创建时间".equals(header) || "修改人".equals(header) || "修改时间".equals(header)){
				continue;
			}
			if(header.indexOf("(") != -1){
				header = header.substring(0,header.indexOf("(")).trim();
			}
			headers.append("\""+header+"\"");
			if(i != Common.SHOW.size()-1){
				headers.append(",");
			}
		}
		sb.append("\t").append("public static final String[] header = new String[] {"+headers+"};").append("\n");
		sb.append("\t").append("").append("\n")
			.append("\t").append("public short getColumnCountLimit() {").append("\n")
			.append("\t\t").append("return 0;").append("\n")
			.append("\t").append("}").append("\n")
			.append("").append("\n")
			.append("\t").append("public int getRowCountLimit() {").append("\n")
			.append("\t\t").append("return 0;").append("\n")
			.append("\t").append("}").append("\n")
			.append("").append("\n")
			.append("\t").append("public void onRowRead(List<Object> datas, int rowIndex) {").append("\n")
			.append("        int temp = 0;").append("\n")
			.append("\t\t").append("for(int i=0;datas!=null&&i<datas.size();i++){").append("\n")
			.append("\t\t\t").append("Object obj = datas.get(i);").append("\n")
			.append("\t\t\t").append("if(obj == null || \"\".equals(obj.toString().trim())){").append("\n")
			.append("\t\t\t\t").append("temp++;").append("\n")
			.append("\t\t\t").append("}").append("\n")
			.append("\t\t").append("}").append("\n")
			.append("\t\t").append("if(temp != datas.size()){").append("\n")
			.append("\t\t\t").append("validatorLine(datas,rowIndex+2);").append("\n")
			.append("\t\t\t").append("saveLine(datas);").append("\n")
			.append("\t\t").append("}").append("\n")
			.append("\t").append("}").append("\n")
			.append("").append("\n")
			.append("\t").append("private void saveLine(List<Object> datas) {").append("\n")
			.append("DECIMAL_FORMAT")
			.append("\t\t").append(Common.DOMAIN_NAME+" "+Common.PROP_NAME+" = new "+Common.DOMAIN_NAME+"();").append("\n");
//			.append("\t\t").append("int i=0;").append("\n");
//			.append("\t\t").append("//机场名称").append("\n")
//			.append("\t\t").append("airPort.setAirportDesc(datas.get(i++).toString());").append("\n")
//			.append("\t\t").append("//经度坐标").append("\n")
//			.append("\t\t").append("airPort.setOrigin(Double.valueOf(datas.get(i++).toString()));").append("\n")
//			.append("\t\t").append("//纬度坐标").append("\n")
//			.append("\t\t").append("airPort.setLatitude(Double.valueOf(datas.get(i++).toString()));").append("\n")
//			.append("\t\t").append("//是否有效;默认为有效").append("\n")
//			.append("\t\t").append("airPort.setStatus(1L);").append("\n")
		
		boolean decimalFormat = false;
		int columnIndex=0;
		for(int i=0;i<Common.SHOW.size();i++){
			String desc = Common.getQueryDesc(Common.SHOW.get(i));
			
			if("创建人".equals(desc) || "创建时间".equals(desc) || "修改人".equals(desc) || "修改时间".equals(desc)){
				continue;
			}
			
			String fieldType = Common.getQueryType(Common.SHOW.get(i));
			String fieldName = (Common.SHOW.get(i).charAt(0)+"").toUpperCase()+Common.SHOW.get(i).substring(1);
			sb.append("\t\t").append("//"+desc).append("\n");
			
			String obj = Common.TABLE_NAME+":"+Common.getColumnName(Common.SHOW.get(i));
			
			if(Common.COMBO.contains(Common.SHOW.get(i))){
				desc = desc.replace("（", "(").replace("）", ")").replace("，", ",").trim();
				desc = desc.substring(desc.indexOf("(")+1, desc.indexOf(")"));
				String[] items = desc.split(",");
System.out.println(items);
				String propName = (fieldName.charAt(0)+"").toLowerCase()+fieldName.substring(1);
				sb.append("\t\t").append(fieldType+" "+propName+" = null;").append("\n");
				int index = 0;
				for (String item : items) {
					String[] arr = item.split("=");
					if(0 == index){
						sb.append("\t\t").append("if(\""+arr[1]+"\".equals(datas.get("+columnIndex+"))){").append("\n");
						sb.append("\t\t\t").append(propName+" = "+fieldType+".valueOf("+arr[0]+");").append("\n");
						sb.append("\t\t").append("}");
						if(index == items.length-1){
							sb.append("\n");
						}
					}else{
						sb.append("else if(\""+arr[1]+"\".equals(datas.get("+columnIndex+"))){").append("\n");
						sb.append("\t\t\t").append(propName+" = "+fieldType+".valueOf("+arr[0]+");").append("\n");
						sb.append("\t\t").append("}");
						if(index == items.length-1){
							sb.append("\n");
						}
					}
					index++;
				}
				columnIndex++;
				sb.append("\t\t").append(Common.PROP_NAME+".set"+fieldName+"("+propName+");").append("\n");
			}else if(Common.MAPPING.containsKey(obj)){
				String[] array = Common.MAPPING.get(obj).split(":");
				String entityName = Common.getEntityName(array[0]);
				
				String propName = (entityName.charAt(0)+"").toLowerCase()+entityName.substring(1);
				
				sb.append("\t\t").append(entityName+" "+propName+" = "+propName+"Biz.findBy(datas.get("+columnIndex+++").toString());").append("\n");
				sb.append("\t\t").append(Common.PROP_NAME+".set"+entityName+"("+propName+");").append("\n");
				
				joins+="import com.sf.module."+Common.MODULE_NAME+".domain."+entityName+";\n";
				defineBiz+="\n\tprivate I"+entityName+"Biz "+propName+"Biz;\n";
				joinBiz.add(propName+"Biz");
			}else{
				if("Date".equals(fieldType)){
					flag = true;
					sb.append("\t\t").append(Common.PROP_NAME+".set"+fieldName+"((Date)datas.get("+columnIndex+++"));").append("\n");
				}else if("Long".equals(fieldType)){
					decimalFormat=true;
					sb.append("\t\t").append(Common.PROP_NAME+".set"+fieldName+"(Long.valueOf(df.format(datas.get("+columnIndex+++"))));").append("\n");
				}else if("Integer".equals(fieldType)){
					decimalFormat=true;
					sb.append("\t\t").append(Common.PROP_NAME+".set"+fieldName+"(Integer.valueOf(df.format(datas.get("+columnIndex+++"))));").append("\n");
				}else if("Double".equals(fieldType)){
					sb.append("\t\t").append(Common.PROP_NAME+".set"+fieldName+"(Double.valueOf(datas.get("+columnIndex+++").toString()));").append("\n");
				}else if("Float".equals(fieldType)){
					sb.append("\t\t").append(Common.PROP_NAME+".set"+fieldName+"(Float.valueOf(datas.get("+columnIndex+++").toString()));").append("\n");
				}else{
					sb.append("\t\t").append(Common.PROP_NAME+".set"+fieldName+"(datas.get("+columnIndex+++").toString());").append("\n");
				}
			}
		}
		sb.append("\n").append("\t\t").append(Common.PROP_NAME_BIZ+".save("+Common.PROP_NAME+");").append("\n")
			.append("\t").append("}").append("\n")
			.append("").append("\n")
			.append("\t").append("private void validatorLine(List<Object> datas, int rowIndex) {").append("\n")
			.append("\t\t").append("validatorEmpty(datas,rowIndex);").append("\n")
			.append("\t\t").append("validatorLength(datas,rowIndex);").append("\n")
			.append("\t").append("}").append("\n")
			.append("").append("\n")
			.append("\t").append("/**").append("\n")
			.append("\t").append(" * 非空验证").append("\n")
			.append("\t").append(" * @author "+Common.getCurrUser()).append("\n")
			.append("\t").append(" * @date "+Common.getCurrDate()).append("\n")
			.append("\t").append(" * @param datas数据").append("\n")
			.append("\t").append(" * @param rowIndex 行号").append("\n")
			.append("\t").append(" */").append("\n")
			.append("\t").append("private void validatorEmpty(List<Object> datas, int rowIndex) {").append("\n")
			.append("\t\t").append("for(int i=0;i<header.length;i++){").append("\n")
			.append("\t\t\t").append("if(datas.get(i) == null || \"\".equals(String.valueOf(datas.get(i)).trim())){").append("\n")
			.append("\t\t\t\t").append("throw new BizException(\"第 \"+rowIndex+\" 行 【\" + header[i] + \"】 不能为空!\");").append("\n")
			.append("\t\t\t").append("}").append("\n")
			.append("\t\t").append("}").append("\n")
			.append("\t").append("}").append("\n")
			.append("\t").append("").append("\n")
			.append("\t").append("/**").append("\n")
			.append("\t").append(" * 长度验证").append("\n")
			.append("\t").append(" * @author "+Common.getCurrUser()).append("\n")
			.append("\t").append(" * @date "+Common.getCurrDate()).append("\n")
			.append("\t").append(" * @param datas数据").append("\n")
			.append("\t").append(" * @param rowIndex 行号").append("\n")
			.append("\t").append(" */").append("\n")
			.append("\t").append("private void validatorLength(List<Object> datas, int rowIndex) {").append("\n")
			.append("\t").append("}").append("\n")
			.append("\t").append("").append("\n")
			.append("").append("\n")
			.append("\t").append("public void processAfter() {").append("\n")
			.append("\t\t").append("").append("\n")
			.append("\t").append("}").append("\n")
			.append("").append("\n")
			.append("\t").append("public void processBefore() {").append("\n")
			.append("\t\t").append("").append("\n")
			.append("\t").append("}").append("\n")
			.append("").append("\n")
			.append("\t").append("public void set"+Common.DOMAIN_NAME+"Biz(I"+Common.DOMAIN_NAME+"Biz "+Common.PROP_NAME_BIZ+") {").append("\n")
			.append("\t\t").append("this."+Common.PROP_NAME_BIZ+" = "+Common.PROP_NAME_BIZ+";").append("\n")
			.append("\t").append("}").append("\n")
			.append("").append("\n");
		
		for(String biz : joinBiz){
			String str = (biz.charAt(0)+"").toUpperCase()+biz.substring(1);
			sb.append("\t").append("public void set"+str+"(I"+str+" "+biz+") {").append("\n")
			.append("\t\t").append("this."+biz+" = "+biz+";").append("\n")
			.append("\t").append("}").append("\n")
			.append("").append("\n");
		}
		
		sb.append("}").append("\n");
		String content = sb.toString();

		content = content.replace("IMPORT_DATEUTIL", flag?"import java.util.Date;\n":"");
		content = content.replace("JOIN_TABLE", joins);
		content = content.replace("DEFINE_IBIZ_NAME", defineBiz);
		content = content.replace("DECIMAL_FORMAT", decimalFormat?"\t\tDecimalFormat df = new DecimalFormat(\"0\");\n\n":"")
						 .replace("IMPORT_DECIMALFORMAT", decimalFormat?"import java.text.DecimalFormat;\n":"");
		return content;
	}
	
	public static void main(String[] args) {
//		BuildBiz.createInterface();
		BuildBiz.createClass();
		System.out.println(buff.toString());
	}
}
