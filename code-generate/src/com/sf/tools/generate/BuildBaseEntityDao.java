package com.sf.tools.generate;
import java.io.File;

import com.sf.tools.common.Common;


public class BuildBaseEntityDao {

	private static StringBuffer buff = new StringBuffer();
	
	public static void createBaseDao(){
		buff = new StringBuffer();
		buff.append("/***********************************************").append("\n")
			.append(" * Copyright sf-express.").append("\n")
			.append(" * All rights reserved.").append("\n")
			.append(" *").append("\n")
			.append(" * HISTORY").append("\n")
			.append(" **********************************************").append("\n")
			.append(" *  ID    DATE           PERSON       REASON").append("\n")
			.append(" *  1     2010-11-4      谢年兵        创建").append("\n")
			.append(" **********************************************/").append("\n")
			.append("package com.sf.module.").append(Common.MODULE_NAME).append(".dao;").append("\n")
			.append("").append("\n")
			.append("import java.util.List;").append("\n")
			.append("").append("\n")
			.append("import org.hibernate.Criteria;").append("\n")
			.append("import org.hibernate.HibernateException;").append("\n")
			.append("import org.hibernate.Session;").append("\n")
			.append("import org.hibernate.criterion.CriteriaSpecification;").append("\n")
			.append("import org.hibernate.criterion.DetachedCriteria;").append("\n")
			.append("import org.hibernate.criterion.Order;").append("\n")
			.append("import org.hibernate.criterion.Projections;").append("\n")
			.append("import org.springframework.orm.hibernate3.HibernateCallback;").append("\n")
			.append("").append("\n")
			.append("import com.sf.framework.base.IPage;").append("\n")
			.append("import com.sf.framework.base.domain.IEntity;").append("\n")
			.append("import com.sf.framework.core.Page;").append("\n")
			.append("import com.sf.framework.core.exception.DaoException;").append("\n")
			.append("import com.sf.framework.server.base.dao.IEntityDao;").append("\n")
			.append("import com.sf.framework.util.StringUtils;").append("\n")
			.append("").append("\n")
			.append("").append("\n")
			.append("/**").append("\n")
			.append(" *").append("\n")
			.append(" * 主要是用来修正BaseEntityDao里的一些问题").append("\n")
			.append(" * @author 谢年兵  2010-11-4").append("\n")
			.append(" *").append("\n")
			.append(" */").append("\n")
			.append("public class BaseEntityDao<E extends IEntity> extends com.sf.framework.server.base.dao.BaseEntityDao<E> implements IEntityDao<E> {").append("\n")
			.append("").append("\n")
			.append("\t").append("@SuppressWarnings({ \"unchecked\", \"rawtypes\" })").append("\n")
			.append("\t").append("@Override").append("\n")
			.append("\t").append("public IPage<E> findPageBy(final DetachedCriteria detachedCriteria, final int pageSize,").append("\n")
			.append("\t\t\t").append("final int pageIndex) throws DaoException {").append("\n")
			.append("\t\t").append("return (IPage<E>) getHibernateTemplate().execute(new HibernateCallback() {").append("\n")
			.append("\t\t\t").append("public Object doInHibernate(Session session) throws HibernateException {").append("\n")
			.append("\t\t\t\t").append("Criteria criteria = detachedCriteria.getExecutableCriteria(session);").append("\n")
			.append("\t\t\t\t").append("Long total = (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();").append("\n")
			.append("\t\t\t\t").append("criteria.setProjection(null);").append("\n")
			.append("\t\t\t\t").append("criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY); //去重复记录").append("\n")
			.append("\t\t\t\t").append("criteria.setFirstResult(pageSize * (pageIndex));").append("\n")
			.append("\t\t\t\t").append("criteria.setMaxResults(pageSize);").append("\n")
			.append("\t\t\t\t").append("return new Page<E>(criteria.list(), total, pageSize, pageIndex);").append("\n")
			.append("\t\t\t").append("}").append("\n")
			.append("\t\t").append("});").append("\n")
			.append("\t").append("}").append("\n")
			.append("").append("\n")
			.append("\t").append("@SuppressWarnings({ \"unchecked\", \"rawtypes\" })").append("\n")
			.append("\t").append("@Override").append("\n")
			.append("\t").append("public List<E> findBy(final DetachedCriteria detachedCriteria)").append("\n")
			.append("\t\t\t").append("throws DaoException {").append("\n")
			.append("\t\t").append("return (List<E>) getHibernateTemplate().execute(new HibernateCallback() {").append("\n")
			.append("\t\t\t").append("public Object doInHibernate(Session session) throws HibernateException {").append("\n")
			.append("\t\t\t\t").append("Criteria criteria = detachedCriteria.getExecutableCriteria(session);").append("\n")
			.append("\t\t\t\t").append("criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY); //去重复记录").append("\n")
			.append("\t\t\t\t").append("return criteria.list();").append("\n")
			.append("\t\t\t").append("}").append("\n")
			.append("\t\t").append("});").append("\n")
			.append("\t").append("}").append("\n")
			.append("").append("\n")
			.append("\t").append("@SuppressWarnings({ \"unchecked\", \"rawtypes\" })").append("\n")
			.append("\t").append("@Override").append("\n")
			.append("\t").append("public IPage<E> findPageBy(final DetachedCriteria detachedCriteria, final int pageSize,").append("\n")
			.append("\t\t\t").append("final int pageIndex, final String sortField, final boolean isAsc) throws DaoException {").append("\n")
			.append("\t\t").append("return (IPage<E>) getHibernateTemplate().execute(new HibernateCallback() {").append("\n")
			.append("\t\t\t").append("public Object doInHibernate(Session session) throws HibernateException {").append("\n")
			.append("\t\t\t\t").append("Criteria criteria = detachedCriteria.getExecutableCriteria(session);").append("\n")
			.append("\t\t\t\t").append("if (StringUtils.isNotEmpty(sortField)) {").append("\n")
			.append("\t\t\t\t\t").append("criteria.addOrder(isAsc ? Order.asc(sortField) : Order.desc(sortField));").append("\n")
			.append("\t\t\t\t").append("}").append("\n")
			.append("\t\t\t\t").append("Long total = (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();").append("\n")
			.append("\t\t\t\t").append("criteria.setProjection(null);").append("\n")
			.append("\t\t\t\t").append("criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY); //去重复记录").append("\n")
			.append("\t\t\t\t").append("criteria.setFirstResult(pageSize * (pageIndex));").append("\n")
			.append("\t\t\t\t").append("criteria.setMaxResults(pageSize);").append("\n")
			.append("\t\t\t\t").append("return new Page<E>(criteria.list(), total, pageSize, pageIndex);").append("\n")
			.append("\t\t\t").append("}").append("\n")
			.append("\t\t").append("});").append("\n")
			.append("\t").append("}").append("\n")
			.append("\t").append("").append("\n")
			.append("}").append("\n");

		File file = new File(Common.SAVE_PATH+File.separator+"dao");
		if(!file.exists()){
			file.mkdirs();
		}
		Common.write(new File(file,"BaseEntityDao.java"), buff.toString());
		
		System.out.println("BaseEntityDao.java 已生成.....");
	}
	
	public static void main(String[] args) {
//		createHibernate();
//		createSpring();
//		createStruts();
	}
}
