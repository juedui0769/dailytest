package com.sf.tools.generate;
import java.io.File;

import com.sf.tools.common.Common;




public class BuildEntity {

	private static StringBuffer buff = new StringBuffer();
	
	private static void importPackage(){
		buff.append("package ").append(Common.PACKAGE_NAME).append(".domain;").append("\n\n");
		if(Common._TYPE.contains("Date")){
			buff.append("import java.util.Date;").append("\n\n");
		}
		buff.append("import com.sf.framework.base.domain.BaseEntity;").append("\n\n");
//		buff.append(sb);
	}
	
	private static void printProperty(){
		for(int i=0;i<Common._FIELD.size();i++){
			if(Common.PRIMATY_KEY.contains(Common._FIELD.get(i))){
				continue;
			}
			buff.append("\t").append("/**\n").append("\t").append(" * ").append(Common._DESC.get(i)).append("\n").append("\t").append(" */\n");
			
			String key = Common.TABLE_NAME+":"+Common._COLUMN.get(i);
			if(Common.MAPPING.containsKey(key)){
				String mappTable = Common.MAPPING.get(key).split(":")[0];
				String entityName = Common.getEntityName(mappTable);
				String entityPropName = (entityName.charAt(0)+"").toLowerCase()+entityName.substring(1);
				buff.append("\t").append("private ").append(entityName).append(" ").append(entityPropName).append(";\n");
				
			}else{
				buff.append("\t").append("private ").append(Common._TYPE.get(i)).append(" ").append(Common._FIELD.get(i)).append(";\n");
			}
		}
	}

	private static void printSetAndGet(){
		buff.append("\n\n");
		for(int i=0;i<Common._FIELD.size();i++){
			if(Common.PRIMATY_KEY.contains(Common._FIELD.get(i))){
				continue;
			}
			buff.append("\t").append("/**\n").append("\t").append(" * 获取").append(Common._DESC.get(i)).append("\n").append("\t").append(" */\n");
			
			String key = Common.TABLE_NAME+":"+Common._COLUMN.get(i);
			if(Common.MAPPING.containsKey(key)){
				String mappTable = Common.MAPPING.get(key).split(":")[0];
				String entityName = Common.getEntityName(mappTable);
				String entityPropName = (entityName.charAt(0)+"").toLowerCase()+entityName.substring(1);
				
				
				buff.append("\t").append("public ").append(entityName).append(" get")
				.append(entityName)
				.append("() {\n")
				.append("\t").append("\t").append("return ").append(entityPropName).append(";\n\t}\n");
				
				buff.append("\t").append("/**\n").append("\t").append(" * 设置").append(Common._DESC.get(i)).append("\n").append("\t").append(" */\n")
				  .append("\t").append("public ").append("void").append(" set")
				  .append(entityName)
				  .append("(").append(entityName).append(" ").append(entityPropName).append(") {\n")
				  .append("\t").append("\t").append("this.").append(entityPropName).append(" = ").append(entityPropName).append(";\n\t}\n");
			}else{
				buff.append("\t").append("public ").append(Common._TYPE.get(i)).append(" get")
				.append((Common._FIELD.get(i).charAt(0)+"").toUpperCase()+Common._FIELD.get(i).substring(1))
				.append("() {\n")
				.append("\t").append("\t").append("return ").append(Common._FIELD.get(i)).append(";\n\t}\n");

				buff.append("\t").append("/**\n").append("\t").append(" * 设置").append(Common._DESC.get(i)).append("\n").append("\t").append(" */\n")
				.append("\t").append("public ").append("void").append(" set")
				.append((Common._FIELD.get(i).charAt(0)+"").toUpperCase()+Common._FIELD.get(i).substring(1))
				.append("(").append(Common._TYPE.get(i)).append(" ").append(Common._FIELD.get(i)).append(") {\n")
				.append("\t").append("\t").append("this.").append(Common._FIELD.get(i)).append(" = ").append(Common._FIELD.get(i)).append(";\n\t}\n");
			}
		}
		buff.append("\n}");
	}
	
	
	public static void createDoMain(){
//		System.out.println("******");
		buff = new StringBuffer();
		importPackage();
		buff.append(Common.getClassDesc(Common.DOMAIN_DESC));
		buff.append("public class ").append(Common.DOMAIN_NAME).append(" extends BaseEntity {").append("\n\n");
		buff.append("\tprivate static final long serialVersionUID = 1L;").append("\n\n");
		printProperty();
		printSetAndGet();
//			System.out.println(buff.toString());
		
		File file = new File(Common.SAVE_PATH+File.separator+"domain");
		if(!file.exists()){
			file.mkdirs();
		}
		Common.write(new File(file,Common.DOMAIN_NAME+".java"), buff.toString());
		
		System.out.println(Common.DOMAIN_NAME+".java 已生成.....");
	}
	
	public static void main(String[] args) {
//		createDoMain(Common.DOMAIN_NAME,Common.filePath);
		
	}
}
