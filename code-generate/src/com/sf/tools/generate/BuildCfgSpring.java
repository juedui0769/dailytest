package com.sf.tools.generate;
import java.io.File;
import java.util.List;

import com.sf.tools.common.Common;


public class BuildCfgSpring {

	private static StringBuffer buff = new StringBuffer();
	private static StringBuffer sb = new StringBuffer();
	
	public static void createSpring(int index, boolean isLastNext, List<String> joinBizs){
		if(index == 0){
			sb = new StringBuffer();
			buff = new StringBuffer();
			buff.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append("\n")
				.append("<beans xmlns=\"http://www.springframework.org/schema/beans\"").append("\n")
				.append("\t").append("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"").append("\n")
				.append("\t").append("xmlns:context=\"http://www.springframework.org/schema/context\"").append("\n")
				.append("\t").append("xsi:schemaLocation=\"http://www.springframework.org/schema/beans").append("\n")
				.append("\t").append("http://www.springframework.org/schema/beans/spring-beans.xsd").append("\n")
				.append("\t").append("http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd\">").append("\n\n");
			
			buff.append("SPRING-BEANS-MARK");
			
			buff.append("</beans>");
		}
		
		if("注解".equals(Common.GENERATE_SPRING_TYPE)){
//			return ;
			if(isLastNext){
				sb.append("\t").append("<context:component-scan base-package=\""+Common.PACKAGE_NAME+".action\" />").append("\n")
				.append("\t").append("<context:component-scan base-package=\""+Common.PACKAGE_NAME+".biz\" />").append("\n")
				.append("\t").append("<context:component-scan base-package=\""+Common.PACKAGE_NAME+".dao\" />").append("\n\n")
				.append("\t").append("<!-- 其中base-package为需要扫描的包（含所有子包）").append("\n")
				.append("\t").append("@Controller用于标注控制层组件（如struts中的action） .*Action类").append("\n")
				.append("\t").append("@Service用于标注业务层组件   .*Biz类").append("\n")
				.append("\t").append("@Repository用于标注数据访问组件，即DAO组件  .*Dao类").append("\n")
				.append("\t").append("@Component泛指组件，当组件不好归类的时候，我们可以使用这个注解进行标注。").append("\n")
				.append("\t").append("bean的默认名称是类名（头字母小 写），").append("\n")
				.append("\t").append("bean默认是单例的").append("\n")
				.append("\t").append("使用 @Service(\"beanName\") @Scope(\"prototype\")来改变").append("\n")
				.append("\t").append("-->").append("\n");
			}
			
		}else{
			sb.append("\t").append("<!-- "+Common.DOMAIN_DESC+" -->").append("\n")
			.append("\t").append("<bean id=\""+Common.PROP_NAME_DAO+"\" class=\""+Common.PACKAGE_NAME+".dao."+Common.DAO_NAME+"\"/>").append("\n")
			.append("\t").append("<bean id=\""+Common.PROP_NAME_BIZ+"\" parent=\"baseLocalTxProxy\">").append("\n")
			.append("\t\t").append("<property name=\"target\">").append("\n")
			.append("\t\t\t").append("<bean class=\"com.sf.module."+Common.MODULE_NAME+".biz."+Common.BIZ_NAME+"\">").append("\n")
			.append("\t\t\t\t").append("<property name=\""+Common.PROP_NAME_DAO+"\" ref=\""+Common.PROP_NAME_DAO+"\"/>").append("\n");
			for (String joinBiz : joinBizs) {
				sb.append("\t\t\t\t").append("<property name=\""+joinBiz+"\" ref=\""+joinBiz+"\"/>").append("\n");
			}
			sb.append("\t\t\t").append("</bean>").append("\n")
			.append("\t\t").append("</property>").append("\n")
			.append("\t").append("</bean>").append("\n")
			
			.append("\t").append("<bean id =\""+Common.PROP_NAME_ACTION+"\" class=\""+Common.PACKAGE_NAME+".action."+Common.ACTION_NAME+"\" >").append("\n")
			.append("\t\t").append("<property name=\""+Common.PROP_NAME_BIZ+"\" ref=\""+Common.PROP_NAME_BIZ+"\"/>").append("\n")
			.append("\t").append("</bean>").append("\n\n");
		}
		
//		System.out.println(buff.toString());
		if(isLastNext){
			
			String content = buff.toString().replace("SPRING-BEANS-MARK", sb);
			
			File file = new File(Common.SAVE_PATH+File.separator+"META-INF"+File.separator+"config");
			if(!file.exists()){
				file.mkdirs();
			}
			Common.write(new File(file,"beans.xml"), content);
		}
//		System.out.println("beans.xml 已生成.....");
	}
	
	public static void main(String[] args) {
//		createHibernate();
//		createSpring();
//		createStruts();
	}
}
