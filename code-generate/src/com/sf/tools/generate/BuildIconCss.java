package com.sf.tools.generate;
import java.io.File;

import com.sf.tools.common.Common;


public class BuildIconCss {

	private static StringBuffer buff = new StringBuffer();
	
	public static void createIconCss(){
		buff = new StringBuffer();
		buff.append(".search {").append("\n")
			.append("\t").append("background-image:url(../../images/"+Common.MODULE_NAME+"/search.gif);").append("\n")
			.append("}").append("\n")
			.append(".add {").append("\n")
			.append("\t").append("background-image:url(../../images/"+Common.MODULE_NAME+"/add.gif);").append("\n")
			.append("}").append("\n")
			.append(".edit {").append("\n")
			.append("\t").append("background-image:url(../../images/"+Common.MODULE_NAME+"/edit.gif);").append("\n")
			.append("}").append("\n")
			.append(".delete {").append("\n")
			.append("\t").append("background-image:url(../../images/"+Common.MODULE_NAME+"/delete.gif);").append("\n")
			.append("}").append("\n")
			.append(".save {").append("\n")
			.append("\t").append("background-image:url(../../images/"+Common.MODULE_NAME+"/save.gif);").append("\n")
			.append("}").append("\n")
			.append(".reset {").append("\n")
			.append("\t").append("background-image:url(../../images/"+Common.MODULE_NAME+"/reset.gif);").append("\n")
			.append("}").append("\n")
			.append(".download {").append("\n")
			.append("\t").append("background-image:url(../../images/"+Common.MODULE_NAME+"/download.gif);").append("\n")
			.append("}").append("\n")
			.append(".import {").append("\n")
			.append("\t").append("background-image:url(../../images/"+Common.MODULE_NAME+"/import.gif);").append("\n")
			.append("}").append("\n")
			.append(".export {").append("\n")
			.append("\t").append("background-image:url(../../images/"+Common.MODULE_NAME+"/export.gif);").append("\n")
			.append("}").append("\n");
		
		
//		System.out.println(buff.toString());

		File file = new File(Common.SAVE_PATH+File.separator+"META-INF"+File.separator+"styles");
		if(!file.exists()){
			file.mkdirs();
		}
		Common.write(new File(file,"icon.css"), buff.toString());
		
		System.out.println("icon.css 已生成.....");
	}
	
	public static void main(String[] args) {
//		createHibernate();
//		createSpring();
//		createStruts();
	}
}
