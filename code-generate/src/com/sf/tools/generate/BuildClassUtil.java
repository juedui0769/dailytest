package com.sf.tools.generate;
import java.io.File;

import com.sf.tools.common.Common;





public class BuildClassUtil {

	private static StringBuffer buff = new StringBuffer();
	
	public static void createBuildClassUtil(){
		buff = new StringBuffer();
		buff.append("package com.sf.module."+Common.MODULE_NAME+".util;").append("\n")
		.append("").append("\n")
		.append("import java.beans.PropertyDescriptor;").append("\n")
		.append("").append("\n")
		.append("import org.springframework.beans.BeanWrapper;").append("\n")
		.append("import org.springframework.beans.BeanWrapperImpl;").append("\n")
		.append("").append("\n")
		.append("public class ClassUtil {").append("\n")
		.append("").append("\n")
		.append("\t").append("/**").append("\n")
		.append("\t").append(" * 复制对象属性").append("\n")
		.append("\t").append(" * @param dest 目标Bean").append("\n")
		.append("\t").append(" * @param src  实体Bean").append("\n")
		.append("\t").append(" */").append("\n")
		.append("\t").append("public static void copyProperties(Object dest,Object src){").append("\n")
		.append("\t\t").append("if (!dest.getClass().equals(src.getClass())) {").append("\n")
		.append("\t\t\t").append("throw new ClassCastException();").append("\n")
		.append("\t\t").append("}").append("\n")
		.append("\t\t").append("BeanWrapper srcWrapper = new BeanWrapperImpl(src);").append("\n")
		.append("\t\t").append("BeanWrapper destWrapper = new BeanWrapperImpl(dest);").append("\n")
		.append("\t\t").append("PropertyDescriptor pds[] = srcWrapper.getPropertyDescriptors();").append("\n")
		.append("\t\t").append("if (null != pds && 0 < pds.length) {").append("\n")
		.append("\t\t\t").append("for (PropertyDescriptor pd : pds) {").append("\n")
		.append("\t\t\t\t").append("String propName = pd.getName();").append("\n")
		.append("\t\t\t\t").append("// 读或写方法为空时不处理").append("\n")
		.append("\t\t\t\t").append("if (null == propName || null == pd.getWriteMethod()").append("\n")
		.append("\t\t\t\t\t\t").append("|| null == pd.getReadMethod()) {").append("\n")
		.append("\t\t\t\t\t").append("continue;").append("\n")
		.append("\t\t\t\t").append("}").append("\n")
		.append("\t\t\t\t").append("Object propValue = srcWrapper.getPropertyValue(propName);").append("\n")
		.append("\t\t\t\t").append("if(null == propValue){").append("\n")
		.append("\t\t\t\t\t").append("continue;").append("\n")
		.append("\t\t\t\t").append("}").append("\n")
		.append("\t\t\t\t").append("destWrapper.setPropertyValue(propName, propValue);").append("\n")
		.append("\t\t\t").append("}").append("\n")
		.append("\t\t").append("}").append("\n")
		.append("\t").append("}").append("\n")
		.append("}").append("\n");

		String content = buff.toString();
		
		File file = new File(Common.SAVE_PATH+File.separator+"action");
		if(!file.exists()){
			file.mkdirs();
		}
		Common.write(new File(file,"DownloadAction.java"), content);
		
		System.out.println("DownloadAction.java 已生成......");
		
	}
	
}
