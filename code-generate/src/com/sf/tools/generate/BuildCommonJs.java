package com.sf.tools.generate;
import java.io.File;

import com.sf.tools.common.Common;


public class BuildCommonJs {

	private static StringBuffer buff = new StringBuffer();
	
	public static void createCommonJs(){
		buff = new StringBuffer();
		buff.append("// <%@page contentType=\"text/html; charset=utf-8\" %>").append("\n")
		.append("Ext.namespace('SF');").append("\n")
		.append("").append("\n")
		.append("Ext.QuickTips.init();").append("\n")
		.append("").append("\n")
		.append("Ext.pageSize = 25;").append("\n")
		.append("Ext.base = \"/"+Common.MODULE_NAME+"\";").append("\n")
		.append("Ext.HOME = \"../ext-3.4.0/\";").append("\n")
		.append("Ext.BLANK_IMAGE_URL = Ext.HOME + \"resources/images/default/s.gif\";").append("\n")
		.append("/** 请求有效时间十五分钟 */").append("\n")
		.append("Ext.Ajax.timeout = 15 * 60 * 1000;").append("\n")
		.append("Ext.Msg.minWidth = 160;").append("\n")
		.append("").append("\n")
		.append("Ext.form.Field.prototype.msgTarget = \"qtip\";	//side|qtip").append("\n")
		.append("").append("\n")
		.append("/** 最大长度计算汉字处理函数 */").append("\n")
		.append("maxLengthOverride = function(value) {").append("\n")
		.append("\t").append("if (this.allowBlank == false) {").append("\n")
		.append("\t\t").append("if (value == null || value == '') {").append("\n")
		.append("\t\t\t").append("this.markInvalid(String.format(this.blankText, value));").append("\n")
		.append("\t\t\t").append("return false;").append("\n")
		.append("\t\t").append("}").append("\n")
		.append("\t").append("}").append("\n")
		.append("\t").append("var maxLen = this.maxLength;").append("\n")
		.append("\t").append("var maxLenText = this.maxLengthText;").append("\n")
		.append("\t").append("if (maxLen != null && maxLen != 'undefined' && maxLen > 0) {").append("\n")
		.append("\t\t").append("var regex = /[^\\x00-\\xff]/g;").append("\n")
		.append("\t\t").append("var len;").append("\n")
		.append("\t\t").append("var repalceValue = value.replace(regex, '***');").append("\n")
		.append("\t\t").append("len = repalceValue.length;").append("\n")
		.append("\t").append("}").append("\n")
		.append("\t").append("if (len > maxLen) {").append("\n")
		.append("\t\t").append("this.markInvalid(String.format(maxLenText, value));").append("\n")
		.append("\t\t").append("return false;").append("\n")
		.append("\t").append("}").append("\n")
		.append("\t").append("return true;").append("\n")
		.append("};").append("\n")
		.append("").append("\n")
		.append("maxLengthValid = function(field,value) {").append("\n")
		.append("\t").append("if (field.allowBlank == false) {").append("\n")
		.append("\t\t").append("if (value == null || value == '') {").append("\n")
		.append("\t\t\t").append("return String.format(field.blankText, value);").append("\n")
		.append("\t\t").append("}").append("\n")
		.append("\t").append("}").append("\n")
		.append("\t").append("var maxLen = field.maxLength;").append("\n")
		.append("\t").append("var maxLenText = field.maxLengthText;").append("\n")
		.append("\t").append("if (maxLen != null && maxLen != 'undefined' && maxLen > 0) {").append("\n")
		.append("\t\t").append("var regex = /[^\\x00-\\xff]/g;").append("\n")
		.append("\t\t").append("var len;").append("\n")
		.append("\t\t").append("var repalceValue = value.replace(regex, '***');").append("\n")
		.append("\t\t").append("len = repalceValue.length;").append("\n")
		.append("\t").append("}").append("\n")
		.append("\t").append("if (len > maxLen) {").append("\n")
		.append("\t\t").append("return String.format(maxLenText+\"当前已输入\"+len+\"个.\");").append("\n")
		.append("\t").append("}").append("\n")
		.append("\t").append("return true;").append("\n")
		.append("};").append("\n")
		.append("").append("\n")
		.append("/** 日期验证,开始日期不能大于结束日期 */").append("\n")
		.append("Ext.apply(Ext.form.VTypes, {").append("\n")
		.append("\t").append("daterangeText : '',").append("\n")
		.append("\t").append("daterange : function(val, field) {").append("\n")
		.append("").append("\n")
		.append("\t\t").append("var date = field.parseDate(val);").append("\n")
		.append("").append("\n")
		.append("\t\t").append("if (!date) {").append("\n")
		.append("\t\t\t").append("this.daterangeText = String.format(field.invalidText, val,").append("\n")
		.append("\t\t\t\t\t").append("field.format);").append("\n")
		.append("\t\t\t").append("return false;").append("\n")
		.append("\t\t").append("}").append("\n")
		.append("\t\t").append("if (field.startDateField) {").append("\n")
		.append("\t\t\t").append("var start = Ext.getCmp(field.startDateField);").append("\n")
		.append("\t\t\t").append("if (!start.maxValue || (date.getTime() != start.maxValue.getTime())) {").append("\n")
		.append("\t\t\t\t").append("start.setMaxValue(date);").append("\n")
		.append("\t\t\t\t").append("start.validate();").append("\n")
		.append("\t\t\t").append("}").append("\n")
		.append("\t\t").append("} else if (field.endDateField) {").append("\n")
		.append("\t\t\t").append("var end = Ext.getCmp(field.endDateField);").append("\n")
		.append("\t\t\t").append("if (!end.minValue || (date.getTime() != end.minValue.getTime())) {").append("\n")
		.append("\t\t\t\t").append("end.setMinValue(date);").append("\n")
		.append("\t\t\t\t").append("end.validate();").append("\n")
		.append("\t\t\t").append("}").append("\n")
		.append("\t\t").append("}").append("\n")
		.append("\t\t").append("/*").append("\n")
		.append("\t\t").append(" * Always return true since we're only using this vtype to set the").append("\n")
		.append("\t\t").append(" * min/max allowed values (these are tested for after the vtype test)").append("\n")
		.append("\t\t").append(" */").append("\n")
		.append("\t\t").append("return true;").append("\n")
		.append("\t").append("}").append("\n")
		.append("});").append("\n")
		.append("").append("\n")
		.append("/**").append("\n")
		.append(" * 重写GridPanel 隐藏列表头的头部下拉菜单").append("\n")
		.append(" */").append("\n")
		.append("Ext.override(Ext.grid.GridPanel, {").append("\n")
		.append("\t").append("enableHdMenu : false").append("\n")
		.append("});").append("\n")
		.append("\t\t").append("").append("\n")
		.append("/**").append("\n")
		.append(" * 重写Ext.grid.RowNumberer").append("\n")
		.append(" * 使GRID分页时实现序号自增").append("\n")
		.append(" */").append("\n")
		.append("/*").append("\n")
		.append("Ext.override(Ext.grid.RowNumberer,{").append("\n")
		.append("\t").append("renderer : function(value, cellmeta, record, rowIndex, columnIndex,store) {").append("\n")
		.append("\t\t").append("if(store.lastOptions && store.lastOptions.params){").append("\n")
		.append("\t\t\t").append("return store.lastOptions.params.start + rowIndex + 1;").append("\n")
		.append("\t\t").append("}").append("\n")
		.append("\t\t").append("return rowIndex + 1;").append("\n")
		.append("\t").append("}").append("\n")
		.append("});").append("\n")
		.append("*/").append("\n")
		.append("").append("\n")
		.append("Ext.renderDateTime = function(value) {").append("\n")
		.append("\t").append("if (value) {").append("\n")
		.append("\t\t").append("value = value.replace('T', ' ');").append("\n")
		.append("\t\t").append("value = value.substring(0,value.length-3);").append("\n")
		.append("\t\t").append("return value;").append("\n")
		.append("\t").append("}").append("\n")
		.append("\t").append("return '';").append("\n")
		.append("};").append("\n")
		.append("").append("\n")
		.append("Ext.renderDate = function(value) {").append("\n")
		.append("\t").append("if (value) {").append("\n")
		.append("\t\t").append("return value.split('T')[0];").append("\n")
		.append("\t").append("}").append("\n")
		.append("\t").append("return '';").append("\n")
		.append("};").append("\n")
		.append("").append("\n")
		.append("Ext.renderTime = function(value) {").append("\n")
		.append("\t").append("if (value && value.length > 12) {").append("\n")
		.append("\t\t").append("return value.substr(11, value.length);").append("\n")
		.append("\t").append("}").append("\n")
		.append("\t").append("return '';").append("\n")
		.append("};").append("\n")
		.append("").append("\n")
		.append("/**").append("\n")
		.append(" * 给所有设置为readOnly的输入灰色显示").append("\n")
		.append(" */").append("\n")
		.append("Ext.override(Ext.form.Field, {").append("\n")
		.append("\t").append("getAutoCreate : function(){").append("\n")
		.append("\t\t").append("var cfg = Ext.isObject(this.autoCreate) ?").append("\n")
		.append("\t\t").append("              this.autoCreate : Ext.apply({}, this.defaultAutoCreate);").append("\n")
		.append("\t\t").append("// 文件域只读").append("\n")
		.append("\t\t").append("if( this.inputType=='file' ){").append("\n")
		.append("\t\t\t").append("cfg.onkeydown='return false;'").append("\n")
		.append("\t\t").append("}").append("\n")
		.append("\t\t").append("if(this.id && !cfg.id){").append("\n")
		.append("\t\t").append("    cfg.id = this.id;").append("\n")
		.append("\t\t").append("}").append("\n")
		.append("\t\t").append("return cfg;").append("\n")
		.append("\t").append("},").append("\n")
		.append("    setReadOnly : function(readOnly) {").append("\n")
		.append("        if (this.rendered) {").append("\n")
		.append("            this.el.dom.readOnly = readOnly;").append("\n")
		.append("            if (readOnly) {").append("\n")
		.append("                this.el.setStyle('color', \"#C0C0C0\");").append("\n")
		.append("            } else {").append("\n")
		.append("                this.el.setStyle('color', \"#000000\");").append("\n")
		.append("            }").append("\n")
		.append("        }").append("\n")
		.append("        this.readOnly = readOnly;").append("\n")
		.append("    },").append("\n")
		.append("    onBlur : function(){").append("\n")
		.append("        this.beforeBlur();").append("\n")
		.append("        if(this.focusClass){").append("\n")
		.append("            this.el.removeClass(this.focusClass);").append("\n")
		.append("        }").append("\n")
		.append("        this.hasFocus = false;").append("\n")
		.append("        if(this.validationEvent !== false && (this.validateOnBlur || this.validationEvent == 'blur')){").append("\n")
		.append("            this.validate();").append("\n")
		.append("        }").append("\n")
		.append("        var v = this.getValue();").append("\n")
		.append("        if((this.xtype=='textfield' || this.xtype=='textarea' || this.xtype=='combo') && this.inputType!='file'){").append("\n")
		.append("\t").append("        if((v || v != null) && !Ext.isNumber(v)){").append("\n")
		.append("\t\t").append("        v = v.trim();").append("\n")
		.append("\t\t").append("        this.setValue(v);").append("\n")
		.append("\t").append("        }").append("\n")
		.append("        }").append("\n")
		.append("        if(String(v) !== String(this.startValue)){").append("\n")
		.append("            this.fireEvent('change', this, v, this.startValue);").append("\n")
		.append("        }").append("\n")
		.append("        this.fireEvent('blur', this);").append("\n")
		.append("        this.postBlur();").append("\n")
		.append("    }").append("\n")
		.append("});").append("\n")
		.append("").append("\n")
		.append("/**").append("\n")
		.append(" * 给所有设置为readOnly的输入灰色显示").append("\n")
		.append(" */").append("\n")
		.append("Ext.override(Ext.form.TriggerField, {").append("\n")
		.append("\t").append("updateEditState: function(){").append("\n")
		.append("        if(this.rendered){").append("\n")
		.append("            if (this.readOnly) {").append("\n")
		.append("                this.el.dom.readOnly = true;").append("\n")
		.append("                this.el.addClass('x-trigger-noedit');").append("\n")
		.append("                this.mun(this.el, 'click', this.onTriggerClick, this);").append("\n")
		.append("                this.trigger.addClass('x-item-disabled');").append("\n")
		.append("            } else {").append("\n")
		.append("                if (!this.editable) {").append("\n")
		.append("                    this.el.dom.readOnly = true;").append("\n")
		.append("                    this.el.addClass('x-trigger-noedit');").append("\n")
		.append("                    this.mon(this.el, 'click', this.onTriggerClick, this);").append("\n")
		.append("                } else {").append("\n")
		.append("                    this.el.dom.readOnly = false;").append("\n")
		.append("                    this.el.removeClass('x-trigger-noedit');").append("\n")
		.append("                    this.mun(this.el, 'click', this.onTriggerClick, this);").append("\n")
		.append("                }").append("\n")
		.append("                this.trigger.removeClass('x-item-disabled');").append("\n")
		.append("                this.trigger.setDisplayed(!this.hideTrigger);").append("\n")
		.append("            }").append("\n")
		.append("            this.onResize(this.width || this.wrap.getWidth()-2);").append("\n")
		.append("        }").append("\n")
		.append("    },").append("\n")
		.append("    setReadOnly: function(readOnly){").append("\n")
		.append("        if(readOnly != this.readOnly){").append("\n")
		.append("            this.readOnly = readOnly;").append("\n")
		.append("            this.updateEditState();").append("\n")
		.append("        }").append("\n")
		.append("        if(this.el){").append("\n")
		.append("\t").append("        if(this.readOnly){").append("\n")
		.append("\t\t").append("        this.el.setStyle('color', \"#C0C0C0\");").append("\n")
		.append("\t").append("        }else{").append("\n")
		.append("\t\t").append("        this.el.setStyle('color', \"#000000\");").append("\n")
		.append("\t").append("        }").append("\n")
		.append("        }").append("\n")
		.append("    }").append("\n")
		.append("});").append("\n")
		.append("").append("\n")
		.append("/**").append("\n")
		.append(" * 重写Grid.Column.renderer 每列悬停时提示内容信息").append("\n")
		.append(" */").append("\n")
		.append("Ext.override(Ext.grid.Column, {").append("\n")
		.append("\t").append("renderer : function(value, metadata, record, rowIdx, colIdx, ds) {").append("\n")
		.append("\t\t").append("if (this.rendererCall) {").append("\n")
		.append("\t\t\t").append("var ret = this.rendererCall(value, metadata, record,").append("\n")
		.append("\t\t\t\t\t").append("rowIdx, colIdx, ds);").append("\n")
		.append("\t\t\t").append("if(this.showTip){").append("\n")
		.append("\t\t\t\t").append("return '<div ext:qtitle=\"' + this.header + '\" ext:qtip=\"'").append("\n")
		.append("\t\t\t\t\t\t").append("+ (ret == null ? \"\" : ret) + '\">'").append("\n")
		.append("\t\t\t\t\t\t").append("+ (ret == null ? \"\" : ret) + '</div>';").append("\n")
		.append("\t\t\t").append("}").append("\n")
		.append("\t\t\t").append("return ret;").append("\n")
		.append("\t\t").append("} else {").append("\n")
		.append("\t\t\t").append("if(this.showTip){").append("\n")
		.append("\t\t\t\t").append("return '<div ext:qtitle=\"' + this.header + '\" ext:qtip=\"'").append("\n")
		.append("\t\t\t\t\t\t").append("+ (value == null ? \"\" : value) + '\">'").append("\n")
		.append("\t\t\t\t\t\t").append("+ (value == null ? \"\" : value) + '</div>';").append("\n")
		.append("\t\t\t").append("}").append("\n")
		.append("\t\t\t").append("return value;").append("\n")
		.append("\t\t").append("}").append("\n")
		.append("\t").append("}").append("\n")
		.append("});").append("\n")
		.append("").append("\n")
		.append("// 重置文件上传框").append("\n")
		.append("Ext.resetFileInput = function(ff){").append("\n")
		.append("\t").append("if (ff != undefined) {").append("\n")
		.append("\t\t").append("if (typeof(ff) == 'string') {").append("\n")
		.append("\t\t\t").append("ff = document.getElementById(ff);").append("\n")
		.append("\t\t").append("}").append("\n")
		.append("\t").append("    var pn = ff.parentNode;").append("\n")
		.append("\t").append("    var tf = document.createElement(\"form\");").append("\n")
		.append("\t").append("    pn.replaceChild(tf,ff);").append("\n")
		.append("\t").append("    tf.appendChild(ff);").append("\n")
		.append("\t").append("    tf.reset();").append("\n")
		.append("\t").append("    pn.replaceChild(ff, tf);").append("\n")
		.append("\t").append("}").append("\n")
		.append("};").append("\n")
		.append("getTplType = function(){").append("\n")
		.append("\t").append("var tplType = Ext.getCmp('_ImportWin').tplType;").append("\n")
		.append("\t").append("window.location =\"download!downloadTpl.action?tplType=\"+tplType;").append("\n")
		.append("};").append("\n")
		.append("/**").append("\n")
		.append(" * 导入文件").append("\n")
		.append(" */").append("\n")
		.append("SF.ImportForm = Ext.extend(Ext.form.FormPanel,{").append("\n")
		.append("\t").append("constructor : function(config){").append("\n")
		.append("\t\t").append("config = Ext.apply({").append("\n")
		.append("\t\t\t").append("height:50,").append("\n")
		.append("\t\t\t").append("frame:true,").append("\n")
		.append("\t\t\t").append("labelWidth:90,").append("\n")
		.append("\t\t\t").append("labelAlign:'left',").append("\n")
		.append("\t\t\t").append("fileUpload : true,").append("\n")
		.append("\t\t\t").append("items:[{").append("\n")
		.append("\t\t\t\t").append("layout:'column',").append("\n")
		.append("\t\t\t\t").append("items:[{").append("\n")
		.append("\t\t\t\t\t").append("columnWidth:.7,").append("\n")
		.append("\t\t\t\t\t").append("layout:'form',").append("\n")
		.append("\t\t\t\t\t").append("items:{").append("\n")
		.append("\t\t\t\t\t\t").append("xtype : 'textfield',").append("\n")
		.append("\t\t\t\t\t\t").append("inputType : \"file\",").append("\n")
		.append("\t\t\t\t\t\t").append("name : \"uploadFile\",").append("\n")
		.append("\t\t\t\t\t\t").append("fieldLabel : \"选择文件\",").append("\n")
		.append("\t\t\t\t\t\t").append("anchor:'95%',").append("\n")
		.append("\t\t\t\t\t\t").append("listeners : {").append("\n")
		.append("\t\t\t\t\t\t\t").append("change : function(field,newValue,oldValue){").append("\n")
		.append("\t\t\t\t\t\t\t\t").append("var index = newValue.lastIndexOf('.');").append("\n")
		.append("\t\t\t\t\t\t\t\t").append("if(index != -1){").append("\n")
		.append("\t\t\t\t\t\t\t\t\t").append("var allow = newValue.substring(index+1);").append("\n")
		.append("\t\t\t\t\t\t\t\t\t").append("if(allow != 'xls'){").append("\n")
		.append("\t\t\t\t\t\t\t\t\t\t").append("Ext.MessageBox.alert(\"提示\", \"请选择符合下列扩展名的文件（大小写不限）：.xls\");").append("\n")
		.append("\t\t\t\t\t\t\t\t\t\t").append("field.setValue('');").append("\n")
		.append("\t\t\t\t\t\t\t\t\t").append("}").append("\n")
		.append("\t\t\t\t\t\t\t\t").append("}").append("\n")
		.append("\t\t\t\t\t\t\t").append("}").append("\n")
		.append("\t\t\t\t\t\t").append("}").append("\n")
		.append("\t\t\t\t\t").append("}").append("\n")
		.append("\t\t\t\t").append("},{").append("\n")
		.append("\t\t\t\t\t").append("columnWidth:.3,").append("\n")
		.append("\t\t\t\t\t").append("items : {").append("\n")
		.append("\t\t\t\t\t\t").append("html : \"&nbsp;&nbsp;<a style=\\\"text-decoration:none;color:#11439C;line-height:23px;font-size:12;padding-top:2px;height:25px;text-overflow:ellipsis;white-space:nowrap;overflow:hidden;\\\"  onmouseover=\\\"this.style.color=\'red\';this.style.cursor=\'hand\'\\\" onmouseout=\\\"this.style.color=\'#11439C\'\\\" href=\'#\' onClick=\\\"getTplType()\\\"><img src=\\\"../images/hflsmgmt/download.gif\\\" /><span>导入模板.xls</span></a>\"").append("\n")
		.append("\t\t\t\t\t").append("}").append("\n")
		.append("\t\t\t\t").append("},{").append("\n")
		.append("\t\t\t\t\t").append("columnWidth:1,").append("\n")
		.append("\t\t\t\t\t").append("items : {").append("\n")
		.append("\t\t\t\t\t\t").append("frame : true,").append("\n")
		.append("\t\t\t\t\t\t").append("html : \"<span>请先下载模板，根据模板填写相关信息。填写完成后请上传该文件。 （注：本系统支持OFFICE2003,为了保证系统性能，尽量上传2M以内的文件）。</span>\"").append("\n")
		.append("\t\t\t\t\t").append("}").append("\n")
		.append("\t\t\t\t").append("}]").append("\n")
		.append("\t\t\t").append("}]").append("\n")
		.append("\t\t").append("},config);").append("\n")
		.append("\t\t").append("SF.ImportForm.superclass.constructor.call(this,config);").append("\n")
		.append("\t").append("}").append("\n")
		.append("});").append("\n")
		.append("Ext.reg('SF.ImportForm',SF.ImportForm);").append("\n")
		.append("").append("\n")
		.append("/**").append("\n")
		.append(" * 导入窗口").append("\n")
		.append(" */").append("\n")
		.append("SF.ImportWindow = Ext.extend(Ext.Window,{").append("\n")
		.append("\t").append("constructor : function(config){").append("\n")
		.append("\t\t").append("this.tplType = config ? config.tplType : null;").append("\n")
		.append("\t\t").append("this.afterLoadGrid = config ? config.afterLoadGrid : null;").append("\n")
		.append("\t\t").append("config = Ext.apply({").append("\n")
		.append("\t\t\t").append("id:'_ImportWin',").append("\n")
		.append("\t\t\t").append("title:'导入',").append("\n")
		.append("\t\t\t").append("closeAction:'hide',").append("\n")
		.append("\t\t\t").append("modal:true,").append("\n")
		.append("\t\t\t").append("plain:true,").append("\n")
		.append("\t\t\t").append("resizable:false,").append("\n")
		.append("\t\t\t").append("domain:null,").append("\n")
		.append("\t\t\t").append("width:600,").append("\n")
		.append("\t\t\t").append("height:140,").append("\n")
		.append("\t\t\t").append("layout:'border',").append("\n")
		.append("\t\t\t").append("items:[{").append("\n")
		.append("\t\t\t\t").append("region:'center',").append("\n")
		.append("\t\t\t\t").append("xtype:'SF.ReportPanel'").append("\n")
		.append("\t\t\t").append("}],").append("\n")
		.append("\t\t\t").append("tbar:[{").append("\n")
		.append("\t\t\t\t").append("text:\"保存\",").append("\n")
		.append("\t\t\t\t").append("iconCls:'save',").append("\n")
		.append("\t\t\t\t").append("handler : this.importHandler.createDelegate(this)").append("\n")
		.append("\t\t\t").append("}]").append("\n")
		.append("\t\t").append("},config);").append("\n")
		.append("\t\t").append("SF.ImportWindow.superclass.constructor.call(this,config);").append("\n")
		.append("\t").append("},").append("\n")
		.append("\t").append("importHandler : function(){").append("\n")
		.append("\t\t").append("var form = this.items.get(0).getForm();").append("\n")
		.append("\t\t").append("").append("\n")
		.append("\t\t").append("var filePath = form.findField('uploadFile').getValue();").append("\n")
		.append("\t").append("    if (filePath == '') {").append("\n")
		.append("\t").append("        Ext.MessageBox.alert(\"提示\", \"请先选择需要导入的文件!\");").append("\n")
		.append("\t").append("        return;").append("\n")
		.append("\t").append("    }").append("\n")
		.append("\t").append("    var index = filePath.lastIndexOf('.');").append("\n")
		.append("\t\t").append("if(index != -1){").append("\n")
		.append("\t\t\t").append("var allow = filePath.substring(index+1);").append("\n")
		.append("\t\t\t").append("if(allow != 'xls'){").append("\n")
		.append("\t\t\t\t").append("Ext.MessageBox.alert(\"提示\", \"请选择符合下列扩展名的文件（大小写不限）：.xls\");").append("\n")
		.append("\t\t\t\t").append("return ;").append("\n")
		.append("\t\t\t").append("}").append("\n")
		.append("\t\t").append("}").append("\n")
		.append("\t\t").append("").append("\n")
		.append("\t\t").append("form.submit({").append("\n")
		.append("\t\t\t").append("waitTitle:\"提示\",").append("\n")
		.append("\t\t\t").append("waitMsg:\"正在导入,请稍后......\",").append("\n")
		.append("\t\t\t").append("success:function(form,action){").append("\n")
		.append("\t\t\t\t").append("var msg = action.result.msg;").append("\n")
		.append("\t\t\t\t").append("if(msg == null){").append("\n")
		.append("\t\t\t\t\t").append("Ext.Msg.alert(\"提示\",\"导入成功!\");").append("\n")
		.append("\t\t\t\t\t").append("if(this.afterLoadGrid){").append("\n")
		.append("\t\t\t\t\t\t").append("Ext.getCmp(this.afterLoadGrid).getStore().reload();").append("\n")
		.append("\t\t\t\t\t").append("}").append("\n")
		.append("\t\t\t\t\t").append("this.hide();").append("\n")
		.append("\t\t\t\t").append("}else{").append("\n")
		.append("\t\t\t\t\t").append("Ext.Msg.alert(\"提示\",msg);").append("\n")
		.append("\t\t\t\t").append("}").append("\n")
		.append("\t\t\t").append("}.createDelegate(this),").append("\n")
		.append("\t\t\t").append("failure:function(form,action){").append("\n")
		.append("\t\t\t\t").append("var msg = action.result.msg;").append("\n")
		.append("\t\t\t\t").append("if(msg == null){").append("\n")
		.append("\t\t\t\t\t").append("Ext.Msg.alert(\"提示\",\"导入失败!\");").append("\n")
		.append("\t\t\t\t").append("}else{").append("\n")
		.append("\t\t\t\t\t").append("Ext.Msg.alert(\"提示\",msg);").append("\n")
		.append("\t\t\t\t").append("}").append("\n")
		.append("\t\t\t").append("}").append("\n")
		.append("\t\t").append("});").append("\n")
		.append("\t").append("}").append("\n")
		.append("});").append("\n")
		.append("Ext.reg('SF.ImportWindow',SF.ImportWindow);").append("\n");
		
		
//		System.out.println(buff.toString());

		File file = new File(Common.SAVE_PATH+File.separator+"web"+File.separator+"ext-3.4.0");
		if(!file.exists()){
			file.mkdirs();
		}
		Common.write(new File(file,"common.js"), buff.toString());
		
		System.out.println("common.js 已生成.....");
	}
	
	public static void main(String[] args) {
//		createHibernate();
//		createSpring();
//		createStruts();
	}
}
