package com.sf.tools.generate;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.sf.tools.common.Common;
import com.sf.tools.common.DBUtils;


public class BuildJspAndJs {

	public static StringBuffer buff = new StringBuffer();
	private static List<String> gridHiddenColumn = new ArrayList<String>(0);
	private static DBUtils dbUtils;
	private static boolean flag = false;
	
	public static void createJSP(){
		flag = false;
		buff = new StringBuffer();
		
		buff.append("<%@ page language=\"java\" contentType=\"text/html; charset=utf-8\"%>").append("\n")
			.append("<%@taglib prefix=\"app\" uri=\"/app-tags\"%>").append("\n")
			.append("<html>").append("\n")
			.append("<head>").append("\n")
			.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">").append("\n")
			.append("<title>"+Common.DOMAIN_DESC+"</title>").append("\n")
			.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"../ext-3.4.0/resources/css/ext-all.css\" />").append("\n\n")
			.append("<script type=\"text/javascript\" src=\"../ext-3.4.0/adapter/ext/ext-base.js\"></script>").append("\n")
			.append("<script type=\"text/javascript\" src=\"../ext-3.4.0/ext-all.js\"></script>").append("\n")
			.append("<script type=\"text/javascript\" src=\"../ext-3.4.0/ext.js\"></script>").append("\n")
			.append("<script type=\"text/javascript\" src=\"../extendJS/ext-basex.js\"></script>").append("\n")
			.append("<script type=\"text/javascript\" src=\"../ext-3.4.0/source/locale/ext-lang-${locale}.js\"></script>").append("\n\n")
			.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/"+Common.MODULE_NAME+"/icon.css\" />").append("\n\n")
			.append("<script type=\"text/javascript\">").append("\n")
//			.append("\t").append("<%@include file=\"/ext-3.4.0/sf/ext.js\"%>").append("\n")
			.append("\t").append("<%@include file=\"/ext-3.4.0/common.js\"%>").append("\n")
			.append("\t").append("<%@include file=\"/scripts/"+Common.MODULE_NAME+"/"+Common.PROP_NAME+".js\"%>").append("\n")
			.append("</script>").append("\n\n")
			.append("</head>").append("\n")
			.append("<body>").append("\n")
			.append("</body>").append("\n")
			.append("</html>");
		
//		System.out.println(buff.toString());
		
		File file = new File(Common.SAVE_PATH+File.separator+"META-INF"+File.separator+"pages");
		if(!file.exists()){
			file.mkdirs();
		}
		Common.write(new File(file,Common.PROP_NAME+".jsp"), buff.toString());
		
//		System.out.println(Common.PROP_NAME+".jsp 已生成......");
	}
	
	public static void createJS(DBUtils dbUtil){
		dbUtils = dbUtil;
		System.out.println("query:"+Common.QUERY);
		System.out.println("show:"+Common.SHOW);
		System.out.println("combo:"+Common.COMBO);
		String packages = "SF."+Common.MODULE_NAME+"."+Common.PROP_NAME;
		buff = new StringBuffer();
		gridHiddenColumn = new ArrayList<String>(0);
		
		List<String> stores = new ArrayList<String>();
		buff.append("// <%@page contentType=\"text/html; charset=utf-8\" %>").append("\n\n");
		buff.append("Ext.ns('"+packages+"');").append("\n").append("\n")
			.append("EXT-STORES")
			.append("Ext.onReady(function(){").append("\n")
			.append("\t").append("new Ext.Viewport({").append("\n")
			.append("\t\t").append("layout:'border',").append("\n")
			.append("\t\t").append("items:[{").append("\n")
			.append("\t\t\t").append("region:'center',").append("\n")
			.append("\t\t\t").append("layout:'border',").append("\n")
			.append("\t\t\t").append("xtype:'"+packages+".MainPanel'").append("\n")
			.append("\t\t").append("}]").append("\n")
			.append("\t").append("});").append("\n")
			.append("});").append("\n\n");
		
		//新增、修改、查看表单
		if(Common.ADD || Common.UPDATE){
			buff.append("/**").append("\n")
			.append(" * 新增、修改、查看表单").append("\n")
			.append(" */").append("\n")
			.append(""+packages+".FormPanel = Ext.extend(Ext.form.FormPanel,{").append("\n")
			.append("\t").append("constructor : function(config){").append("\n")
			.append("\t\t").append("config = Ext.apply({").append("\n")
			.append("\t\t\t").append("frame:true,").append("\n")
			.append("\t\t\t").append("labelWidth:75,").append("\n")
			.append("\t\t\t").append("labelAlign:'right',").append("\n")
			.append("\t\t\t").append("style:'padding: 1px 2px 2px 1px',").append("\n")
			.append("\t\t\t").append("autoScroll:true,").append("\n")
			.append("\t\t\t").append("items:[{").append("\n")
			.append("\t\t\t\t").append("layout:'column',").append("\n")
			.append("\t\t\t\t").append("defaults:{").append("\n")
			.append("\t\t\t\t\t").append("defaults:{").append("\n")
			.append("\t\t\t\t\t\t").append("allowBlank:false,").append("\n")
			.append("\t\t\t\t\t\t").append("anchor:'85%'").append("\n")
			.append("\t\t\t\t\t").append("},").append("\n")
			.append("\t\t\t\t\t").append("bodyStyle:'padding: 5px 0px 0px 0px'").append("\n")
			.append("\t\t\t\t").append("},").append("\n")
			.append("\t\t\t\t").append("items:[");
			
			int formItemsCount = Common._FIELD.size()-Common.PRIMATY_KEY.size();
			String columnWidth = "1";
			if(formItemsCount>5){//500 280
				columnWidth = ".5";
			}
			
			for(int i=0;i<Common._FIELD.size();i++){
				if(Common.PRIMATY_KEY.contains(Common._FIELD.get(i))){	//主键不用再新增表单上显示
					continue;
				}
				String fieldLabel = Common._DESC.get(i);
				if("创建人".equals(fieldLabel) 
						|| "创建时间".equals(fieldLabel)
						|| "修改人".equals(fieldLabel)
						|| "修改时间".equals(fieldLabel)){
						continue;
					}
				
				String type = "textfield";
				if("Integer".equalsIgnoreCase(Common._TYPE.get(i))
						|| "Long".equalsIgnoreCase(Common._TYPE.get(i))
						|| "Double".equalsIgnoreCase(Common._TYPE.get(i))){
					type = "numberfield";
				}else if("Date".equalsIgnoreCase(Common._TYPE.get(i))){
					type = "datefield";
				}
				buff.append("{").append("\n")
				.append("\t\t\t\t\t").append("columnWidth:"+columnWidth+",").append("\n")
				.append("\t\t\t\t\t").append("layout:'form',").append("\n")
				.append("\t\t\t\t\t").append("items:{").append("\n");
				
				
				if(Common.COMBO.contains(Common._FIELD.get(i))){
					flag = true;
					String desc = Common._DESC.get(i).replace("（", "(").replace("）", ")").replace("，", ",").trim();
					if(desc.indexOf("(") == -1){
						continue;
					}
					buff.append("\t\t\t\t\t\t").append("xtype:'combo',").append("\n")
					.append("\t\t\t\t\t\t").append("fieldLabel:\""+desc.substring(0, desc.indexOf("("))+"\",").append("\n")
					.append("\t\t\t\t\t\t").append("triggerAction:\"all\",").append("\n")
					.append("\t\t\t\t\t\t").append("lazyRender : true,").append("\n")
					.append("\t\t\t\t\t\t").append("editable: false,").append("\n")
					.append("\t\t\t\t\t\t").append("mode:\"local\",").append("\n")
					.append("\t\t\t\t\t\t").append("displayField:\"name\",").append("\n")
					.append("\t\t\t\t\t\t").append("valueField:\"id\",").append("\n")
					.append("\t\t\t\t\t\t").append("hiddenName:'"+Common.PROP_NAME+"."+Common._FIELD.get(i)+"',").append("\n")
					.append("\t\t\t\t\t\t").append("store : "+Common._FIELD.get(i)+"Store,").append("\n")
					.append("\t\t\t\t\t\t").append("emptyText:'请选择',").append("\n")
					.append("\t\t\t\t\t\t").append("listeners : {").append("\n")
					.append("\t\t\t\t\t\t\t").append("expand : this.expandHandler.createDelegate(this)").append("\n")
					.append("\t\t\t\t\t\t").append("}").append("\n");
					
					StringBuffer sb = new StringBuffer();
					
					desc = desc.substring(desc.indexOf("(")+1, desc.indexOf(")"));
					String[] items = desc.split(",");
					StringBuffer data = new StringBuffer();
					for(int k=0;k<items.length;k++){
						data.append("[");
						if("textfield".equals(type)){
							data.append("'").append(items[k].split("=")[0]).append("'").append(",'").append(items[k].split("=")[1]).append("'");
						}else{
							data.append(items[k].split("=")[0]).append(",'").append(items[k].split("=")[1]).append("'");
						}
						data.append("]");
						if(k != items.length -1){
							data.append(",");
						}
					}
					sb.append("var "+Common._FIELD.get(i)+"Store = new Ext.data.ArrayStore({").append("\n")
					.append("\t").append("fields:['id','name'],").append("\n")
					.append("\t").append("data:["+data+"]").append("\n")
					.append("});").append("\n");
					stores.add(sb.toString());
				}else if(Common.MAPPING.containsKey(Common.TABLE_NAME+":"+Common._COLUMN.get(i))){
					flag = true;
					
					String mappingTableName = Common.MAPPING.get(Common.TABLE_NAME+":"+Common._COLUMN.get(i)).split(":")[0];
					String mappingColumn = Common.MAPPING.get(Common.TABLE_NAME+":"+Common._COLUMN.get(i)).split(":")[1];
					String mappingColumnName = Common.MAPPING.get(Common.TABLE_NAME+":"+Common._COLUMN.get(i)).split(":")[2];
					String mappingEntityName = Common.getEntityName(mappingTableName);
					String mappingPropName = (mappingEntityName.charAt(0)+"").toLowerCase()+mappingEntityName.substring(1);
					
					mappingColumnName = Common.getFieldName(mappingColumnName);
					
					buff.append("\t\t\t\t\t\t").append("xtype:'combo',").append("\n")
					.append("\t\t\t\t\t\t").append("fieldLabel:\""+Common._DESC.get(i)+"\",").append("\n")
					.append("\t\t\t\t\t\t").append("triggerAction:\"all\",").append("\n")
					.append("\t\t\t\t\t\t").append("lazyRender : true,").append("\n")
					.append("\t\t\t\t\t\t").append("editable: false,").append("\n")
					.append("\t\t\t\t\t\t").append("mode:\"local\",").append("\n")
					.append("\t\t\t\t\t\t").append("displayField:\"name\",").append("\n");
					
					List<String> keys = Common.primaryKeys.get(mappingTableName);
					if(keys == null){
						keys = dbUtils.getPrimaryKey(mappingTableName);
						Common.primaryKeys.put(mappingTableName, keys);
					}
					if(keys.indexOf(mappingColumn) != -1){
						buff.append("\t\t\t\t\t\t").append("valueField:\"id\",").append("\n")
						.append("\t\t\t\t\t\t").append("hiddenName:'"+Common.PROP_NAME+"."+mappingPropName+".id',").append("\n");
					}else{
						buff.append("\t\t\t\t\t\t").append("valueField:\"name\",").append("\n")
						.append("\t\t\t\t\t\t").append("hiddenName:'"+Common.PROP_NAME+"."+mappingPropName+"."+mappingColumnName+"',").append("\n");
					}
					
					buff.append("\t\t\t\t\t\t").append("store : "+mappingPropName+"Store,").append("\n")
					.append("\t\t\t\t\t\t").append("emptyText:'请选择',").append("\n")
					.append("\t\t\t\t\t\t").append("listeners : {").append("\n")
					.append("\t\t\t\t\t\t\t").append("expand : this.expandHandler.createDelegate(this)").append("\n")
					.append("\t\t\t\t\t\t").append("}").append("\n");
					
					StringBuffer sb = new StringBuffer();
					sb.append("var "+mappingPropName+"Store = new Ext.data.JsonStore({").append("\n")
					.append("\t").append("autoLoad:true,").append("\n")
					.append("\t").append("url:'"+mappingPropName+"_findAll.action',").append("\n")
					.append("\t").append("root:'data',").append("\n");
					
					if(keys.indexOf(mappingColumn) != -1){
						if("name".equals(mappingColumnName)){
							sb.append("\t").append("fields:['id','name']").append("\n");
						}else{
							sb.append("\t").append("fields:['id',{name:'name',mapping:'"+mappingColumnName+"'}]").append("\n");
						}
					}else{
						if("name".equals(mappingColumnName)){
							sb.append("\t").append("fields:['name']").append("\n");
						}else{
							sb.append("\t").append("fields:[{name:'name',mapping:'"+mappingColumnName+"'}]").append("\n");
						}
					}
					
					sb.append("});").append("\n");
					stores.add(sb.toString());
				}else{
					buff.append("\t\t\t\t\t\t").append("xtype:'"+type+"',").append("\n")
					.append("\t\t\t\t\t\t").append("name:'"+Common.PROP_NAME+"."+Common._FIELD.get(i)+"',").append("\n");
					if("datefield".equals(type)){
						buff.append("\t\t\t\t\t\t").append("format:'Y-m-d',").append("\n");
					}else if("numberfield".equals(type)){
//					String scale = "0";
//					if(Common._LENGTH.get(i).indexOf("scale") != -1){
//						scale = Common._LENGTH.get(i).substring(Common._LENGTH.get(i).indexOf("scale")+6).replace("\"", "");
//						buff.append("\t\t\t\t\t\t").append("decimalPrecision:"+scale+",").append("\n");
//					}
						//precision="15" scale="3"
						buff.append("\t\t\t\t\t\t").append("allowNegative:false,").append("\n");
						buff.append("\t\t\t\t\t\t").append("decimalPrecision:2,").append("\n");
					}
					buff.append("\t\t\t\t\t\t").append("fieldLabel:\""+fieldLabel+"\"").append("\n");
				}
				
				
				buff.append("\t\t\t\t\t").append("}").append("\n")
				.append("\t\t\t\t").append("},");
			}
			buff.append("{").append("\n")
			.append("\t\t\t\t\t").append("xtype:'hidden',").append("\n")
			.append("\t\t\t\t\t").append("name:'"+Common.PROP_NAME+".id'").append("\n")
			.append("\t\t\t\t").append("}]").append("\n")
			.append("\t\t\t").append("}]").append("\n")
			.append("\t\t").append("},config);").append("\n")
			.append("\t\t").append(""+packages+".FormPanel.superclass.constructor.call(this,config);").append("\n")
			.append("\t").append("}");
			
			if(flag){
				buff.append(",").append("\n")
				.append("\t").append("expandHandler : function(combo){").append("\n")
				.append("\t\t").append("var store = combo.getStore();").append("\n")
				.append("\t\t").append("if(store.getCount()>0 && store.getAt(0).get('name') == \"全部\"){").append("\n")
				.append("\t\t\t").append("store.removeAt(0);").append("\n")
				.append("\t\t").append("}").append("\n")
				.append("\t").append("}").append("\n");
			}
			
			buff.append("\n").append("});").append("\n")
			.append("Ext.reg('"+packages+".FormPanel',"+packages+".FormPanel);").append("\n")
			.append("").append("\n")
			.append("/**").append("\n");
			
			int winWidth = 400;
			int winHeight = 250;
			if(formItemsCount>5){//500 280
				winWidth = 500;
				winHeight = (formItemsCount+1)/2 * 50;
				if(winHeight>600){
					winHeight = 600;
				}
			}
			//新增、修改、查看窗口
			buff.append(" * 新增、修改、查看窗口").append("\n")
			.append(" */").append("\n")
			.append(""+packages+".Win = Ext.extend(Ext.Window,{").append("\n")
			.append("\t").append("constructor : function(config){").append("\n")
			.append("\t\t").append("config = Ext.apply({").append("\n")
			.append("\t\t\t").append("closeAction:'hide',").append("\n")
			.append("\t\t\t").append("modal:true,").append("\n")
			.append("\t\t\t").append("plain:true,").append("\n")
			.append("\t\t\t").append("resizable:false,").append("\n")
			.append("\t\t\t").append("domain:null,").append("\n")
			.append("\t\t\t").append("width:"+winWidth+",").append("\n")
			.append("\t\t\t").append("height:"+winHeight+",").append("\n")
			.append("\t\t\t").append("layout:'border',").append("\n")
			.append("\t\t\t").append("items:[{").append("\n")
			.append("\t\t\t\t").append("region:'center',").append("\n")
			.append("\t\t\t\t").append("xtype:'"+packages+".FormPanel'").append("\n")
			.append("\t\t\t").append("}],").append("\n")
			.append("\t\t\t").append("tbar:[{").append("\n")
			.append("\t\t\t\t").append("text:\"保存\",").append("\n")
			.append("\t\t\t\t").append("iconCls:'save',").append("\n")
			.append("\t\t\t\t").append("handler : this.save.createDelegate(this)").append("\n")
			.append("\t\t\t").append("},{").append("\n")
			.append("\t\t\t\t").append("text:\"重置\",").append("\n")
			.append("\t\t\t\t").append("iconCls:'reset',").append("\n")
			.append("\t\t\t\t").append("handler : this.reset.createDelegate(this)").append("\n")
			.append("\t\t\t").append("}]").append("\n")
			.append("\t\t").append("},config);").append("\n")
			.append("\t\t").append(""+packages+".Win.superclass.constructor.call(this,config);").append("\n")
			.append("\t").append("},").append("\n")
			.append("\t").append("reset : function(){").append("\n")
			.append("\t\t").append("var form = this.items.get(0).getForm();").append("\n")
			.append("\t\t").append("//重置表单").append("\n")
			.append("\t\t").append("form.reset();").append("\n")
			.append("\t\t").append("//修改时回到初始值").append("\n")
			.append("\t\t").append("if(this.domain != null){").append("\n")
			.append("\t\t\t").append("form.setValues(this.domain);").append("\n")
			.append("\t\t").append("}else{").append("\n")
			.append("\t\t\t").append("form.clearInvalid();").append("\n")
			.append("\t\t").append("}").append("\n")
			.append("\t").append("},").append("\n")
			.append("\t").append("distory : function(){").append("\n")
			.append("\t\t").append("this.reset();").append("\n")
			.append("\t\t").append("this.hide();").append("\n")
			.append("\t").append("},").append("\n")
			.append("\t").append("save : function(){").append("\n")
			.append("\t\t").append("var form = this.items.get(0).getForm();").append("\n")
			.append("\t\t").append("").append("\n")
			.append("\t\t").append("if(!form.isValid()){").append("\n")
			.append("\t\t\t").append("Ext.Msg.alert(\"提示\", \"数据校验错误!\");").append("\n")
			.append("\t\t\t").append("return ;").append("\n")
			.append("\t\t").append("}").append("\n")
			.append("\t\t").append("form.submit({").append("\n")
			.append("\t\t\t").append("waitTitle:\"提示\",").append("\n")
			.append("\t\t\t").append("waitMsg:\"正在保存,请稍后......\",").append("\n")
			.append("\t\t\t").append("success:function(form,action){").append("\n")
			.append("\t\t\t\t").append("var msg = action.result.msg;").append("\n")
			.append("\t\t\t\t").append("if(msg == null){").append("\n")
			.append("\t\t\t\t\t").append("Ext.Msg.alert(\"提示\",\"保存成功!\");").append("\n")
			.append("\t\t\t\t\t").append("Ext.getCmp('"+Common.PROP_NAME+"Grid').getStore().reload();").append("\n")
			.append("\t\t\t\t\t").append("this.distory();").append("\n")
			.append("\t\t\t\t").append("}else{").append("\n")
			.append("\t\t\t\t\t").append("Ext.Msg.alert(\"提示\",msg);").append("\n")
			.append("\t\t\t\t").append("}").append("\n")
			.append("\t\t\t").append("}.createDelegate(this),").append("\n")
			.append("\t\t\t").append("failure:function(form,action){").append("\n")
			.append("\t\t\t\t").append("var msg = action.result.msg;").append("\n")
			.append("\t\t\t\t").append("Ext.Msg.alert(\"提示\",msg);").append("\n")
			.append("\t\t\t").append("}").append("\n")
			.append("\t\t").append("});").append("\n")
			.append("\t").append("}").append("\n")
			.append("});").append("\n")
			.append("Ext.reg('"+packages+".Win',"+packages+".Win);").append("\n")
			.append("").append("\n")
			.append("").append("\n")
			.append("/**").append("\n");
		}
		
		//主界面
		buff.append("* 主界面").append("\n")
			.append("*/").append("\n")
			.append(""+packages+".MainPanel = Ext.extend(Ext.Panel,{").append("\n");
		if(Common.ADD || Common.UPDATE){
			buff.append("\t").append("infoWin : new "+packages+".Win(),").append("\n");
		}
		if(Common.IMPORT){
			buff.append("\t").append("importWin : new SF.ImportWindow(),").append("\n");
		}
		
		buff.append("\t").append("constructor : function(config){").append("\n")
			.append("\t\t").append("config = Ext.apply({").append("\n")
			.append("\t\t\t").append("id:'mainPanel',").append("\n")
			.append("\t\t\t").append("frame:true,").append("\n")
			.append("\t\t\t").append("tbar : new Ext.Toolbar({").append("\n")
			.append("\t\t\t\t").append("items:[{").append("\n")
			.append("\t\t\t\t\t").append("text:\"查询\",").append("\n")
			.append("\t\t\t\t\t").append("iconCls:'search',").append("\n")
			.append("\t\t\t\t\t").append("handler : this.searchHandler.createDelegate(this)").append("\n")
			.append("\t\t\t\t").append("}");
		if(Common.ADD){
			buff.append(",{").append("\n")
				.append("\t\t\t\t\t").append("text:\"新增\",").append("\n")
				.append("\t\t\t\t\t").append("iconCls:'add',").append("\n")
				.append("\t\t\t\t\t").append("handler : this.addHandler.createDelegate(this)").append("\n")
				.append("\t\t\t\t").append("}");
		}
		if(Common.UPDATE){
			buff.append(",{").append("\n")
				.append("\t\t\t\t\t").append("text:\"修改\",").append("\n")
				.append("\t\t\t\t\t").append("iconCls:'edit',").append("\n")
				.append("\t\t\t\t\t").append("handler : this.editHandler.createDelegate(this)").append("\n")
				.append("\t\t\t\t").append("}");
		}
		if(Common.DELETE){
			buff.append(",{").append("\n")
				.append("\t\t\t\t\t").append("text:\"删除\",").append("\n")
				.append("\t\t\t\t\t").append("iconCls:'delete',").append("\n")
				.append("\t\t\t\t\t").append("handler : this.deleteHandler.createDelegate(this)").append("\n")
				.append("\t\t\t\t").append("}");
		}
		if(Common.IMPORT){
			buff.append(",{").append("\n")
				.append("\t\t\t\t\t").append("text:\"导入\",").append("\n")
				.append("\t\t\t\t\t").append("iconCls:'import',").append("\n")
				.append("\t\t\t\t\t").append("handler : this.importHandler.createDelegate(this)").append("\n")
				.append("\t\t\t\t").append("}");
		}
		if(Common.EXPORT){
			buff.append(",{").append("\n")
				.append("\t\t\t\t\t").append("text:\"导出\",").append("\n")
				.append("\t\t\t\t\t").append("iconCls:'export',").append("\n")
				.append("\t\t\t\t\t").append("handler : this.exportHandler.createDelegate(this)").append("\n")
				.append("\t\t\t\t").append("}");
		}
//		if(Common.SEARCH){
			buff.append(",{").append("\n")
				.append("\t\t\t\t\t").append("text:\"重置\",").append("\n")
				.append("\t\t\t\t\t").append("iconCls:'reset',").append("\n")
				.append("\t\t\t\t\t").append("handler : this.resetHandler.createDelegate(this)").append("\n")
				.append("\t\t\t\t").append("}");
//		}
		buff.append("]").append("\n")
			.append("\t\t\t").append("}),").append("\n")
			.append("\t\t\t").append("items:[{").append("\n")
			.append("\t\t\t\t").append("region:'north',").append("\n")
//			.append("\t\t\t\t").append("margins:'0 0 5 0',").append("\n")
			.append("\t\t\t\t").append("xtype:'"+packages+".QueryPanel'").append("\n")
			.append("\t\t\t").append("},{").append("\n")
			.append("\t\t\t\t").append("region:'center',").append("\n")
			.append("\t\t\t\t").append("xtype:'"+packages+".GridPanel'").append("\n")
			.append("\t\t\t").append("}]").append("\n")
			.append("\t\t").append("},config);").append("\n")
			.append("\t\t").append(""+packages+".MainPanel.superclass.constructor.call(this,config);").append("\n")
			.append("\t").append("},").append("\n")
			.append("\t").append("searchHandler : function(){").append("\n")
			.append("\t\t").append("var store = this.items.get(1).getStore();").append("\n")
			.append("\t\t").append("var form = this.items.get(0).getForm();").append("\n")
			.append("\t\t").append("if(!form.isValid()){").append("\n")
			.append("\t\t\t").append("Ext.Msg.alert(\"提示\",\"数据校验错误!\");").append("\n")
			.append("\t\t\t").append("return ;").append("\n")
			.append("\t\t").append("}").append("\n")
			.append("\t\t").append("store.load({").append("\n")
			.append("\t\t\t").append("callback : function(r,option,success){").append("\n")
			.append("\t\t\t\t").append("if(!success){").append("\n")
			.append("\t\t\t\t\t").append("store.removeAll();").append("\n")
			.append("\t\t\t\t\t").append("if(this.reader && this.reader.jsonData){").append("\n")
			.append("\t\t\t\t\t\t").append("Ext.Msg.alert(\"提示\", this.reader.jsonData.message);").append("\n")
			.append("\t\t\t\t\t").append("}").append("\n")
			.append("\t\t\t\t").append("}").append("\n")
			.append("\t\t\t").append("}").append("\n")
			.append("\t\t").append("});").append("\n")
			.append("\t").append("}");
		if(Common.ADD){
			buff.append(",").append("\n")
				.append("\t").append("addHandler : function(){").append("\n")
				.append("\t\t").append("var form = this.infoWin.items.get(0).getForm();").append("\n")
				.append("\t\t").append("form.url='"+Common.PROP_NAME+"_save.action',").append("\n")
				.append("\t\t").append("this.infoWin.setTitle(\"新增\");").append("\n")
				.append("\t\t").append("this.infoWin.domain = null;").append("\n")
				.append("\t\t").append("this.infoWin.show();").append("\n")
				.append("\t\t").append("this.infoWin.reset();").append("\n")
				.append("\t").append("}");
		}
		
		String dateFields = "";
		for(int i=0 ;i<Common.SHOW.size();i++){
			if("Date".equalsIgnoreCase(Common.getQueryType(Common.SHOW.get(i)))){
				dateFields += "i=='" + Common.SHOW.get(i)+"'||";
			}
		}
		if(!"".equals(dateFields)){
			dateFields = dateFields.substring(0, dateFields.length()-2);
			dateFields = "("+dateFields+") && data[i] != null";
		}

		if(Common.UPDATE){
			buff.append(",").append("\n")
				.append("\t").append("editHandler : function(){").append("\n")
				.append("\t\t").append("var cm = this.items.get(1).getSelectionModel();").append("\n")
				.append("\t\t").append("var count = cm.getCount();").append("\n")
				.append("\t\t").append("if(count < 1){").append("\n")
				.append("\t\t\t").append("Ext.Msg.alert(\"提示\",\"请选择要修改的记录!\");").append("\n")
				.append("\t\t\t").append("return ;").append("\n")
				.append("\t\t").append("}else if(count > 1){").append("\n")
				.append("\t\t\t").append("Ext.Msg.alert(\"提示\",\"一次只能修改一条记录!\");").append("\n")
				.append("\t\t\t").append("return ;").append("\n")
				.append("\t\t").append("}else {").append("\n")
				.append("\t\t\t").append("var data = cm.getSelected().data;").append("\n")
				.append("\t\t\t").append("var obj = {};").append("\n")
				.append("\t\t\t").append("for(var i in data){").append("\n");
			if(!"".equals(dateFields)){
				buff.append("\t\t\t\t").append("if("+dateFields+"){").append("\n")
					.append("\t\t\t\t\t").append("data[i] = data[i].split(\"T\")[0];").append("\n")
					.append("\t\t\t\t").append("}").append("\n");
			}
			buff.append("\t\t\t\t").append("obj['"+Common.PROP_NAME+".'+i] = data[i];").append("\n")
				.append("\t\t\t").append("}").append("\n")
				.append("\t\t\t").append("this.infoWin.setTitle(\"修改\");").append("\n")
				.append("\t\t\t").append("this.infoWin.domain = obj;").append("\n")
				.append("\t\t\t").append("var form = this.infoWin.items.get(0).getForm();").append("\n")
				.append("\t\t\t").append("form.url = '"+Common.PROP_NAME+"_update.action';").append("\n")
				.append("\t\t\t").append("this.infoWin.show();").append("\n")
				.append("\t\t\t").append("this.infoWin.reset();").append("\n")
				.append("\t\t").append("}").append("\n")
				.append("\t").append("}");
		}
		
		if(Common.DELETE){
			buff.append(",").append("\n")
				.append("\t").append("deleteHandler : function(){").append("\n")
				.append("\t\t").append("var cm = this.items.get(1).getSelectionModel();").append("\n")
				.append("\t\t").append("var count = cm.getCount();").append("\n")
				.append("\t\t").append("if(count < 1){").append("\n")
				.append("\t\t\t").append("Ext.Msg.alert('提示','请选择要删除的记录!');").append("\n")
				.append("\t\t\t").append("return ;").append("\n")
				.append("\t\t").append("}").append("\n")
				.append("\t\t").append("var records = cm.getSelections();").append("\n")
				.append("\t\t").append("var ids = [];").append("\n")
				.append("\t\t").append("for(var i=0;i<records.length;i++){").append("\n")
				.append("\t\t\t").append("ids.push(records[i].data.id);").append("\n")
				.append("\t\t").append("}").append("\n")
				.append("\t\t").append("var loadMask = new Ext.LoadMask(this.body,{removeMask:true,msg:'正在删除,请稍后......'});").append("\n")
				.append("\t\t").append("Ext.Msg.confirm('提示','确定要删除吗?',function(btn){").append("\n")
				.append("\t\t\t").append("if(btn=='yes'){").append("\n")
				.append("\t\t\t\t").append("loadMask.show();").append("\n")
				.append("\t\t\t\t").append("Ext.Ajax.request({").append("\n")
				.append("\t\t\t\t\t").append("url:'"+Common.PROP_NAME+"_delete.action',").append("\n")
				.append("\t\t\t\t\t").append("params : {").append("\n")
				.append("\t\t\t\t\t\t").append("ids : ids").append("\n")
				.append("\t\t\t\t\t").append("},").append("\n")
				.append("\t\t\t\t\t").append("success:function(response,opts){").append("\n")
				.append("\t\t\t\t\t\t").append("loadMask.hide();").append("\n")
				.append("\t\t\t\t\t\t").append("var msg = Ext.decode(response.responseText).msg;").append("\n")
				.append("\t\t\t\t\t\t").append("if(msg){").append("\n")
				.append("\t\t\t\t\t\t\t").append("Ext.Msg.alert('提示',msg);").append("\n")
				.append("\t\t\t\t\t\t").append("}else{").append("\n")
				.append("\t\t\t\t\t\t\t").append("Ext.Msg.alert('提示','删除成功!');").append("\n")
				.append("\t\t\t\t\t\t\t").append("Ext.getCmp('"+Common.PROP_NAME+"Grid').getStore().reload();").append("\n")
				.append("\t\t\t\t\t\t").append("}").append("\n")
				.append("\t\t\t\t\t").append("},").append("\n")
				.append("\t\t\t\t\t").append("failure:function(response,opts){").append("\n")
				.append("\t\t\t\t\t\t").append("loadMask.hide();").append("\n")
				.append("\t\t\t\t\t\t").append("var msg = Ext.decode(response.responseText).msg;").append("\n")
				.append("\t\t\t\t\t\t").append("Ext.Msg.alert(\"提示\",msg);").append("\n")
				.append("\t\t\t\t\t").append("}").append("\n")
				.append("\t\t\t\t").append("});").append("\n")
				.append("\t\t\t").append("}").append("\n")
				.append("\t\t").append("},this);").append("\n")
				.append("\t").append("}");
		}
		
		if(Common.IMPORT){
			buff.append(",").append("\n")
				.append("\t").append("importHandler : function(){").append("\n")
				.append("\t\t").append("var form = this.importWin.items.get(0).getForm();").append("\n\n")
				.append("\t\t").append("form.url='"+Common.PROP_NAME+"_add_import.action',").append("\n")
				.append("\t\t").append("form.reset();").append("\n\n")
				.append("\t\t").append("this.importWin = Ext.apply(this.importWin,{tplType:'"+Common.DOMAIN_NAME+"',afterLoadGrid:'"+Common.PROP_NAME+"Grid'});").append("\n")
				.append("\t\t").append("this.importWin.setTitle('导入');").append("\n")
				.append("\t\t").append("this.importWin.show();").append("\n")
				.append("\t").append("}");
		}
		
		if(Common.EXPORT){
			buff.append(",").append("\n")
				.append("\t").append("exportHandler : function(){").append("\n")
				.append("\t\t").append("var form = this.items.get(0).getForm();").append("\n")
				.append("\t\t").append("if(form.isValid()){").append("\n")
				.append("\t\t\t").append("form.submit({").append("\n")
				.append("\t\t\t\t").append("waitTitle:\"提示\",").append("\n")
				.append("\t\t\t\t").append("waitMsg:\"正在导出,请稍后......\",").append("\n")
				.append("\t\t\t\t").append("url:\""+Common.PROP_NAME+"_export.action\",").append("\n")
				.append("\t\t\t\t").append("timeout:900,").append("\n")
				.append("\t\t\t\t").append("success:function(form,action){").append("\n")
				.append("\t\t\t\t\t").append("if(action.result.msg){").append("\n")
				.append("\t\t\t\t\t\t").append("Ext.Msg.alert(\"提示\", action.result.msg);").append("\n")
				.append("\t\t\t\t\t").append("}else {").append("\n")
				.append("\t\t\t\t\t\t").append("var url=encodeURI(encodeURI(action.result.fileName));").append("\n")
				.append("\t\t\t\t\t\t").append("window.location = 'download!downloadFile.action?fileName=' + url;").append("\n")
				.append("\t\t\t\t\t").append("}").append("\n")
				.append("\t\t\t\t").append("}.createDelegate(this),").append("\n")
				.append("\t\t\t\t").append("failure:function(form,action){").append("\n")
				.append("\t\t\t\t\t").append("Ext.Msg.alert(\"提示\",\"导出失败!\");").append("\n")
				.append("\t\t\t\t").append("}").append("\n")
				.append("\t\t\t").append("});").append("\n")
				.append("\t\t").append("}").append("\n")
				.append("\t").append("}");
		}
		
//		if(Common.SEARCH){
			buff.append(",").append("\n")
				.append("\t").append("resetHandler : function(){").append("\n")
				.append("\t\t").append("this.items.get(0).getForm().reset();").append("\n")
				.append("\t").append("}");
//		}
		
		buff.append("\n")
			.append("});").append("\n")
			.append("Ext.reg('"+packages+".MainPanel',"+packages+".MainPanel);").append("\n")
			.append("").append("\n")
			.append("/**").append("\n");
		
		//查询表单
		buff.append(" * 查询表单").append("\n")
			.append(" */").append("\n")
			.append(""+packages+".QueryPanel = Ext.extend(Ext.form.FormPanel,{").append("\n")
			.append("\t").append("constructor : function(config){").append("\n")
			.append("\t\t").append("config = Ext.apply({").append("\n")
			.append("\t\t\t").append("id:'"+Common.PROP_NAME+"QueryPanel',").append("\n")
			.append("\t\t\t").append("autoHeight:true,").append("\n")
			.append("\t\t\t").append("frame:true,").append("\n")
			.append("\t\t\t").append("labelWidth:60,").append("\n")
			.append("\t\t\t").append("labelAlign:'right',").append("\n")
			.append("\t\t\t").append("bodyStyle:'padding: 1px 5px 0px 1px',").append("\n")
			.append("\t\t\t").append("margins:'0 0 5 0',").append("\n")
			.append("\t\t\t").append("items:[{").append("\n")
			.append("\t\t\t\t").append("xtype:'fieldset',").append("\n")
			.append("\t\t\t\t").append("title:\"查询条件\",").append("\n")
			.append("\t\t\t\t").append("layout:'column',").append("\n")
			.append("\t\t\t\t").append("defaults:{").append("\n")
			.append("\t\t\t\t\t").append("defaults:{").append("\n")
			.append("\t\t\t\t\t\t").append("anchor:'80%'").append("\n")
			.append("\t\t\t\t\t").append("}").append("\n")
			.append("\t\t\t\t").append("},").append("\n")
			.append("\t\t\t\t").append("items:[");
		
		for(int i=0;i<Common.QUERY.size();i++){
			
			String type = "textfield";
			if("Integer".equalsIgnoreCase(Common.getQueryType(Common.QUERY.get(i)))
				|| "Long".equalsIgnoreCase(Common.getQueryType(Common.QUERY.get(i)))
				|| "Double".equalsIgnoreCase(Common.getQueryType(Common.QUERY.get(i)))){
				type = "numberfield";
			}else if("Date".equalsIgnoreCase(Common.getQueryType(Common.QUERY.get(i)))){
				type = "datefield";
			}
			buff.append("{").append("\n")
				.append("\t\t\t\t\t").append("columnWidth:.25,").append("\n")
				.append("\t\t\t\t\t").append("layout:'form',").append("\n")
				.append("\t\t\t\t\t").append("items:{").append("\n");
			
			if(Common.COMBO.contains(Common.QUERY.get(i))){
				flag = true;
				String desc = Common.getQueryDesc(Common.QUERY.get(i)).replace("（", "(").replace("）", ")").replace("，", ",").trim();
				if(desc.indexOf("(") == -1){
					continue;
				}
				buff.append("\t\t\t\t\t\t").append("xtype:'combo',").append("\n")
					.append("\t\t\t\t\t\t").append("fieldLabel:\""+desc.substring(0, desc.indexOf("("))+"\",").append("\n")
					.append("\t\t\t\t\t\t").append("triggerAction:\"all\",").append("\n")
					.append("\t\t\t\t\t\t").append("lazyRender : true,").append("\n")
					.append("\t\t\t\t\t\t").append("editable: false,").append("\n")
					.append("\t\t\t\t\t\t").append("mode:\"local\",").append("\n")
					.append("\t\t\t\t\t\t").append("displayField:\"name\",").append("\n")
					.append("\t\t\t\t\t\t").append("valueField:\"id\",").append("\n")
					.append("\t\t\t\t\t\t").append("hiddenName:'query."+Common.QUERY.get(i)+"',").append("\n")
					.append("\t\t\t\t\t\t").append("store : "+Common.QUERY.get(i)+"Store,").append("\n")
					.append("\t\t\t\t\t\t").append("emptyText:'请选择',").append("\n")
					.append("\t\t\t\t\t\t").append("listeners : {").append("\n")
					.append("\t\t\t\t\t\t\t").append("expand : this.expandHandler.createDelegate(this)").append("\n")
					.append("\t\t\t\t\t\t").append("}").append("\n");
				
				desc = desc.substring(desc.indexOf("(")+1, desc.indexOf(")"));
				String[] items = desc.split(",");
				StringBuffer data = new StringBuffer();
				for(int k=0;k<items.length;k++){
					data.append("[")
						.append(items[k].split("=")[0]).append(",'").append(items[k].split("=")[1]).append("'")
						.append("]");
					if(k != items.length -1){
						data.append(",");
					}
				}
			}else if(Common.MAPPING.containsKey(Common.TABLE_NAME+":"+Common._COLUMN.get(Common._FIELD.indexOf(Common.QUERY.get(i))))){
				flag = true;
				int index = Common._FIELD.indexOf(Common.QUERY.get(i));
				String field = Common._FIELD.get(index);
				
				String mappInfo = Common.MAPPING.get(Common.TABLE_NAME+":"+Common._COLUMN.get(index));
				String mappingTableName = mappInfo.split(":")[0];
				String mappingColumn = mappInfo.split(":")[1];
				String mappingColumnName = mappInfo.split(":")[2];
				String mappingEntityName = Common.getEntityName(mappingTableName);
				String mappingPropName = (mappingEntityName.charAt(0)+"").toLowerCase()+mappingEntityName.substring(1);
				
				mappingColumnName = Common.getFieldName(mappingColumnName);
				
				buff.append("\t\t\t\t\t\t").append("xtype:'combo',").append("\n")
					.append("\t\t\t\t\t\t").append("fieldLabel:\""+Common._DESC.get(Common._FIELD.indexOf(Common.QUERY.get(i)))+"\",").append("\n")
					.append("\t\t\t\t\t\t").append("triggerAction:\"all\",").append("\n")
					.append("\t\t\t\t\t\t").append("lazyRender : true,").append("\n")
					.append("\t\t\t\t\t\t").append("editable: false,").append("\n")
					.append("\t\t\t\t\t\t").append("mode:\"local\",").append("\n")
					.append("\t\t\t\t\t\t").append("displayField:\"name\",").append("\n");
				
				List<String> keys = Common.primaryKeys.get(mappingTableName);
				if(keys == null){
					keys = dbUtils.getPrimaryKey(mappingTableName);
					Common.primaryKeys.put(mappingTableName, keys);
				}
				if(keys.indexOf(mappingColumn) != -1){
					buff.append("\t\t\t\t\t\t").append("valueField:\"id\",").append("\n")
						.append("\t\t\t\t\t\t").append("hiddenName:'query."+field+"',").append("\n");
				}else{
					buff.append("\t\t\t\t\t\t").append("valueField:\"name\",").append("\n")
						.append("\t\t\t\t\t\t").append("hiddenName:'query."+field+"',").append("\n");
				}
//				buff.append("\t\t\t\t\t\t").append("valueField:\"id\",").append("\n")
//					.append("\t\t\t\t\t\t").append("hiddenName:'"+Common.PROP_NAME+"."+mappingPropName+".id',").append("\n");
					
				buff.append("\t\t\t\t\t\t").append("store : "+mappingPropName+"Store,").append("\n")
					.append("\t\t\t\t\t\t").append("emptyText:'请选择',").append("\n")
					.append("\t\t\t\t\t\t").append("listeners : {").append("\n")
					.append("\t\t\t\t\t\t\t").append("expand : this.expandHandler.createDelegate(this)").append("\n")
					.append("\t\t\t\t\t\t").append("}").append("\n");
				  
			}else{
				buff.append("\t\t\t\t\t\t").append("xtype:'"+type+"',").append("\n")
				.append("\t\t\t\t\t\t").append("name:'query."+Common.QUERY.get(i)+"',").append("\n");
				if("datefield".equals(type)){
					buff.append("\t\t\t\t\t\t").append("format:'Y-m-d',").append("\n");
				}else if("numberfield".equals(type)){
					buff.append("\t\t\t\t\t\t").append("allowNegative:false,").append("\n");
					buff.append("\t\t\t\t\t\t").append("decimalPrecision:2,").append("\n");
				}
				buff.append("\t\t\t\t\t\t").append("fieldLabel:\""+Common.getQueryDesc(Common.QUERY.get(i))+"\"").append("\n");
			}
			
			buff.append("\t\t\t\t\t").append("}").append("\n")
				.append("\t\t\t\t").append("}");
			if(i != Common.QUERY.size()-1){
				buff.append(",");
			}else{
				buff.append("]").append("\n");
			}
		}
			
		buff.append("\t\t\t").append("}]").append("\n")
			.append("\t\t").append("},config);").append("\n")
			.append("\t\t").append(""+packages+".QueryPanel.superclass.constructor.call(this,config);").append("\n")
			.append("\t").append("}");
		
		if(flag){
			buff.append(",").append("\n")
				.append("\t").append("expandHandler : function(combo){").append("\n")
				.append("\t\t").append("var store = combo.getStore();").append("\n")
				.append("\t\t").append("if(store.getCount()>0 && store.getAt(0).get('name') != \"全部\"){").append("\n")
				.append("\t\t\t").append("store.insert(0,new store.recordType({name:\"全部\",id:null}));").append("\n")
				.append("\t\t").append("}").append("\n")
				.append("\t").append("}");
		}
		
		buff.append("\n").append("});").append("\n")
			.append("Ext.reg('"+packages+".QueryPanel',"+packages+".QueryPanel);").append("\n")
			.append("").append("\n")
			.append("/**").append("\n");
			
		//列表显示
		buff.append(" * 列表显示").append("\n")
			.append(" */").append("\n")
			.append(""+packages+".GridPanel = Ext.extend(Ext.grid.GridPanel,{").append("\n")
			.append("\t").append("constructor : function(config){").append("\n")
			.append("\t\t").append("var store = new Ext.data.JsonStore({").append("\n")
//			.append("\t\t\t").append("autoLoad:true,").append("\n")
			.append("\t\t\t").append("url:'"+Common.PROP_NAME+"_list.action',").append("\n")
			.append("\t\t\t").append("root:'data',").append("\n")
			.append("\t\t\t").append("totalProperty:'total',").append("\n")
			
//			.append("\t\t\t").append("fields:['id','pname','plinkman','paddress','ptel','pemail','premark'],").append("\n")
			.append("\t\t\t").append(getStoreFields()).append("\n")
			
			.append("\t\t\t").append("listeners : {").append("\n")
			.append("\t\t\t\t").append("beforeload : this.before"+Common.DOMAIN_NAME+"ListStoreLoad.createDelegate(this) ").append("\n")
			.append("\t\t\t").append("}").append("\n")
			.append("\t\t").append("});").append("\n")
			.append("\t\t").append("").append("\n")
			.append("\t\t").append("var sm = new Ext.grid.CheckboxSelectionModel();").append("\n")
			.append("\t\t").append("var cm = new Ext.grid.ColumnModel({").append("\n")
			.append("\t\t\t").append("columns:[new Ext.grid.RowNumberer(),sm");
			
		for(int i=0;i<Common.SHOW.size();i++){
			String type = "textfield";
			if("Integer".equalsIgnoreCase(Common.getQueryType(Common.SHOW.get(i))) ||
			   "Long".equalsIgnoreCase(Common.getQueryType(Common.SHOW.get(i))) ||
			   "Double".equalsIgnoreCase(Common.getQueryType(Common.SHOW.get(i)))){
				type = "numberfield";
			}else if("Date".equalsIgnoreCase(Common.getQueryType(Common.SHOW.get(i)))){
				type = "datefield";
			}
			if(Common.COMBO.contains(Common.SHOW.get(i))){
				String header = Common.getQueryDesc(Common.SHOW.get(i));
				if(header.indexOf("(") == -1){
					continue;
				}
				header = header.substring(0,header.indexOf("(")).trim();
				buff.append(",{").append("\n")
					.append("\t\t\t\t\t").append("header:\""+header).append("\",").append("\n")
					.append("\t\t\t\t\t").append("dataIndex:'"+Common.SHOW.get(i)+"',").append("\n")
					.append("\t\t\t\t\t").append("renderer : function(value){").append("\n");
				
				StringBuffer sb = new StringBuffer();
				String desc = Common.getQueryDesc(Common.SHOW.get(i));
				desc = desc.replace("（", "(")
							.replace("）", ")")
							.replace("，", ",");
				
				desc = desc.substring(desc.indexOf("(")+1, desc.indexOf(")"));
				String[] items = desc.split(",");
				for(int k=0;k<items.length;k++){
					if(k == 0){
						sb.append("\t\t\t\t\t\t").append("if(value == "+items[k].split("=")[0]+"){").append("\n")
						.append("\t\t\t\t\t\t\t").append("return '"+items[k].split("=")[1]+"'").append("\n")
						.append("\t\t\t\t\t\t").append("}");
					}else{
						sb.append("else if(value == "+items[k].split("=")[0]+"){").append("\n")
						.append("\t\t\t\t\t\t\t").append("return '"+items[k].split("=")[1]+"'").append("\n")
						.append("\t\t\t\t\t\t").append("}");
					}
					if(k == items.length - 1){
						sb.append("\n")
						  .append("\t\t\t\t\t\t").append("return value;").append("\n");
					}
				}
				buff.append(sb);
				buff.append("\t\t\t\t\t").append("}");
					
			}else if(Common.MAPPING.containsKey(Common.TABLE_NAME+":"+Common._COLUMN.get(Common._FIELD.indexOf(Common.SHOW.get(i))))){
				String mappInfo = Common.MAPPING.get(Common.TABLE_NAME+":"+Common._COLUMN.get(Common._FIELD.indexOf(Common.SHOW.get(i))));
				String mappingTableName = mappInfo.split(":")[0];
				String mappingColumnName = mappInfo.split(":")[1];
				String mappingShowName = Common.getFieldName(mappInfo.split(":")[2]);
				String mappingEntityName = Common.getEntityName(mappingTableName);
				String mappingPropName = (mappingEntityName.charAt(0)+"").toLowerCase()+mappingEntityName.substring(1);
				
				mappingColumnName = Common.getFieldName(mappingColumnName);
				
				buff.append(",{").append("\n")
					.append("\t\t\t\t\t").append("header:\""+Common.getQueryDesc(Common.SHOW.get(i))).append("\",").append("\n")
					.append("\t\t\t\t\t").append("dataIndex:'"+mappingPropName+"."+mappingShowName+"'");
			}else{
				buff.append(",{").append("\n")
					.append("\t\t\t\t\t").append("header:\""+Common.getQueryDesc(Common.SHOW.get(i))).append("\",").append("\n")
					.append("\t\t\t\t\t").append("dataIndex:'"+Common.SHOW.get(i)+"'");
				if("datefield".equals(type)){
					buff.append(",").append("\n")
						.append("\t\t\t\t\t").append("renderer : Ext.renderDateTime");
				}
			}
			buff.append("\n")
				.append("\t\t\t\t").append("}");
			if(i==Common.SHOW.size()-1 && !gridHiddenColumn.isEmpty()){
				for (String hiddenColumn : gridHiddenColumn) {
					buff.append(",{").append("\n")
						.append("\t\t\t\t\t").append("hidden:true,").append("\n")
						.append("\t\t\t\t\t").append("dataIndex:'"+hiddenColumn+"'");
					buff.append("\n")
					.append("\t\t\t\t").append("}");
				}
			}
		}
			
		buff.append("]").append("\n")
			.append("\t\t").append("});").append("\n")
			.append("\t\t").append("config = Ext.apply({").append("\n")
			.append("\t\t\t").append("id:'"+Common.PROP_NAME+"Grid',").append("\n")
			.append("\t\t\t").append("store:store,").append("\n")
			.append("\t\t\t").append("sm:sm,").append("\n")
			.append("\t\t\t").append("cm:cm,").append("\n")
			.append("\t\t\t").append("stripeRows: true,").append("\n")
			.append("\t\t\t").append("loadMask:true,").append("\n")
			.append("\t\t\t").append("viewConfig:{").append("\n")
			.append("\t\t\t\t").append("forceFit: true").append("\n")
			.append("\t\t\t").append("},").append("\n")
			.append("\t\t\t").append("tbar : new Ext.PagingToolbar({").append("\n")
			.append("\t\t\t\t").append("store : store,").append("\n")
			.append("\t\t\t\t").append("pageSize : Ext.pageSize,").append("\n")
			.append("\t\t\t\t").append("displayInfo : true").append("\n")
			.append("\t\t\t").append("})").append("\n")
			.append("\t\t").append("},config);").append("\n")
			.append("\t\t").append(""+packages+".GridPanel.superclass.constructor.call(this,config);").append("\n")
			.append("\t").append("},").append("\n")
			.append("\t").append("before"+Common.DOMAIN_NAME+"ListStoreLoad : function(store){").append("\n")
			.append("\t\t").append("store.baseParams = Ext.apply(Ext.getCmp('"+Common.PROP_NAME+"QueryPanel').getForm().getValues(),{").append("\n")
			.append("\t\t\t").append("start:0,").append("\n")
			.append("\t\t\t").append("limit:Ext.pageSize").append("\n")
			.append("\t\t").append("})").append("\n")
			.append("\t").append("}").append("\n")
			.append("});").append("\n")
			.append("Ext.reg('"+packages+".GridPanel',"+packages+".GridPanel);").append("\n");
		
		StringBuffer storeContent = new StringBuffer();
		for(String store : stores){
			storeContent.append(store).append("\n");
		}
		String content = buff.toString().replace("EXT-STORES", storeContent);
//		System.out.println(buff.toString());
		
		File file = new File(Common.SAVE_PATH+File.separator+"META-INF"+File.separator+"scripts");
		if(!file.exists()){
			file.mkdirs();
		}
		Common.write(new File(file,Common.PROP_NAME+".js"), content);
		
		System.out.println(Common.PROP_NAME+".js 已生成......");
	}
	
	public static String getStoreFields(){
		StringBuffer sb = new StringBuffer();
		sb.append("fields:['id',");
		for(int i=0;i<Common.SHOW.size();i++){
			if(Common.PRIMATY_KEY.contains(Common.SHOW.get(i))){	//主键不用再新增表单上显示
				continue;
			}
			if(Common.MAPPING.containsKey(Common.TABLE_NAME+":"+Common._COLUMN.get(Common._FIELD.indexOf(Common.SHOW.get(i))))){
				String mappInfo = Common.MAPPING.get(Common.TABLE_NAME+":"+Common._COLUMN.get(Common._FIELD.indexOf(Common.SHOW.get(i))));
				String mappingTableName = mappInfo.split(":")[0];
				String mappingColumn = mappInfo.split(":")[1];
				String mappingColumnName = mappInfo.split(":")[2];
				String mappingEntityName = Common.getEntityName(mappingTableName);
				String mappingPropName = (mappingEntityName.charAt(0)+"").toLowerCase()+mappingEntityName.substring(1);
				
				mappingColumnName = Common.getFieldName(mappingColumnName);
				
				List<String> keys = Common.primaryKeys.get(mappingTableName);
				if(keys == null){
					keys = dbUtils.getPrimaryKey(mappingTableName);
					Common.primaryKeys.put(mappingTableName, keys);
				}
				if(keys.indexOf(mappingColumn) != -1){
					sb.append("'").append(mappingPropName+".id").append("',");
					gridHiddenColumn.add(mappingPropName+".id");
				}
				sb.append("'").append(mappingPropName+"."+mappingColumnName).append("'");
				
			}else{
				sb.append("'").append(Common.SHOW.get(i)).append("'");
			}
			if(i != Common.SHOW.size()-1){
				sb.append(",");
			}
		}
		
		sb.append("],");
		return sb.toString();
	}
	
	public static String getGridColumns(){
		
		return null;
	}
	
	public static void main(String[] args) {
//		createJSP();
//		createJS();
	}
	
}
