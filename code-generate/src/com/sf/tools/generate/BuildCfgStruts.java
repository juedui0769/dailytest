package com.sf.tools.generate;
import java.io.File;

import com.sf.tools.common.Common;


public class BuildCfgStruts {

	private static StringBuffer buff = new StringBuffer();
	
	public static void createStruts(){
		buff = new StringBuffer();
		buff.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append("\n")
			.append("<!DOCTYPE struts PUBLIC \"-//Apache Software Foundation//DTD Struts Configuration 2.0//EN\" \"http://struts.apache.org/dtds/struts-2.0.dtd\">").append("\n")
			.append("<struts>").append("\n\n")
			.append("\t").append("<package name=\""+Common.MODULE_NAME+"\" namespace=\"/"+Common.MODULE_NAME+"\" extends=\"web-default\">").append("\n\n");
		
//		<!-- 罚款信息 -->
//		<action name="forfeit" class="forwardAction">
//			<result>forfeit.jsp</result>
//		</action>
		for(int i=0;i<Common.PROP_NAMES.size();i++){
			buff.append("\t\t").append("<!-- "+Common.DOMAIN_DESCS.get(i)+" -->").append("\n")
				.append("\t\t").append("<action name=\""+Common.PROP_NAMES.get(i)+"\" class=\"forwardAction\">").append("\n")
				.append("\t\t\t").append("<result>"+Common.PROP_NAMES.get(i)+".jsp</result>").append("\n")
				.append("\t\t").append("</action>").append("\n");
		}
		
		buff.append("\n\t\t").append("<!-- 通用下载Action配置 -->").append("\n")
			.append("\t\t").append("<action name=\"download\" class=\"downloadAction\">").append("\n")
			.append("\t\t\t").append("<result name=\"success\" type=\"stream\">").append("\n")
			.append("\t\t\t\t").append("<param name=\"contentType\">application/octet-stream;charset=UTF-8</param>").append("\n")
			.append("\t\t\t\t").append("<param name=\"inputName\">inputStream</param>").append("\n")
			.append("\t\t\t\t").append("<param name=\"contentDisposition\">attachment;filename=\"${fileName}\"</param>").append("\n")
			.append("\t\t\t\t").append("<param name=\"bufferSize\">4096</param>").append("\n")
			.append("\t\t\t").append("</result>").append("\n")
			.append("\t\t\t").append("<result name=\"error\">downloadFailed.jsp</result>").append("\n")
			.append("\t\t").append("</action>").append("\n\n");
		
		buff.append("\t\t").append("<!-- 通用导入Action配置 -->").append("\n")
		.append("\t\t").append("<action name=\"*_*_import\" class=\"{1}Action\" method=\"{2}Import\">").append("\n")
		.append("\t\t\t").append("<result type=\"json\">").append("\n")
		.append("\t\t\t\t").append("<!-- 注意这里必须配置contentType否则会出错 -->").append("\n")
		.append("\t\t\t\t").append("<param name=\"contentType\">text/html; charset=utf-8</param>").append("\n")
		.append("\t\t\t\t").append("<param name=\"includeProperties\">").append("\n")
		.append("\t\t\t\t\t").append("success,msg,data").append("\n")
		.append("\t\t\t\t").append("</param>").append("\n")
		.append("\t\t\t").append("</result>").append("\n")
		.append("\t\t").append("</action>").append("\n\n");
		
		buff.append("\t\t").append("<!-- 通用Action 使用通配符  例如：user_list.action,则访问 UserAction类中的list方法 -->").append("\n")
			.append("\t\t").append("<action name=\"*_*\" class=\"{1}Action\" method=\"{2}\">").append("\n")
			.append("\t\t\t").append("<result name=\"success\" type=\"json\" />").append("\n")
			.append("\t\t").append("</action>").append("\n\n");
		
			
		buff.append("\t").append("</package>").append("\n\n")
			.append("</struts>");
//		System.out.println(buff.toString());

		File file = new File(Common.SAVE_PATH+File.separator+"META-INF"+File.separator+"config");
		if(!file.exists()){
			file.mkdirs();
		}
		Common.write(new File(file,"action.xml"), buff.toString());
		
		System.out.println("action.xml 已生成.....");
	}
	
	public static void main(String[] args) {
//		createHibernate();
//		createSpring();
//		createStruts();
	}
}
