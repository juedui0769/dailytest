package com.sf.tools.generate;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.sf.tools.common.Common;





public class BuildAction {

	private static StringBuffer buff = new StringBuffer();
	
	public static void createAction(){
		buff = new StringBuffer();
		importPackage();
		buff.append(Common.getClassDesc(Common.DOMAIN_DESC+" Action处理类"));
		if("注解".equals(Common.GENERATE_SPRING_TYPE)){
			buff.append("@Controller").append("\n").append("@Scope(\"prototype\")").append("\n");
		}
		buff.append("public class "+Common.ACTION_NAME+" extends BaseGridAction<"+Common.DOMAIN_NAME+"> {").append("\n\n")
			.append("\t").append("private static final long serialVersionUID = 1L;").append("\n\n");
		if("注解".equals(Common.GENERATE_SPRING_TYPE)){
			buff.append("\t").append("@Resource").append("\n");
		}
		buff.append("\t").append("private "+Common.IBIZ_NAME+" "+Common.PROP_NAME_BIZ+";").append("\n\n")
			.append("\t").append("private Collection<"+Common.DOMAIN_NAME+"> data;").append("\n\n")
			.append("\t").append("private "+Common.DOMAIN_NAME+" "+Common.PROP_NAME+";").append("\n\n")
			.append("\t").append("private Boolean success = true;").append("\n\n")
			.append("\t").append("private long total;").append("\n\n")
//			.append("\t").append("private int start;").append("\n\n")
//			.append("\t").append("private int limit;").append("\n\n")
			.append("\t").append("private String msg;").append("\n\n");
		if(Common.DELETE){
			buff.append("\t").append("private Long[] ids;").append("\n\n");
		}
		if(Common.IMPORT){
			buff.append("\t").append("private File uploadFile;").append("\n\n");
		}
		if(Common.EXPORT){
			buff.append("\t").append("private String fileName;").append("\n\n");
		}
		
		buff.append(create_Method_list());
		if(Common.ADD){
			buff.append(create_Method_save());
		}
		if(Common.UPDATE){
			buff.append(create_Method_update());
		}
		if(Common.DELETE){
			buff.append(create_Method_delete());
		}
		if(Common.IMPORT){
			buff.append(create_Method_import());
		}
		if(Common.EXPORT){
			buff.append(create_Method_export());
		}
		
		List<String> queryNames = new ArrayList<String>();
		Collection<String> values = Common.MAPPING.values();
		for (String value : values) {
			String[] arr = value.split(":");
			String tableName = arr[0];
			String showName = arr[2];
			if(tableName.equals(Common.TABLE_NAME) && !queryNames.contains(showName)){
				queryNames.add(showName);
			}
		}
		if(!queryNames.isEmpty()){
			buff.append(Common.getMethodDesc("获取所有"+Common.DOMAIN_DESC));
			buff.append("\t").append("public String findAll(){").append("\n")
				.append("\t\t").append("data = "+Common.PROP_NAME_BIZ+".findAll();").append("\n")
				.append("\t\t").append("return SUCCESS;").append("\n")
				.append("\t").append("}").append("\n\n");
		}
		
		buff.append("\t").append("public void set"+Common.BIZ_NAME+"("+Common.IBIZ_NAME+" "+Common.PROP_NAME_BIZ+") {").append("\n")
		.append("\t\t").append("this."+Common.PROP_NAME_BIZ+" = "+Common.PROP_NAME_BIZ+";").append("\n")
		.append("\t}").append("\n")
		
		.append("\t").append("public Collection<"+Common.DOMAIN_NAME+"> getData() {").append("\n")
		.append("\t\t").append("return data;").append("\n")
		.append("\t}").append("\n")
		
		.append("\t").append("public "+Common.DOMAIN_NAME+" get"+Common.DOMAIN_NAME+"() {").append("\n")
		.append("\t\t").append("return "+Common.PROP_NAME+";").append("\n")
		.append("\t}").append("\n")
		
		.append("\t").append("public void set"+Common.DOMAIN_NAME+"("+Common.DOMAIN_NAME+" "+Common.PROP_NAME+") {").append("\n")
		.append("\t\t").append("this."+Common.PROP_NAME+" = "+Common.PROP_NAME+";").append("\n")
		.append("\t}").append("\n")
		
		.append("\t").append("public Boolean getSuccess() {").append("\n")
		.append("\t\t").append("return success;").append("\n")
		.append("\t}").append("\n")
		
		.append("\t").append("public long getTotal() {").append("\n")
		.append("\t\t").append("return total;").append("\n")
		.append("\t}").append("\n")
		
//			.append("\t").append("public void setStart(int start) {").append("\n")
//			.append("\t\t").append("this.start = start;").append("\n")
//			.append("\t}").append("\n")
		
//			.append("\t").append("public void setLimit(int limit) {").append("\n")
//			.append("\t\t").append("this.limit = limit;").append("\n")
//			.append("\t}").append("\n")
		
		.append("\t").append("public String getMsg() {").append("\n")
		.append("\t\t").append("return msg;").append("\n")
		.append("\t}").append("\n");
			
		if(Common.DELETE){
			buff.append("\t").append("public void setIds(Long[] ids) {").append("\n")
				.append("\t\t").append("this.ids = ids;").append("\n")
				.append("\t}").append("\n");
		}
		
		if(Common.IMPORT){
			buff.append("\t").append("public void setUploadFile(File uploadFile) {").append("\n")
				.append("\t\t").append("this.uploadFile = uploadFile;").append("\n")
				.append("\t}").append("\n");
		}
		
		if(Common.EXPORT){
			buff.append("\t").append("public String getFileName() {").append("\n")
				.append("\t\t").append("return fileName;").append("\n")
				.append("\t}").append("\n");
		}
			
		buff.append("}");
//		System.out.println(buff.toString());
		String content = buff.toString();
		
		File file = new File(Common.SAVE_PATH+File.separator+"action");
		if(!file.exists()){
			file.mkdirs();
		}
		Common.write(new File(file,Common.ACTION_NAME+".java"), content);
		
		System.out.println(Common.ACTION_NAME+".java 已生成......");
		
	}
	
	public static StringBuffer getTryCatch(StringBuffer sb, String msg){
		StringBuffer buff = new StringBuffer();
		buff.append("\t\t").append("try {").append("\n")
			.append(sb)
			.append("\t\t").append("} catch (BizException e) {").append("\n")
			.append("\t\t\t").append("success = false;").append("\n")
			.append("\t\t\t").append("msg = e.getMessage();").append("\n")
			.append("\t\t").append("} catch (Exception e){").append("\n")
			.append("\t\t\t").append("success = false;").append("\n")
			.append("\t\t\t").append("msg = \""+msg+"\";").append("\n")
			.append("\t\t\t").append("log.error(msg,e);").append("\n")
			.append("\t\t").append("}").append("\n");
		return buff;
	}
	
	private static StringBuffer create_Method_list() {
		StringBuffer sb = new StringBuffer();
		sb.append(Common.getMethodDesc("查询"));
		sb.append("\t").append("public String list(){").append("\n");
		
		StringBuffer operate = new StringBuffer();
		operate.append("\t\t\t").append("QueryObj queryObj = QueryObj.parseQueryObj(this);").append("\n");
		operate.append("\t\t\t").append("IPage<"+Common.DOMAIN_NAME+"> page = "+Common.PROP_NAME_BIZ+".findPageBy(queryObj);").append("\n")
		  	   .append("\t\t\t").append("data = page.getData();").append("\n")
		  	   .append("\t\t\t").append("total = page.getTotalSize();").append("\n");
		sb.append(getTryCatch(operate,"查询失败"));
		
//		sb.append("\t\t").append(Common.PROP_NAME+" = null;").append("\n")
		sb.append("\t\t").append("return SUCCESS;").append("\n")
		  .append("\t").append("}").append("\n\n");
		return sb;
	}
	private static StringBuffer create_Method_save() {
		StringBuffer sb = new StringBuffer();
		sb.append(Common.getMethodDesc("新增"));
		sb.append("\t").append("public String save(){").append("\n");
		
		StringBuffer operate = new StringBuffer();
		operate.append("\t\t\t").append(""+Common.PROP_NAME_BIZ+".save("+Common.PROP_NAME+");").append("\n");
		sb.append(getTryCatch(operate,"保存失败"));
		
		sb.append("\t\t").append(Common.PROP_NAME+" = null;").append("\n")
		  .append("\t\t").append("return SUCCESS;").append("\n")
		  .append("\t").append("}").append("\n\n");
		return sb;
	}
	private static StringBuffer create_Method_update() {
		StringBuffer sb = new StringBuffer();
		sb.append(Common.getMethodDesc("修改"));
		sb.append("\t").append("public String update(){").append("\n");
		
		StringBuffer operate = new StringBuffer();
		operate.append("\t\t\t").append(Common.PROP_NAME_BIZ+".update("+Common.PROP_NAME+");").append("\n");
		sb.append(getTryCatch(operate,"保存失败"));
		
		sb.append("\t\t").append(Common.PROP_NAME+" = null;").append("\n")
		  .append("\t\t").append("return SUCCESS;").append("\n")
		  .append("\t").append("}").append("\n\n");
		return sb;
	}
	private static StringBuffer create_Method_delete() {
		StringBuffer sb = new StringBuffer();
		sb.append(Common.getMethodDesc("删除"));
		sb.append("\t").append("public String delete(){").append("\n");
		
		StringBuffer operate = new StringBuffer();
		operate.append("\t\t\t").append(Common.PROP_NAME_BIZ+".delete(ids);").append("\n");
		sb.append(getTryCatch(operate,"删除失败"));
		  
		sb.append("\t\t").append("return SUCCESS;").append("\n")
		  .append("\t").append("}").append("\n\n");
		return sb;
	}
	private static StringBuffer create_Method_import() {
		StringBuffer sb = new StringBuffer();
		sb.append(Common.getMethodDesc("导入"));
		sb.append("\t").append("public String addImport(){").append("\n");
		
		StringBuffer operate = new StringBuffer();
		operate.append("\t\t\t").append(Common.PROP_NAME_BIZ+".addImports(uploadFile);").append("\n");
		sb.append(getTryCatch(operate,"导入失败"));
		
		sb.append("\t\t").append("return SUCCESS;").append("\n")
		  .append("\t").append("}").append("\n\n");
		return sb;
	}
	private static StringBuffer create_Method_export() {
		StringBuffer sb = new StringBuffer();
		sb.append(Common.getMethodDesc("导出"));
		sb.append("\t").append("public String export(){").append("\n");
		
		StringBuffer operate = new StringBuffer();
		operate.append("\t\t\t").append("QueryObj queryObj = QueryObj.parseQueryObj(this);").append("\n");
		operate.append("\t\t\t").append("fileName = "+Common.PROP_NAME_BIZ+".export(queryObj);").append("\n");
		sb.append(getTryCatch(operate,"导出失败"));
		
		sb.append("\t\t").append("return SUCCESS;").append("\n")
		  .append("\t").append("}").append("\n\n");
		return sb;
	}

	private static void importPackage(){
		buff.append("package ").append(Common.PACKAGE_NAME).append(".action;").append("\n\n");
		if(Common.IMPORT){
			buff.append("import java.io.File;").append("\n");
		}
		buff.append("import java.util.Collection;").append("\n")
			.append("").append("\n");
		
		if("注解".equals(Common.GENERATE_SPRING_TYPE)){
			buff.append("import javax.annotation.Resource;").append("\n\n")
				.append("import org.springframework.context.annotation.Scope;").append("\n")
				.append("import org.springframework.stereotype.Controller;").append("\n\n");
		}
		
		buff.append("import com.sf.framework.base.IPage;").append("\n")
			.append("import com.sf.framework.core.exception.BizException;").append("\n")
			.append("import com.sf.framework.server.base.action.BaseGridAction;\n")
			.append("import com.sf.module.").append(Common.MODULE_NAME).append(".biz."+Common.IBIZ_NAME+";\n")
			.append("import com.sf.module.").append(Common.MODULE_NAME).append(".domain.").append(Common.DOMAIN_NAME).append(";\n")
			.append("import com.sf.module.frameworkimpl.domain.QueryObj;").append("\n")
			.append("\n");
	}
	
	public static void main(String[] args) {
		createAction();
	}
}
