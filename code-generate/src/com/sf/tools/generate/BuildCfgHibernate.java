package com.sf.tools.generate;
import java.io.File;

import com.sf.tools.common.Common;


public class BuildCfgHibernate {

	private static StringBuffer buff = new StringBuffer();
	private static StringBuffer sb = new StringBuffer();
	
	public static void createHibernate(int index,boolean isLastNext){
		if(index == 0){
			sb = new StringBuffer();
			buff = new StringBuffer();
			buff.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append("\n")
				.append("<!DOCTYPE hibernate-mapping PUBLIC \"-//Hibernate/Hibernate Mapping DTD//EN\" \"http://hibernate.sourceforge.net/hibernate-mapping-3.0.dtd\">").append("\n\n")
				.append("<hibernate-mapping package=\""+Common.PACKAGE_NAME+".domain\" auto-import=\"false\">").append("\n\n");
			buff.append("HIBERNATE-MAPPING-MARK");
			buff.append("</hibernate-mapping>");
		}
		
			
		sb.append("\t").append("<!--"+Common.DOMAIN_DESC+"-->").append("\n")
		  .append("\t").append("<class name=\""+Common.DOMAIN_NAME+"\" table=\""+Common.TABLE_NAME+"\">").append("\n")
			.append("\t\t").append("<id name=\""+(Common.PRIMATY_KEY.isEmpty()?Common._FIELD.get(0):Common.PRIMATY_KEY.get(0))+"\" type=\"java.lang.Long\">").append("\n")
			.append("\t\t\t").append("<column name=\""+(Common.PRIMATY_KEY_COLUMN.isEmpty()?Common._COLUMN.get(0):Common.PRIMATY_KEY_COLUMN.get(0))+"\" precision=\"19\" scale=\"0\" />").append("\n")
			.append("\t\t\t").append("<generator class=\"sequence\">").append("\n")
			.append("\t\t\t\t").append("<param name=\"sequence\">"+Common.SEQUENCE_NAME+"</param>").append("\n")
			.append("\t\t\t").append("</generator>").append("\n")
			.append("\t\t").append("</id>").append("\n");
			
			for(int i=1;i<Common._FIELD.size();i++){
				String type = Common._TYPE.get(i);
				if("Date".equalsIgnoreCase(type)){
					type = "java.util.Date";
				}else{
					type = "java.lang."+type;
				}
//				String length = type.endsWith("String") ? " length=\"32\"" : "";
				String key = Common.TABLE_NAME+":"+Common._COLUMN.get(i);
				if(Common.MAPPING.containsKey(key)){
					String mappTable = Common.MAPPING.get(key).split(":")[0];
					String entityName = Common.getEntityName(mappTable);
					String entityPropName = (entityName.charAt(0)+"").toLowerCase()+entityName.substring(1);
					sb.append("\t\t").append("<many-to-one name=\""+entityPropName+"\" column=\""+Common._COLUMN.get(i)+"\" not-null=\"false\" fetch=\"join\" lazy=\"false\" />").append("\n");
				}else{
					sb.append("\t\t").append("<property name=\""+Common._FIELD.get(i)+"\" type=\""+type+"\">").append("\n")
					  .append("\t\t\t").append("<column name=\""+Common._COLUMN.get(i)+"\" "+Common._LENGTH.get(i)+" />").append("\n")
					  .append("\t\t").append("</property>").append("\n");
				}
			}
//			<property name="vehicleCode" type="java.lang.String">
//				<column name="VEHICLE_CODE" length="16" />
//			</property>
			
		sb.append("\t").append("</class>").append("\n\n");
//		System.out.println(buff.toString());
		
		if(isLastNext){
			
			String content = buff.toString().replace("HIBERNATE-MAPPING-MARK", sb);
			
			File file = new File(Common.SAVE_PATH+File.separator+"META-INF"+File.separator+"config");
			if(!file.exists()){
				file.mkdirs();
			}
			Common.write(new File(file,"mapping.xml"), content);
		}
		
//		System.out.println("mapping.xml 已生成.....");
	}
	
	public static void main(String[] args) {
//		createHibernate();
//		createSpring();
//		createStruts();
	}
}
